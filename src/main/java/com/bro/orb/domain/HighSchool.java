package com.bro.orb.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "bro_high_school")
public class HighSchool implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "high_year")
	private boolean highYearView;
	
	@Column(name = "high_institution")
	private boolean highInstitutionView;
	
	@Column(name = "high_specialization")
	private boolean highSpecializationView;
	
	@Column(name = "high_course_type")
	private boolean highCourseTypeView;
	
	@Column(name = "high_total_marks")
	private boolean highTotalMarksView;
	
	@Column(name = "high_obtain_marks")
	private boolean highObtainarksView;
	
	@Column(name = "high_percentage")
	private boolean highPercentageView;
	
	@Column(name = "high_board")
	private boolean highBoardView;
	
	@Column(name = "high_certificate")
	private boolean highCertificateView;
	
	
	@Column(name = "high_grade")
	private boolean highGradeView;
	
	@Column(name = "high_min_percent")
	private int highMinPercent;
	
	

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public HighSchool() {
		super();
	}

	public HighSchool(int id, boolean highYearView, boolean highInstitutionView, boolean highSpecializationView,
			boolean highCourseTypeView, boolean highTotalMarksView, boolean highObtainarksView,
			boolean highPercentageView, boolean highBoardView, boolean highCertificateView, boolean highGradeView,
			int highMinPercent, RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.highYearView = highYearView;
		this.highInstitutionView = highInstitutionView;
		this.highSpecializationView = highSpecializationView;
		this.highCourseTypeView = highCourseTypeView;
		this.highTotalMarksView = highTotalMarksView;
		this.highObtainarksView = highObtainarksView;
		this.highPercentageView = highPercentageView;
		this.highBoardView = highBoardView;
		this.highCertificateView = highCertificateView;
		this.highGradeView = highGradeView;
		this.highMinPercent = highMinPercent;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isHighYearView() {
		return highYearView;
	}

	public void setHighYearView(boolean highYearView) {
		this.highYearView = highYearView;
	}

	public boolean isHighInstitutionView() {
		return highInstitutionView;
	}

	public void setHighInstitutionView(boolean highInstitutionView) {
		this.highInstitutionView = highInstitutionView;
	}

	public boolean isHighSpecializationView() {
		return highSpecializationView;
	}

	public void setHighSpecializationView(boolean highSpecializationView) {
		this.highSpecializationView = highSpecializationView;
	}

	public boolean isHighCourseTypeView() {
		return highCourseTypeView;
	}

	public void setHighCourseTypeView(boolean highCourseTypeView) {
		this.highCourseTypeView = highCourseTypeView;
	}

	public boolean isHighTotalMarksView() {
		return highTotalMarksView;
	}

	public void setHighTotalMarksView(boolean highTotalMarksView) {
		this.highTotalMarksView = highTotalMarksView;
	}

	public boolean isHighObtainarksView() {
		return highObtainarksView;
	}

	public void setHighObtainarksView(boolean highObtainarksView) {
		this.highObtainarksView = highObtainarksView;
	}

	public boolean isHighPercentageView() {
		return highPercentageView;
	}

	public void setHighPercentageView(boolean highPercentageView) {
		this.highPercentageView = highPercentageView;
	}

	public boolean isHighBoardView() {
		return highBoardView;
	}

	public void setHighBoardView(boolean highBoardView) {
		this.highBoardView = highBoardView;
	}

	public boolean isHighCertificateView() {
		return highCertificateView;
	}

	public void setHighCertificateView(boolean highCertificateView) {
		this.highCertificateView = highCertificateView;
	}

	public boolean isHighGradeView() {
		return highGradeView;
	}

	public void setHighGradeView(boolean highGradeView) {
		this.highGradeView = highGradeView;
	}

	public int getHighMinPercent() {
		return highMinPercent;
	}

	public void setHighMinPercent(int highMinPercent) {
		this.highMinPercent = highMinPercent;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "HighSchool [id=" + id + ", highYearView=" + highYearView + ", highInstitutionView="
				+ highInstitutionView + ", highSpecializationView=" + highSpecializationView + ", highCourseTypeView="
				+ highCourseTypeView + ", highTotalMarksView=" + highTotalMarksView + ", highObtainarksView="
				+ highObtainarksView + ", highPercentageView=" + highPercentageView + ", highBoardView=" + highBoardView
				+ ", highCertificateView=" + highCertificateView + ", highGradeView=" + highGradeView
				+ ", highMinPercent=" + highMinPercent + ", registrationViewBean=" + registrationViewBean + "]";
	}

	
	
	
}
