package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bro_candidate_personal_details")
public class PersonalDetails extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "registration_id", unique = true, nullable = false)
	private String registrationId;

	@Column(name = "recruitment_zone")
	private String recruitmentZone;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "mother_name")
	private String motherName;

	@Column(name = "category")
	private String category;

	@Column(name = "category_certificate")
	private String categoryCertificate;

//	@Column(name = "aadhar_certificate")
//	private String aadharCertificate;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "age")
	private String age;

	@Column(name = "gender")
	private String gender;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "email_address")
	private String emailAddress;

	@Column(name = "nationality")
	private String nationality;

//	@Column(name = "aadhaar_card")
//	private boolean aadhaarCard;

	@Column(name="casualplabour")
	private boolean casualplabour;
	
	@Column(name="cplCertificate")
	private String cplCertificate;
	
	@Column(name="panNumber")
	private String panNumber;
	
	@Column(name="panCertificate")
	private String pancertificate;
	
	
	

	@Column(name = "aadhaar_number")
	private String aadhaarNumber;

	@Column(name = "marital_status")
	private String maritalStatus;

	@Column(name = "belong_community")
	private boolean belongCommunity;

	@Column(name = "community_name")
	private String communityName;

	@Column(name = "permanent_address")
	private String permanentAddress;

	@Column(name = "permanent_state")
	private String permanentState;

	@Column(name = "permanent_city")
	private String permanentCity;

	@Column(name = "permanent_pincode")
	private int permanentPincode;

	@Column(name = "postal_address")
	private String postalAddress;

	@Column(name = "postal_state")
	private String postalState;

	@Column(name = "postal_city")
	private String postalCity;

	@Column(name = "postal_pincode")
	private int postalPincode;

//	@Column(name = "son_daughter_exservice")
//	private boolean sonDaughterExservice;
//
//	
//
//	@Column(name = "son_daughter_gref")
//	private boolean sonDaughterGref;
//
//	@Column(name = "son_daughter_number")
//	private String sonDaughterNumber;
//
//	@Column(name = "disablity")
//	private boolean disablity;
//
//	@Column(name = "disability_specify")
//	private String disabilitySpecify;
//
//	@Column(name = "height")
//	private double candidateHeight;
//
//	@Column(name = "weight")
//	private double candidateWeight;
//
//	@Column(name = "blood_group")
//	private String bloodGroup;
//
//	@Column(name = "eye_sight")
//	private String eyeSight;
//	
//	@Column(name = "armed_force")
//	private boolean armedForce;
//
//	@Column(name = "armed_from")
//	private Date armedFrom;
//
//	@Column(name = "armed_to")
//	private Date armedTo;
//	
//	@Column(name = "armed_dis_certificate")
//	private String armeddisCertificate;
//	
//	@Column(name = "armed_army_number")
//	private String armedarmyNumber;
//
//	@Column(name = "central_govt")
//	private boolean centralGovt;
//
//	@Column(name = "served_from")
//	private Date servedFrom;
//
//	@Column(name = "served_to")
//	private Date servedTo;
//	
//	@Column(name = "emp_ex_gref")
//	private boolean empExGref;
//
//	@Column(name = "served_bro_from")
//	private Date servedBroFrom;
//
//	@Column(name = "served_bro_to")
//	private Date servedBroTo;
//	
//	@Column(name = "gs_number")
//	private String gsNumber;
//	
//	@Column(name = "central_govt_certificate")
//	private String centralGovtCertificate;
//
//	@Column(name = "son_daughter_rank")
//	private String sonDaughterRank;
//
//	@Column(name = "son_daughter_name")
//	private String sonDaughterName;
//
//	@Column(name = "brother_sister_gref")
//	private boolean brotherSisterGref;
//
//	@Column(name = "brother_sister_number")
//	private String brotherSisterNumber;
//
//	@Column(name = "brother_sister_rank")
//	private String brotherSisterRank;
//
//	@Column(name = "brother_sister_name")
//	private String brotherSisterName;

	@Column(name = "ncc_b")
	private boolean nccB;

	@Column(name = "nccb_certificate")
	private String nccbCertificate;

	@Column(name = "ncc_c")
	private boolean nccC;

	@Column(name = "nccc_certificate")
	private String nccCertificate;

	@Column(name = "sportsman")
	private boolean sportsman;
	
	@Column(name="sportsLevel")
	private String sportsLevel;
	
	@Column(name="sportCertificate")
	private String sportCertificate;

	@Column(name = "sonExGps")
	private boolean sonExGps;
	
	@Column(name="sonGsNumber")
	private String sonGsNumber;
	
	@Column(name="sonGpsRank")
	private String sonGpsRank;
	
	@Column(name="sonGpsName")
	private String sonGpsName;
	
	@Column(name="sonUnitName")
	private String sonUnitName;
	
	@Column(name="unitCertificate")
	private String unitCertificate;
	
	@Column(name="reapptCandidate")
	private boolean reapptCandidate;
	
	@Column(name="reapptCandidateGs")
	private String reapptCandidateGs;
	
	@Column(name="reaaptCandidateRank")
	private String reaaptCandidateRank;
	
	@Column(name="reapptCandidateName")
	private String reapptCandidateName;
	
	@Column(name="reapptCandidateUnit")
	private String reapptCandidateUnit;
	
	@Column(name="sonExservice")
	private boolean sonExservice;
	
	@Column(name="armyNumber")
	private String armyNumber;
	
	@Column(name="armyRank")
	private String armyRank;
	
	@Column(name="armyName")
    private String armyName;
	
	@Column(name="armyLastUnit")
	private String armyLastUnit;
	
	@Column(name="lastUnitCertificate")
	private String lastUnitCertificate;
	
	@Column(name="exService")
	private boolean exService;
	
	@Column(name="dischargeCertificate")
	private String dischargeCertificate;
	
	@Column(name = "payment")
	private boolean payment;

	@Column(name = "final_submit")
	private boolean finalSubmit;

	

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "essential_id", referencedColumnName = "id")
	private EssentialQualification essential;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "graduation_id", referencedColumnName = "id")
	private GraduationQualification graduation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "technical_id", referencedColumnName = "id")
	private TechnicalQualification technical;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "experience_id", referencedColumnName = "id")
	private OrganisationExperience experience;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "driving_id", referencedColumnName = "id")
	private DrivingLicense driving;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "certificate_id", referencedColumnName = "id")
	private UploadCertificate certificate;

	@OneToOne
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private JobDetails jobDetails;

	public PersonalDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PersonalDetails(long id, String registrationId, String recruitmentZone, String firstName, String middleName,
			String lastName, String fatherName, String motherName, String category, String categoryCertificate,
			Date dateOfBirth, String age, String gender, String mobileNumber, String emailAddress, String nationality,
			boolean casualplabour, String cplCertificate, String panNumber, String pancertificate, String aadhaarNumber,
			String maritalStatus, boolean belongCommunity, String communityName, String permanentAddress,
			String permanentState, String permanentCity, int permanentPincode, String postalAddress, String postalState,
			String postalCity, int postalPincode, boolean nccB, String nccbCertificate, boolean nccC,
			String nccCertificate, boolean sportsman, String sportsLevel, String sportCertificate, boolean sonExGps,
			String sonGsNumber, String sonGpsRank, String sonGpsName, String sonUnitName, String unitCertificate,
			boolean reapptCandidate, String reapptCandidateGs, String reaaptCandidateRank, String reapptCandidateName,
			String reapptCandidateUnit, boolean sonExservice, String armyNumber, String armyRank, String armyName,
			String armyLastUnit, String lastUnitCertificate, boolean exService, String dischargeCertificate,
			boolean payment, boolean finalSubmit, User user, EssentialQualification essential,
			GraduationQualification graduation, TechnicalQualification technical, OrganisationExperience experience,
			DrivingLicense driving, UploadCertificate certificate, JobDetails jobDetails) {
		super();
		this.id = id;
		this.registrationId = registrationId;
		this.recruitmentZone = recruitmentZone;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.category = category;
		this.categoryCertificate = categoryCertificate;
		this.dateOfBirth = dateOfBirth;
		this.age = age;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailAddress = emailAddress;
		this.nationality = nationality;
		this.casualplabour = casualplabour;
		this.cplCertificate = cplCertificate;
		this.panNumber = panNumber;
		this.pancertificate = pancertificate;
		this.aadhaarNumber = aadhaarNumber;
		this.maritalStatus = maritalStatus;
		this.belongCommunity = belongCommunity;
		this.communityName = communityName;
		this.permanentAddress = permanentAddress;
		this.permanentState = permanentState;
		this.permanentCity = permanentCity;
		this.permanentPincode = permanentPincode;
		this.postalAddress = postalAddress;
		this.postalState = postalState;
		this.postalCity = postalCity;
		this.postalPincode = postalPincode;
		this.nccB = nccB;
		this.nccbCertificate = nccbCertificate;
		this.nccC = nccC;
		this.nccCertificate = nccCertificate;
		this.sportsman = sportsman;
		this.sportsLevel = sportsLevel;
		this.sportCertificate = sportCertificate;
		this.sonExGps = sonExGps;
		this.sonGsNumber = sonGsNumber;
		this.sonGpsRank = sonGpsRank;
		this.sonGpsName = sonGpsName;
		this.sonUnitName = sonUnitName;
		this.unitCertificate = unitCertificate;
		this.reapptCandidate = reapptCandidate;
		this.reapptCandidateGs = reapptCandidateGs;
		this.reaaptCandidateRank = reaaptCandidateRank;
		this.reapptCandidateName = reapptCandidateName;
		this.reapptCandidateUnit = reapptCandidateUnit;
		this.sonExservice = sonExservice;
		this.armyNumber = armyNumber;
		this.armyRank = armyRank;
		this.armyName = armyName;
		this.armyLastUnit = armyLastUnit;
		this.lastUnitCertificate = lastUnitCertificate;
		this.exService = exService;
		this.dischargeCertificate = dischargeCertificate;
		this.payment = payment;
		this.finalSubmit = finalSubmit;
		this.user = user;
		this.essential = essential;
		this.graduation = graduation;
		this.technical = technical;
		this.experience = experience;
		this.driving = driving;
		this.certificate = certificate;
		this.jobDetails = jobDetails;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getRecruitmentZone() {
		return recruitmentZone;
	}

	public void setRecruitmentZone(String recruitmentZone) {
		this.recruitmentZone = recruitmentZone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryCertificate() {
		return categoryCertificate;
	}

	public void setCategoryCertificate(String categoryCertificate) {
		this.categoryCertificate = categoryCertificate;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public boolean isCasualplabour() {
		return casualplabour;
	}

	public void setCasualplabour(boolean casualplabour) {
		this.casualplabour = casualplabour;
	}

	public String getCplCertificate() {
		return cplCertificate;
	}

	public void setCplCertificate(String cplCertificate) {
		this.cplCertificate = cplCertificate;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPancertificate() {
		return pancertificate;
	}

	public void setPancertificate(String pancertificate) {
		this.pancertificate = pancertificate;
	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public boolean isBelongCommunity() {
		return belongCommunity;
	}

	public void setBelongCommunity(boolean belongCommunity) {
		this.belongCommunity = belongCommunity;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentState() {
		return permanentState;
	}

	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}

	public String getPermanentCity() {
		return permanentCity;
	}

	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}

	public int getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(int permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getPostalState() {
		return postalState;
	}

	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}

	public String getPostalCity() {
		return postalCity;
	}

	public void setPostalCity(String postalCity) {
		this.postalCity = postalCity;
	}

	public int getPostalPincode() {
		return postalPincode;
	}

	public void setPostalPincode(int postalPincode) {
		this.postalPincode = postalPincode;
	}

	public boolean isNccB() {
		return nccB;
	}

	public void setNccB(boolean nccB) {
		this.nccB = nccB;
	}

	public String getNccbCertificate() {
		return nccbCertificate;
	}

	public void setNccbCertificate(String nccbCertificate) {
		this.nccbCertificate = nccbCertificate;
	}

	public boolean isNccC() {
		return nccC;
	}

	public void setNccC(boolean nccC) {
		this.nccC = nccC;
	}

	public String getNccCertificate() {
		return nccCertificate;
	}

	public void setNccCertificate(String nccCertificate) {
		this.nccCertificate = nccCertificate;
	}

	public boolean isSportsman() {
		return sportsman;
	}

	public void setSportsman(boolean sportsman) {
		this.sportsman = sportsman;
	}

	public String getSportsLevel() {
		return sportsLevel;
	}

	public void setSportsLevel(String sportsLevel) {
		this.sportsLevel = sportsLevel;
	}

	public String getSportCertificate() {
		return sportCertificate;
	}

	public void setSportCertificate(String sportCertificate) {
		this.sportCertificate = sportCertificate;
	}

	public boolean isSonExGps() {
		return sonExGps;
	}

	public void setSonExGps(boolean sonExGps) {
		this.sonExGps = sonExGps;
	}

	public String getSonGsNumber() {
		return sonGsNumber;
	}

	public void setSonGsNumber(String sonGsNumber) {
		this.sonGsNumber = sonGsNumber;
	}

	public String getSonGpsRank() {
		return sonGpsRank;
	}

	public void setSonGpsRank(String sonGpsRank) {
		this.sonGpsRank = sonGpsRank;
	}

	public String getSonGpsName() {
		return sonGpsName;
	}

	public void setSonGpsName(String sonGpsName) {
		this.sonGpsName = sonGpsName;
	}

	public String getSonUnitName() {
		return sonUnitName;
	}

	public void setSonUnitName(String sonUnitName) {
		this.sonUnitName = sonUnitName;
	}

	public String getUnitCertificate() {
		return unitCertificate;
	}

	public void setUnitCertificate(String unitCertificate) {
		this.unitCertificate = unitCertificate;
	}

	public boolean isReapptCandidate() {
		return reapptCandidate;
	}

	public void setReapptCandidate(boolean reapptCandidate) {
		this.reapptCandidate = reapptCandidate;
	}

	public String getReapptCandidateGs() {
		return reapptCandidateGs;
	}

	public void setReapptCandidateGs(String reapptCandidateGs) {
		this.reapptCandidateGs = reapptCandidateGs;
	}

	public String getReaaptCandidateRank() {
		return reaaptCandidateRank;
	}

	public void setReaaptCandidateRank(String reaaptCandidateRank) {
		this.reaaptCandidateRank = reaaptCandidateRank;
	}

	public String getReapptCandidateName() {
		return reapptCandidateName;
	}

	public void setReapptCandidateName(String reapptCandidateName) {
		this.reapptCandidateName = reapptCandidateName;
	}

	public String getReapptCandidateUnit() {
		return reapptCandidateUnit;
	}

	public void setReapptCandidateUnit(String reapptCandidateUnit) {
		this.reapptCandidateUnit = reapptCandidateUnit;
	}

	public boolean isSonExservice() {
		return sonExservice;
	}

	public void setSonExservice(boolean sonExservice) {
		this.sonExservice = sonExservice;
	}

	public String getArmyNumber() {
		return armyNumber;
	}

	public void setArmyNumber(String armyNumber) {
		this.armyNumber = armyNumber;
	}

	public String getArmyRank() {
		return armyRank;
	}

	public void setArmyRank(String armyRank) {
		this.armyRank = armyRank;
	}

	public String getArmyName() {
		return armyName;
	}

	public void setArmyName(String armyName) {
		this.armyName = armyName;
	}

	public String getArmyLastUnit() {
		return armyLastUnit;
	}

	public void setArmyLastUnit(String armyLastUnit) {
		this.armyLastUnit = armyLastUnit;
	}

	public String getLastUnitCertificate() {
		return lastUnitCertificate;
	}

	public void setLastUnitCertificate(String lastUnitCertificate) {
		this.lastUnitCertificate = lastUnitCertificate;
	}

	public boolean isExService() {
		return exService;
	}

	public void setExService(boolean exService) {
		this.exService = exService;
	}

	public String getDischargeCertificate() {
		return dischargeCertificate;
	}

	public void setDischargeCertificate(String dischargeCertificate) {
		this.dischargeCertificate = dischargeCertificate;
	}

	public boolean isPayment() {
		return payment;
	}

	public void setPayment(boolean payment) {
		this.payment = payment;
	}

	public boolean isFinalSubmit() {
		return finalSubmit;
	}

	public void setFinalSubmit(boolean finalSubmit) {
		this.finalSubmit = finalSubmit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EssentialQualification getEssential() {
		return essential;
	}

	public void setEssential(EssentialQualification essential) {
		this.essential = essential;
	}

	public GraduationQualification getGraduation() {
		return graduation;
	}

	public void setGraduation(GraduationQualification graduation) {
		this.graduation = graduation;
	}

	public TechnicalQualification getTechnical() {
		return technical;
	}

	public void setTechnical(TechnicalQualification technical) {
		this.technical = technical;
	}

	public OrganisationExperience getExperience() {
		return experience;
	}

	public void setExperience(OrganisationExperience experience) {
		this.experience = experience;
	}

	public DrivingLicense getDriving() {
		return driving;
	}

	public void setDriving(DrivingLicense driving) {
		this.driving = driving;
	}

	public UploadCertificate getCertificate() {
		return certificate;
	}

	public void setCertificate(UploadCertificate certificate) {
		this.certificate = certificate;
	}

	public JobDetails getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(JobDetails jobDetails) {
		this.jobDetails = jobDetails;
	}
	

	@Override
	public String toString() {
		return "PersonalDetails [id=" + id + ", registrationId=" + registrationId + ", recruitmentZone="
				+ recruitmentZone + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", fatherName=" + fatherName + ", motherName=" + motherName + ", category=" + category
				+ ", categoryCertificate=" + categoryCertificate + ", dateOfBirth=" + dateOfBirth + ", age=" + age
				+ ", gender=" + gender + ", mobileNumber=" + mobileNumber + ", emailAddress=" + emailAddress
				+ ", nationality=" + nationality + ", casualplabour=" + casualplabour + ", cplCertificate="
				+ cplCertificate + ", panNumber=" + panNumber + ", pancertificate=" + pancertificate
				+ ", aadhaarNumber=" + aadhaarNumber + ", maritalStatus=" + maritalStatus + ", belongCommunity="
				+ belongCommunity + ", communityName=" + communityName + ", permanentAddress=" + permanentAddress
				+ ", permanentState=" + permanentState + ", permanentCity=" + permanentCity + ", permanentPincode="
				+ permanentPincode + ", postalAddress=" + postalAddress + ", postalState=" + postalState
				+ ", postalCity=" + postalCity + ", postalPincode=" + postalPincode + ", nccB=" + nccB
				+ ", nccbCertificate=" + nccbCertificate + ", nccC=" + nccC + ", nccCertificate=" + nccCertificate
				+ ", sportsman=" + sportsman + ", sportsLevel=" + sportsLevel + ", sportCertificate=" + sportCertificate
				+ ", sonExGps=" + sonExGps + ", sonGsNumber=" + sonGsNumber + ", sonGpsRank=" + sonGpsRank
				+ ", sonGpsName=" + sonGpsName + ", sonUnitName=" + sonUnitName + ", unitCertificate=" + unitCertificate
				+ ", reapptCandidate=" + reapptCandidate + ", reapptCandidateGs=" + reapptCandidateGs
				+ ", reaaptCandidateRank=" + reaaptCandidateRank + ", reapptCandidateName=" + reapptCandidateName
				+ ", reapptCandidateUnit=" + reapptCandidateUnit + ", sonExservice=" + sonExservice + ", armyNumber="
				+ armyNumber + ", armyRank=" + armyRank + ", armyName=" + armyName + ", armyLastUnit=" + armyLastUnit
				+ ", lastUnitCertificate=" + lastUnitCertificate + ", exService=" + exService
				+ ", dischargeCertificate=" + dischargeCertificate + ", payment=" + payment + ", finalSubmit="
				+ finalSubmit + ", user=" + user + ", essential=" + essential + ", graduation=" + graduation
				+ ", technical=" + technical + ", experience=" + experience + ", driving=" + driving + ", certificate="
				+ certificate + ", jobDetails=" + jobDetails + "]";
	}

	
	
	
			
}
