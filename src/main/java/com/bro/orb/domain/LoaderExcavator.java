package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_driving_loaderexcavator")

public class LoaderExcavator implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "licence_number_oeg")
	private boolean licenceNumberOegView;

	@Column(name = "issue_date_oeg")
	private boolean issueDateOegView;

	@Column(name = "valid_upto_oeg")
	private boolean validUptoOegView;

	@Column(name = "issuing_authority_oeg")
	private boolean issuingAuthorityOegView;

	@Column(name = "oeg_experience")
	private boolean oegExperienceView;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public LoaderExcavator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoaderExcavator(int id, boolean licenceNumberOegView, boolean issueDateOegView, boolean validUptoOegView,
			boolean issuingAuthorityOegView, boolean oegExperienceView, RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.licenceNumberOegView = licenceNumberOegView;
		this.issueDateOegView = issueDateOegView;
		this.validUptoOegView = validUptoOegView;
		this.issuingAuthorityOegView = issuingAuthorityOegView;
		this.oegExperienceView = oegExperienceView;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLicenceNumberOegView() {
		return licenceNumberOegView;
	}

	public void setLicenceNumberOegView(boolean licenceNumberOegView) {
		this.licenceNumberOegView = licenceNumberOegView;
	}

	public boolean isIssueDateOegView() {
		return issueDateOegView;
	}

	public void setIssueDateOegView(boolean issueDateOegView) {
		this.issueDateOegView = issueDateOegView;
	}

	public boolean isValidUptoOegView() {
		return validUptoOegView;
	}

	public void setValidUptoOegView(boolean validUptoOegView) {
		this.validUptoOegView = validUptoOegView;
	}

	public boolean isIssuingAuthorityOegView() {
		return issuingAuthorityOegView;
	}

	public void setIssuingAuthorityOegView(boolean issuingAuthorityOegView) {
		this.issuingAuthorityOegView = issuingAuthorityOegView;
	}

	public boolean isOegExperienceView() {
		return oegExperienceView;
	}

	public void setOegExperienceView(boolean oegExperienceView) {
		this.oegExperienceView = oegExperienceView;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "LoaderExcavator [id=" + id + ", licenceNumberOegView=" + licenceNumberOegView + ", issueDateOegView="
				+ issueDateOegView + ", validUptoOegView=" + validUptoOegView + ", issuingAuthorityOegView="
				+ issuingAuthorityOegView + ", oegExperienceView=" + oegExperienceView + ", registrationViewBean="
				+ registrationViewBean + "]";
	}


}
