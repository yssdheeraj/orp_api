package com.bro.orb.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_job_form_details")
public class RegistrationViewBean extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "ncc_b_certificate")
	private boolean nccbCertificateForm;
	@Column(name = "ncc_b_upload")
	private boolean nccbUploadForm;
	@Column(name = "ncc_c_certificate")
	private boolean nccCertificateForm;
	@Column(name = "ncc_c_upload")
	private boolean nccCUploadForm;
	@Column(name = "sport_certificate")
	private boolean sportCertificateForm;
	@Column(name = "sport_upload")
	private boolean sportUploadForm;
	@Column(name = "employment_period")
	private boolean employmentPeriodForm;
	@Column(name = "cin_number")
	private boolean cinNumberForm;
	@Column(name = "regtin_number")
	private boolean regtinNumberForm;
	@Column(name = "company_name")
	private boolean companyNameForm;
	@Column(name = "work_nature")
	private boolean workNatureForm;
	@Column(name = "monthly_salary")
	private boolean monthlySalaryForm;
	@Column(name = "temporary_permanent")
	private boolean temporaryPermanentForm;
	@Column(name = "experience_certificate")
	private boolean experienceCertificateForm;
	 @Column(name = "essential_qualification")
	 private boolean essentialQualificationForm;
	 @Column(name = "high_school")
	 private boolean highSchoolForm;
	@Column(name = "intermediate")
	private boolean intermediateForm;
	@Column(name = "typing")
	private boolean typingForm;
	@Column(name = "hindi_typing")
	private boolean hindiTypingForm;
	@Column(name = "english_typing")
	private boolean englishTypingForm;
	
	@Column(name = "stenography")
	private boolean stenographyForm;
	@Column(name = "hindi_stenography")
	private boolean hindiStenographyForm;
	@Column(name = "english_stenography")
	private boolean englishStenographyForm;
	
	@Column(name = "other_qualification")
	private boolean otherQualificationForm;
	@Column(name = "graduation_form")
	private boolean graduationForm;
	@Column(name = "post_graduation_form")
	private boolean postGraduationForm;
	@Column(name = "technical_form")
	private boolean technicalForm;
	@Column(name = "degree_form")
	private boolean degreeForm;
	@Column(name = "iti_form")
	private boolean itiForm;
	@Column(name = "diploma_form")
	private boolean diplomaForm;
	@Column(name = "driving_license")
	private boolean drivingLicenseForm;
	@Column(name = "driving_license_lmv")
	private boolean drivingLicenseLmvForm;
	@Column(name = "driving_license_hgmv")
	private boolean drivingLicenseHgmvForm;
	@Column(name = "driving_licence_road")
	private boolean drivingLicenceRoadForm;
	@Column(name = "driving_licence_oeg")
	private boolean drivingLicenceOegForm;
	@Column(name = "experience_form")
	private boolean experienceForm;
	@Column(name = "sports_upload_form")
	private boolean sportsUploadForm;
	@Column(name = "driving_license_View")
	private boolean drivingUploadView;
	@Column(name = "driving_experience_view")
	private boolean drivingExpUploadView;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private JobDetails jobDetails;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "degree_id", referencedColumnName = "id")
	private Degree degree;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "iti_id", referencedColumnName = "id")
	private Iti iti;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "diploma_id", referencedColumnName = "id")
	private Diploma diploma;

	 @OneToOne(cascade = CascadeType.ALL)
	 @JoinColumn(name = "high_id", referencedColumnName = "id")
	 private HighSchool high;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inter_id", referencedColumnName = "id")
	private Intermediate inter;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ug_id", referencedColumnName = "id")
	private Graduation graduation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pg_id", referencedColumnName = "id")
	private PostGraduation pgraduation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "lmv_id", referencedColumnName = "id")
	private DrivingLmv dlmv;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "hgmv_id", referencedColumnName = "id")
	private DrivingHgmv dhgmv;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "road_id", referencedColumnName = "id")
	private DrivingRoadRoller road;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "oeg_id", referencedColumnName = "id")
	private LoaderExcavator oeg;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "zone_id", referencedColumnName = "id")
	private RecruitmentZone zone;

	/*
	 * @JoinTable
	 * 
	 * @OneToMany(mappedBy = "viewBean")
	 * 
	 * @JoinColumn(name="jobAmount",referencedColumnName = "id") private
	 * Set<JobAmountDetails> jobAmountDetils;
	 */

	@OneToMany(mappedBy = "viewBean")
	private List<JobAmountDetails> jobAmountDetails;

	public RegistrationViewBean() {
		super();
	}

	public RegistrationViewBean(int id, boolean nccbCertificateForm, boolean nccbUploadForm, boolean nccCertificateForm,
			boolean nccCUploadForm, boolean sportCertificateForm, boolean sportUploadForm, boolean employmentPeriodForm,
			boolean cinNumberForm, boolean regtinNumberForm, boolean companyNameForm, boolean workNatureForm,
			boolean monthlySalaryForm, boolean temporaryPermanentForm, boolean experienceCertificateForm,
			boolean essentialQualificationForm, boolean highSchoolForm, boolean intermediateForm, boolean typingForm, boolean hindiTypingForm, boolean englishTypingForm,
			boolean stenographyForm, boolean hindiStenographyForm, boolean englishStenographyForm,
			boolean otherQualificationForm, boolean graduationForm, boolean postGraduationForm, boolean technicalForm,
			boolean degreeForm, boolean itiForm, boolean diplomaForm, boolean drivingLicenseForm,
			boolean drivingLicenseLmvForm, boolean drivingLicenseHgmvForm, boolean drivingLicenceRoadForm,
			boolean drivingLicenceOegForm, boolean experienceForm, boolean sportsUploadForm, boolean drivingUploadView,
			boolean drivingExpUploadView, JobDetails jobDetails, Degree degree, Iti iti, Diploma diploma,
			Intermediate inter, Graduation graduation, PostGraduation pgraduation, DrivingLmv dlmv, DrivingHgmv dhgmv,
			DrivingRoadRoller road, LoaderExcavator oeg, RecruitmentZone zone,
			List<JobAmountDetails> jobAmountDetails) {
		super();
		this.id = id;
		this.nccbCertificateForm = nccbCertificateForm;
		this.nccbUploadForm = nccbUploadForm;
		this.nccCertificateForm = nccCertificateForm;
		this.nccCUploadForm = nccCUploadForm;
		this.sportCertificateForm = sportCertificateForm;
		this.sportUploadForm = sportUploadForm;
		this.employmentPeriodForm = employmentPeriodForm;
		this.cinNumberForm = cinNumberForm;
		this.regtinNumberForm = regtinNumberForm;
		this.companyNameForm = companyNameForm;
		this.workNatureForm = workNatureForm;
		this.monthlySalaryForm = monthlySalaryForm;
		this.temporaryPermanentForm = temporaryPermanentForm;
		this.experienceCertificateForm = experienceCertificateForm;
		this.essentialQualificationForm =essentialQualificationForm;
		this.highSchoolForm = highSchoolForm;
		this.intermediateForm = intermediateForm;
		this.typingForm = typingForm;
		this.hindiTypingForm = hindiTypingForm;
		this.englishTypingForm = englishTypingForm;
		this.stenographyForm = stenographyForm;
		this.hindiStenographyForm = hindiStenographyForm;
		this.englishStenographyForm = englishStenographyForm;
		this.otherQualificationForm = otherQualificationForm;
		this.graduationForm = graduationForm;
		this.postGraduationForm = postGraduationForm;
		this.technicalForm = technicalForm;
		this.degreeForm = degreeForm;
		this.itiForm = itiForm;
		this.diplomaForm = diplomaForm;
		this.drivingLicenseForm = drivingLicenseForm;
		this.drivingLicenseLmvForm = drivingLicenseLmvForm;
		this.drivingLicenseHgmvForm = drivingLicenseHgmvForm;
		this.drivingLicenceRoadForm = drivingLicenceRoadForm;
		this.drivingLicenceOegForm = drivingLicenceOegForm;
		this.experienceForm = experienceForm;
		this.sportsUploadForm = sportsUploadForm;
		this.drivingUploadView = drivingUploadView;
		this.drivingExpUploadView = drivingExpUploadView;
		this.jobDetails = jobDetails;
		this.degree = degree;
		this.iti = iti;
		this.diploma = diploma;
		this.inter = inter;
		this.graduation = graduation;
		this.pgraduation = pgraduation;
		this.dlmv = dlmv;
		this.dhgmv = dhgmv;
		this.road = road;
		this.oeg = oeg;
		this.zone = zone;
		this.jobAmountDetails = jobAmountDetails;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isNccbCertificateForm() {
		return nccbCertificateForm;
	}

	public void setNccbCertificateForm(boolean nccbCertificateForm) {
		this.nccbCertificateForm = nccbCertificateForm;
	}

	public boolean isNccbUploadForm() {
		return nccbUploadForm;
	}

	public void setNccbUploadForm(boolean nccbUploadForm) {
		this.nccbUploadForm = nccbUploadForm;
	}

	public boolean isNccCertificateForm() {
		return nccCertificateForm;
	}

	public void setNccCertificateForm(boolean nccCertificateForm) {
		this.nccCertificateForm = nccCertificateForm;
	}

	public boolean isNccCUploadForm() {
		return nccCUploadForm;
	}

	public void setNccCUploadForm(boolean nccCUploadForm) {
		this.nccCUploadForm = nccCUploadForm;
	}

	public boolean isSportCertificateForm() {
		return sportCertificateForm;
	}

	public void setSportCertificateForm(boolean sportCertificateForm) {
		this.sportCertificateForm = sportCertificateForm;
	}

	public boolean isSportUploadForm() {
		return sportUploadForm;
	}

	public void setSportUploadForm(boolean sportUploadForm) {
		this.sportUploadForm = sportUploadForm;
	}

	public boolean isEmploymentPeriodForm() {
		return employmentPeriodForm;
	}

	public void setEmploymentPeriodForm(boolean employmentPeriodForm) {
		this.employmentPeriodForm = employmentPeriodForm;
	}

	public boolean isCinNumberForm() {
		return cinNumberForm;
	}

	public void setCinNumberForm(boolean cinNumberForm) {
		this.cinNumberForm = cinNumberForm;
	}

	public boolean isRegtinNumberForm() {
		return regtinNumberForm;
	}

	public void setRegtinNumberForm(boolean regtinNumberForm) {
		this.regtinNumberForm = regtinNumberForm;
	}

	public boolean isCompanyNameForm() {
		return companyNameForm;
	}

	public void setCompanyNameForm(boolean companyNameForm) {
		this.companyNameForm = companyNameForm;
	}

	public boolean isWorkNatureForm() {
		return workNatureForm;
	}

	public void setWorkNatureForm(boolean workNatureForm) {
		this.workNatureForm = workNatureForm;
	}

	public boolean isMonthlySalaryForm() {
		return monthlySalaryForm;
	}

	public void setMonthlySalaryForm(boolean monthlySalaryForm) {
		this.monthlySalaryForm = monthlySalaryForm;
	}

	public boolean isTemporaryPermanentForm() {
		return temporaryPermanentForm;
	}

	public void setTemporaryPermanentForm(boolean temporaryPermanentForm) {
		this.temporaryPermanentForm = temporaryPermanentForm;
	}

	public boolean isExperienceCertificateForm() {
		return experienceCertificateForm;
	}

	public void setExperienceCertificateForm(boolean experienceCertificateForm) {
		this.experienceCertificateForm = experienceCertificateForm;
	}

	public boolean isIntermediateForm() {
		return intermediateForm;
	}

	public void setIntermediateForm(boolean intermediateForm) {
		this.intermediateForm = intermediateForm;
	}

	public boolean isTypingForm() {
		return typingForm;
	}

	public void setTypingForm(boolean typingForm) {
		this.typingForm = typingForm;
	}

	public boolean isHindiTypingForm() {
		return hindiTypingForm;
	}

	public void setHindiTypingForm(boolean hindiTypingForm) {
		this.hindiTypingForm = hindiTypingForm;
	}

	public boolean isEnglishTypingForm() {
		return englishTypingForm;
	}

	public void setEnglishTypingForm(boolean englishTypingForm) {
		this.englishTypingForm = englishTypingForm;
	}

	public boolean isStenographyForm() {
		return stenographyForm;
	}

	public void setStenographyForm(boolean stenographyForm) {
		this.stenographyForm = stenographyForm;
	}

	public boolean isHindiStenographyForm() {
		return hindiStenographyForm;
	}

	public void setHindiStenographyForm(boolean hindiStenographyForm) {
		this.hindiStenographyForm = hindiStenographyForm;
	}

	public boolean isEnglishStenographyForm() {
		return englishStenographyForm;
	}

	public void setEnglishStenographyForm(boolean englishStenographyForm) {
		this.englishStenographyForm = englishStenographyForm;
	}

	public boolean isOtherQualificationForm() {
		return otherQualificationForm;
	}

	public void setOtherQualificationForm(boolean otherQualificationForm) {
		this.otherQualificationForm = otherQualificationForm;
	}

	public boolean isGraduationForm() {
		return graduationForm;
	}

	public void setGraduationForm(boolean graduationForm) {
		this.graduationForm = graduationForm;
	}

	public boolean isPostGraduationForm() {
		return postGraduationForm;
	}

	public void setPostGraduationForm(boolean postGraduationForm) {
		this.postGraduationForm = postGraduationForm;
	}

	public boolean isTechnicalForm() {
		return technicalForm;
	}

	public void setTechnicalForm(boolean technicalForm) {
		this.technicalForm = technicalForm;
	}

	public boolean isDegreeForm() {
		return degreeForm;
	}

	public void setDegreeForm(boolean degreeForm) {
		this.degreeForm = degreeForm;
	}

	public boolean isItiForm() {
		return itiForm;
	}

	public void setItiForm(boolean itiForm) {
		this.itiForm = itiForm;
	}

	public boolean isDiplomaForm() {
		return diplomaForm;
	}

	public void setDiplomaForm(boolean diplomaForm) {
		this.diplomaForm = diplomaForm;
	}

	public boolean isDrivingLicenseForm() {
		return drivingLicenseForm;
	}

	public void setDrivingLicenseForm(boolean drivingLicenseForm) {
		this.drivingLicenseForm = drivingLicenseForm;
	}

	public boolean isDrivingLicenseLmvForm() {
		return drivingLicenseLmvForm;
	}

	public void setDrivingLicenseLmvForm(boolean drivingLicenseLmvForm) {
		this.drivingLicenseLmvForm = drivingLicenseLmvForm;
	}

	public boolean isDrivingLicenseHgmvForm() {
		return drivingLicenseHgmvForm;
	}

	public void setDrivingLicenseHgmvForm(boolean drivingLicenseHgmvForm) {
		this.drivingLicenseHgmvForm = drivingLicenseHgmvForm;
	}

	public boolean isDrivingLicenceRoadForm() {
		return drivingLicenceRoadForm;
	}

	public void setDrivingLicenceRoadForm(boolean drivingLicenceRoadForm) {
		this.drivingLicenceRoadForm = drivingLicenceRoadForm;
	}

	public boolean isDrivingLicenceOegForm() {
		return drivingLicenceOegForm;
	}

	public void setDrivingLicenceOegForm(boolean drivingLicenceOegForm) {
		this.drivingLicenceOegForm = drivingLicenceOegForm;
	}

	public boolean isExperienceForm() {
		return experienceForm;
	}

	public void setExperienceForm(boolean experienceForm) {
		this.experienceForm = experienceForm;
	}

	public boolean isSportsUploadForm() {
		return sportsUploadForm;
	}

	public void setSportsUploadForm(boolean sportsUploadForm) {
		this.sportsUploadForm = sportsUploadForm;
	}

	public boolean isDrivingUploadView() {
		return drivingUploadView;
	}

	public void setDrivingUploadView(boolean drivingUploadView) {
		this.drivingUploadView = drivingUploadView;
	}

	public boolean isDrivingExpUploadView() {
		return drivingExpUploadView;
	}

	public void setDrivingExpUploadView(boolean drivingExpUploadView) {
		this.drivingExpUploadView = drivingExpUploadView;
	}

	public JobDetails getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(JobDetails jobDetails) {
		this.jobDetails = jobDetails;
	}

	public Degree getDegree() {
		return degree;
	}

	public void setDegree(Degree degree) {
		this.degree = degree;
	}

	public Iti getIti() {
		return iti;
	}

	public void setIti(Iti iti) {
		this.iti = iti;
	}

	public Diploma getDiploma() {
		return diploma;
	}

	public void setDiploma(Diploma diploma) {
		this.diploma = diploma;
	}

	public Intermediate getInter() {
		return inter;
	}

	public void setInter(Intermediate inter) {
		this.inter = inter;
	}

	public Graduation getGraduation() {
		return graduation;
	}

	public void setGraduation(Graduation graduation) {
		this.graduation = graduation;
	}

	public PostGraduation getPgraduation() {
		return pgraduation;
	}

	public void setPgraduation(PostGraduation pgraduation) {
		this.pgraduation = pgraduation;
	}

	public DrivingLmv getDlmv() {
		return dlmv;
	}

	public void setDlmv(DrivingLmv dlmv) {
		this.dlmv = dlmv;
	}

	public DrivingHgmv getDhgmv() {
		return dhgmv;
	}

	public void setDhgmv(DrivingHgmv dhgmv) {
		this.dhgmv = dhgmv;
	}

	public DrivingRoadRoller getRoad() {
		return road;
	}

	public void setRoad(DrivingRoadRoller road) {
		this.road = road;
	}

	public LoaderExcavator getOeg() {
		return oeg;
	}

	public void setOeg(LoaderExcavator oeg) {
		this.oeg = oeg;
	}

	public RecruitmentZone getZone() {
		return zone;
	}

	public void setZone(RecruitmentZone zone) {
		this.zone = zone;
	}

	public List<JobAmountDetails> getJobAmountDetails() {
		return jobAmountDetails;
	}

	public void setJobAmountDetails(List<JobAmountDetails> jobAmountDetails) {
		this.jobAmountDetails = jobAmountDetails;
	}

	public boolean isEssentialQualificationForm() {
		return essentialQualificationForm;
	}

	public void setEssentialQualificationForm(boolean essentialQualificationForm) {
		this.essentialQualificationForm = essentialQualificationForm;
	}

	public boolean isHighSchoolForm() {
		return highSchoolForm;
	}

	public void setHighSchoolForm(boolean highSchoolForm) {
		this.highSchoolForm = highSchoolForm;
	}

	public HighSchool getHigh() {
		return high;
	}

	public void setHigh(HighSchool high) {
		this.high = high;
	}

	


	
}
