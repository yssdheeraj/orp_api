package com.bro.orb.domain;

import java.util.Date;
import java.util.List;

import javax.mail.Multipart;

public class JobFormFields {

	private boolean recruitmentZoneForm;
	private boolean nccbCertificateForm;
	private boolean nccbUploadForm;
	private boolean nccCertificateForm;
	private boolean nccCUploadForm;
	private boolean sportCertificateForm;
	private boolean sportUploadForm;
	private boolean companyNameForm;
	private boolean employmentPeriodForm;
	private boolean cinNumberForm;
	private boolean regtinNumberForm;
	private boolean workNatureForm;
	private boolean monthlySalaryForm;
	private boolean temporaryPermanentForm;
	private boolean experienceCertificateForm;
	private boolean essentialQualificationForm;
	private boolean highSchoolForm;
	private boolean intermediateForm;
	
	
//	private boolean additionalForm;
	private boolean typingForm;
	private boolean hindiTypingForm;
	private boolean englishTypingForm;
	private boolean stenographyForm;
	private boolean hindiStenographyForm;
	private boolean englishStenographyForm;
	private boolean otherQualificationForm;
	private boolean graduationForm;
	private boolean postGraduationForm;
	private boolean technicalForm;
	private boolean degreeForm;
	private boolean diplomaForm;
	private boolean itiForm;
	private boolean drivingLicenseForm;
	private boolean drivingLicenseLmvForm;
	private boolean drivingLicenseHgmvForm;
	private boolean drivingLicenceRoadForm;
	private boolean drivingLicenceOegForm;
	private boolean drivingUploadView;
	private boolean drivingExpUploadView;
	private boolean experienceForm;
	private boolean sportsUploadForm;
	private boolean rishikeshZone;
	private boolean puneZone;
	private boolean tezpurZone;
	private boolean degreeYearView;
	private boolean degreeInstitutionView;
	private boolean degreeSpecializationView;
	private boolean degreeCourseTypeView;
	private boolean degreeTotalMarksView;
	private boolean degreeObtainarksView;
	private boolean degreePercentageView;
	private boolean degreeUniversityView;
	private boolean degreeCertificateView;
	private int degreeMinPercent;
	private boolean degreeBranchView;
	private boolean itiYearView;
	private boolean itiInstitutionView;
	private boolean itiSpecializationView;
	private boolean itiCourseTypeView;
	private boolean itiTotalMarksView;
	private boolean itiObtainarksView;
	private boolean itiPercentageView;
	private boolean itiUniversityView;
	private boolean itiCertificateView;
	private int itiMinPercent;
	private boolean itiBranchView;
	private boolean diplomaYearView;
	private boolean diplomaInstitutionView;
	private boolean diplomaSpecializationView;
	private boolean diplomaCourseTypeView;
	private boolean diplomaTotalMarksView;
	private boolean diplomaObtainarksView;
	private boolean diplomaPercentageView;
	private boolean diplomaUniversityView;
	private boolean diplomaCertificateView;
	private boolean diplomaBranchView;
	private int diplomaMinPercent;
	private boolean ugYearView;
	private boolean ugInstitutionView;
	private boolean ugSpecializationView;
	private boolean ugCourseTypeView;
	private boolean ugTotalMarksView;
	private boolean ugObtainarksView;
	private boolean ugPercentageView;
	private boolean ugCourseView;
	private boolean ugUniversityView;
	private boolean ugCertificateView;
	private int ugMinPercent;
	private boolean pgYearView;
	private boolean pgInstitutionView;
	private boolean pgSpecializationView;
	private boolean pgCourseTypeView;
	private boolean pgTotalMarksView;
	private boolean pgObtainarksView;
	private boolean pgPercentageView;
	private boolean pgCourseView;
	private boolean pgUniversityView;
	private boolean pgCertificateView;
	private int pgMinPercent;
	private boolean licenceNumberHgmvView;
	private boolean issueDateHgmvView;
	private boolean validUptoHgmvView;
	private boolean issuingAuthorityHgmvView;
	private boolean issueDateLmvView;
	private boolean validUptoLmvView;
	private boolean issuingAuthorityLmvView;
	private boolean licenceNumberLmvView;
	private boolean licenceNumberRoadView;
	private boolean issueDateRoadView;
	private boolean validUptoRoadView;
	private boolean issuingAuthorityRoadView;
	private boolean licenceNumberOegView;
	private boolean issueDateOegView;
	private boolean validUptoOegView;
	private boolean issuingAuthorityOegView;


	private boolean lmvExperienceView;
	private boolean highYearView;
	private boolean highInstitutionView;
	private boolean highSpecializationView;
	private boolean highCourseTypeView;
	private boolean highTotalMarksView;
	private boolean highObtainarksView;
	private boolean highPercentageView;
	private boolean highBoardView;
	private boolean highCertificateView;
	private boolean highGradeView;
	private int highMinPercent;
	private boolean interYearView;
	private boolean interInstitutionView;
	private boolean interSpecializationView;
	private boolean interCourseTypeView;
	private boolean interTotalMarksView;
	private boolean interObtainarksView;
	private boolean interPercentageView;
	private boolean interBoardView;
	private boolean interGradeView;
	private boolean typingview;
	private boolean stenographyview;
	private boolean interCertificateView;
	private boolean additionalCertificateView;
	private boolean firstaidCertificateView;
	private int interMinPercent;
	private int ageRelax;
	private int percentRelax;
	private RegistrationViewBean viewBea;	
	private String postName;
	private String postProfile;
	private int jobId;
	// private Multipart postDetailsDocument;
	private Date activeDate;
	private Date expiryDate;
	private Date feeSubmitDate;
	private Date feeSubmitLastDate;
	private List<JobAmountDetails> categoryAmount;

	
	
	public RegistrationViewBean getViewBea() {
		return viewBea;
	}

	public void setViewBea(RegistrationViewBean viewBea) {
		this.viewBea = viewBea;
	}

	public boolean isRecruitmentZoneForm() {
		return recruitmentZoneForm;
	}

	public void setRecruitmentZoneForm(boolean recruitmentZoneForm) {
		this.recruitmentZoneForm = recruitmentZoneForm;
	}

	public boolean isNccbCertificateForm() {
		return nccbCertificateForm;
	}

	public void setNccbCertificateForm(boolean nccbCertificateForm) {
		this.nccbCertificateForm = nccbCertificateForm;
	}

	public boolean isNccbUploadForm() {
		return nccbUploadForm;
	}

	public void setNccbUploadForm(boolean nccbUploadForm) {
		this.nccbUploadForm = nccbUploadForm;
	}

	public boolean isNccCertificateForm() {
		return nccCertificateForm;
	}

	public void setNccCertificateForm(boolean nccCertificateForm) {
		this.nccCertificateForm = nccCertificateForm;
	}

	public boolean isNccCUploadForm() {
		return nccCUploadForm;
	}

	public void setNccCUploadForm(boolean nccCUploadForm) {
		this.nccCUploadForm = nccCUploadForm;
	}

	public boolean isSportCertificateForm() {
		return sportCertificateForm;
	}

	public void setSportCertificateForm(boolean sportCertificateForm) {
		this.sportCertificateForm = sportCertificateForm;
	}

	public boolean isSportUploadForm() {
		return sportUploadForm;
	}

	public void setSportUploadForm(boolean sportUploadForm) {
		this.sportUploadForm = sportUploadForm;
	}

	public boolean isCompanyNameForm() {
		return companyNameForm;
	}

	public void setCompanyNameForm(boolean companyNameForm) {
		this.companyNameForm = companyNameForm;
	}

	public boolean isEmploymentPeriodForm() {
		return employmentPeriodForm;
	}

	public void setEmploymentPeriodForm(boolean employmentPeriodForm) {
		this.employmentPeriodForm = employmentPeriodForm;
	}

	public boolean isCinNumberForm() {
		return cinNumberForm;
	}

	public void setCinNumberForm(boolean cinNumberForm) {
		this.cinNumberForm = cinNumberForm;
	}

	public boolean isWorkNatureForm() {
		return workNatureForm;
	}

	public void setWorkNatureForm(boolean workNatureForm) {
		this.workNatureForm = workNatureForm;
	}

	public boolean isMonthlySalaryForm() {
		return monthlySalaryForm;
	}

	public void setMonthlySalaryForm(boolean monthlySalaryForm) {
		this.monthlySalaryForm = monthlySalaryForm;
	}

	public boolean isTemporaryPermanentForm() {
		return temporaryPermanentForm;
	}

	public void setTemporaryPermanentForm(boolean temporaryPermanentForm) {
		this.temporaryPermanentForm = temporaryPermanentForm;
	}

	public boolean isExperienceCertificateForm() {
		return experienceCertificateForm;
	}

	public void setExperienceCertificateForm(boolean experienceCertificateForm) {
		this.experienceCertificateForm = experienceCertificateForm;
	}

	public List<JobAmountDetails> getCategoryAmount() {
		return categoryAmount;
	}

	public void setCategoryAmount(List<JobAmountDetails> categoryAmount) {
		this.categoryAmount = categoryAmount;
	}

	public boolean isEssentialQualificationForm() {
		return essentialQualificationForm;
	}

	public void setEssentialQualificationForm(boolean essentialQualificationForm) {
		this.essentialQualificationForm = essentialQualificationForm;
	}

	public boolean isHighSchoolForm() {
		return highSchoolForm;
	}

	public void setHighSchoolForm(boolean highSchoolForm) {
	this.highSchoolForm = highSchoolForm;
	}

	public boolean isIntermediateForm() {
		return intermediateForm;
	}

	public void setIntermediateForm(boolean intermediateForm) {
		this.intermediateForm = intermediateForm;
	}

	public boolean isTypingForm() {
		return typingForm;
	}

	public boolean isDrivingUploadView() {
		return drivingUploadView;
	}

	public void setDrivingUploadView(boolean drivingUploadView) {
		this.drivingUploadView = drivingUploadView;
	}

	public void setTypingForm(boolean typingForm) {
		this.typingForm = typingForm;
	}

	public boolean isHindiTypingForm() {
		return hindiTypingForm;
	}

	public void setHindiTypingForm(boolean hindiTypingForm) {
		this.hindiTypingForm = hindiTypingForm;
	}

	public boolean isEnglishTypingForm() {
		return englishTypingForm;
	}

	public void setEnglishTypingForm(boolean englishTypingForm) {
		this.englishTypingForm = englishTypingForm;
	}

	public boolean isOtherQualificationForm() {
		return otherQualificationForm;
	}

	public void setOtherQualificationForm(boolean otherQualificationForm) {
		this.otherQualificationForm = otherQualificationForm;
	}

	public boolean isGraduationForm() {
		return graduationForm;
	}

	public void setGraduationForm(boolean graduationForm) {
		this.graduationForm = graduationForm;
	}

	public boolean isPostGraduationForm() {
		return postGraduationForm;
	}

	public void setPostGraduationForm(boolean postGraduationForm) {
		this.postGraduationForm = postGraduationForm;
	}

	public boolean isTechnicalForm() {
		return technicalForm;
	}

	public void setTechnicalForm(boolean technicalForm) {
		this.technicalForm = technicalForm;
	}

	public boolean isDegreeForm() {
		return degreeForm;
	}

	public void setDegreeForm(boolean degreeForm) {
		this.degreeForm = degreeForm;
	}

	public boolean isDiplomaForm() {
		return diplomaForm;
	}

	public void setDiplomaForm(boolean diplomaForm) {
		this.diplomaForm = diplomaForm;
	}

	public boolean isDrivingLicenseForm() {
		return drivingLicenseForm;
	}

	public void setDrivingLicenseForm(boolean drivingLicenseForm) {
		this.drivingLicenseForm = drivingLicenseForm;
	}

	public boolean isDrivingLicenseLmvForm() {
		return drivingLicenseLmvForm;
	}

	public void setDrivingLicenseLmvForm(boolean drivingLicenseLmvForm) {
		this.drivingLicenseLmvForm = drivingLicenseLmvForm;
	}

	public boolean isDrivingLicenseHgmvForm() {
		return drivingLicenseHgmvForm;
	}

	public void setDrivingLicenseHgmvForm(boolean drivingLicenseHgmvForm) {
		this.drivingLicenseHgmvForm = drivingLicenseHgmvForm;
	}

	public boolean isExperienceForm() {
		return experienceForm;
	}

	public void setExperienceForm(boolean experienceForm) {
		this.experienceForm = experienceForm;
	}

	public int getAgeRelax() {
		return ageRelax;
	}

	public void setAgeRelax(int ageRelax) {
		this.ageRelax = ageRelax;
	}

	public int getPercentRelax() {
		return percentRelax;
	}

	public void setPercentRelax(int percentRelax) {
		this.percentRelax = percentRelax;
	}

	public boolean isSportsUploadForm() {
		return sportsUploadForm;
	}

	public void setSportsUploadForm(boolean sportsUploadForm) {
		this.sportsUploadForm = sportsUploadForm;
	}

	public boolean isRishikeshZone() {
		return rishikeshZone;
	}

	public void setRishikeshZone(boolean rishikeshZone) {
		this.rishikeshZone = rishikeshZone;
	}

	public boolean isPuneZone() {
		return puneZone;
	}

	public void setPuneZone(boolean puneZone) {
		this.puneZone = puneZone;
	}

	public boolean isTezpurZone() {
		return tezpurZone;
	}

	public void setTezpurZone(boolean tezpurZone) {
		this.tezpurZone = tezpurZone;
	}

	public boolean isDegreeYearView() {
		return degreeYearView;
	}

	public void setDegreeYearView(boolean degreeYearView) {
		this.degreeYearView = degreeYearView;
	}

	public boolean isDegreeInstitutionView() {
		return degreeInstitutionView;
	}

	public void setDegreeInstitutionView(boolean degreeInstitutionView) {
		this.degreeInstitutionView = degreeInstitutionView;
	}

	public boolean isDegreeSpecializationView() {
		return degreeSpecializationView;
	}

	public void setDegreeSpecializationView(boolean degreeSpecializationView) {
		this.degreeSpecializationView = degreeSpecializationView;
	}

	public boolean isDegreeCourseTypeView() {
		return degreeCourseTypeView;
	}

	public void setDegreeCourseTypeView(boolean degreeCourseTypeView) {
		this.degreeCourseTypeView = degreeCourseTypeView;
	}

	public boolean isDegreeTotalMarksView() {
		return degreeTotalMarksView;
	}

	public void setDegreeTotalMarksView(boolean degreeTotalMarksView) {
		this.degreeTotalMarksView = degreeTotalMarksView;
	}

	public boolean isDegreeObtainarksView() {
		return degreeObtainarksView;
	}

	public void setDegreeObtainarksView(boolean degreeObtainarksView) {
		this.degreeObtainarksView = degreeObtainarksView;
	}

	public boolean isDegreePercentageView() {
		return degreePercentageView;
	}

	public void setDegreePercentageView(boolean degreePercentageView) {
		this.degreePercentageView = degreePercentageView;
	}

	public boolean isDegreeUniversityView() {
		return degreeUniversityView;
	}

	public void setDegreeUniversityView(boolean degreeUniversityView) {
		this.degreeUniversityView = degreeUniversityView;
	}

	public boolean isDegreeCertificateView() {
		return degreeCertificateView;
	}

	public void setDegreeCertificateView(boolean degreeCertificateView) {
		this.degreeCertificateView = degreeCertificateView;
	}

	public boolean isDiplomaYearView() {
		return diplomaYearView;
	}

	public void setDiplomaYearView(boolean diplomaYearView) {
		this.diplomaYearView = diplomaYearView;
	}

	public boolean isDiplomaInstitutionView() {
		return diplomaInstitutionView;
	}

	public void setDiplomaInstitutionView(boolean diplomaInstitutionView) {
		this.diplomaInstitutionView = diplomaInstitutionView;
	}

	public boolean isDiplomaSpecializationView() {
		return diplomaSpecializationView;
	}

	public void setDiplomaSpecializationView(boolean diplomaSpecializationView) {
		this.diplomaSpecializationView = diplomaSpecializationView;
	}

	public boolean isDiplomaCourseTypeView() {
		return diplomaCourseTypeView;
	}

	public void setDiplomaCourseTypeView(boolean diplomaCourseTypeView) {
		this.diplomaCourseTypeView = diplomaCourseTypeView;
	}

	public boolean isDiplomaTotalMarksView() {
		return diplomaTotalMarksView;
	}

	public void setDiplomaTotalMarksView(boolean diplomaTotalMarksView) {
		this.diplomaTotalMarksView = diplomaTotalMarksView;
	}

	public boolean isDiplomaObtainarksView() {
		return diplomaObtainarksView;
	}

	public void setDiplomaObtainarksView(boolean diplomaObtainarksView) {
		this.diplomaObtainarksView = diplomaObtainarksView;
	}

	public boolean isDiplomaPercentageView() {
		return diplomaPercentageView;
	}

	public void setDiplomaPercentageView(boolean diplomaPercentageView) {
		this.diplomaPercentageView = diplomaPercentageView;
	}

	public boolean isDiplomaUniversityView() {
		return diplomaUniversityView;
	}

	public void setDiplomaUniversityView(boolean diplomaUniversityView) {
		this.diplomaUniversityView = diplomaUniversityView;
	}

	public boolean isDiplomaCertificateView() {
		return diplomaCertificateView;
	}

	public void setDiplomaCertificateView(boolean diplomaCertificateView) {
		this.diplomaCertificateView = diplomaCertificateView;
	}

	public boolean isUgYearView() {
		return ugYearView;
	}

	public void setUgYearView(boolean ugYearView) {
		this.ugYearView = ugYearView;
	}

	public boolean isUgInstitutionView() {
		return ugInstitutionView;
	}

	public void setUgInstitutionView(boolean ugInstitutionView) {
		this.ugInstitutionView = ugInstitutionView;
	}

	public boolean isUgSpecializationView() {
		return ugSpecializationView;
	}

	public void setUgSpecializationView(boolean ugSpecializationView) {
		this.ugSpecializationView = ugSpecializationView;
	}

	public boolean isUgCourseTypeView() {
		return ugCourseTypeView;
	}

	public void setUgCourseTypeView(boolean ugCourseTypeView) {
		this.ugCourseTypeView = ugCourseTypeView;
	}

	public boolean isUgTotalMarksView() {
		return ugTotalMarksView;
	}

	public void setUgTotalMarksView(boolean ugTotalMarksView) {
		this.ugTotalMarksView = ugTotalMarksView;
	}

	public boolean isUgObtainarksView() {
		return ugObtainarksView;
	}

	public void setUgObtainarksView(boolean ugObtainarksView) {
		this.ugObtainarksView = ugObtainarksView;
	}

	public boolean isUgPercentageView() {
		return ugPercentageView;
	}

	public void setUgPercentageView(boolean ugPercentageView) {
		this.ugPercentageView = ugPercentageView;
	}

	public boolean isUgCourseView() {
		return ugCourseView;
	}

	public void setUgCourseView(boolean ugCourseView) {
		this.ugCourseView = ugCourseView;
	}

	public boolean isUgUniversityView() {
		return ugUniversityView;
	}

	public void setUgUniversityView(boolean ugUniversityView) {
		this.ugUniversityView = ugUniversityView;
	}

	public boolean isUgCertificateView() {
		return ugCertificateView;
	}

	public void setUgCertificateView(boolean ugCertificateView) {
		this.ugCertificateView = ugCertificateView;
	}

	public boolean isPgYearView() {
		return pgYearView;
	}

	public void setPgYearView(boolean pgYearView) {
		this.pgYearView = pgYearView;
	}

	public boolean isPgInstitutionView() {
		return pgInstitutionView;
	}

	public void setPgInstitutionView(boolean pgInstitutionView) {
		this.pgInstitutionView = pgInstitutionView;
	}

	public boolean isPgSpecializationView() {
		return pgSpecializationView;
	}

	public void setPgSpecializationView(boolean pgSpecializationView) {
		this.pgSpecializationView = pgSpecializationView;
	}

	public boolean isPgCourseTypeView() {
		return pgCourseTypeView;
	}

	public void setPgCourseTypeView(boolean pgCourseTypeView) {
		this.pgCourseTypeView = pgCourseTypeView;
	}

	public boolean isPgTotalMarksView() {
		return pgTotalMarksView;
	}

	public void setPgTotalMarksView(boolean pgTotalMarksView) {
		this.pgTotalMarksView = pgTotalMarksView;
	}

	public boolean isPgObtainarksView() {
		return pgObtainarksView;
	}

	public void setPgObtainarksView(boolean pgObtainarksView) {
		this.pgObtainarksView = pgObtainarksView;
	}

	public boolean isPgPercentageView() {
		return pgPercentageView;
	}

	public void setPgPercentageView(boolean pgPercentageView) {
		this.pgPercentageView = pgPercentageView;
	}

	public boolean isPgCourseView() {
		return pgCourseView;
	}

	public void setPgCourseView(boolean pgCourseView) {
		this.pgCourseView = pgCourseView;
	}

	public boolean isPgUniversityView() {
		return pgUniversityView;
	}

	public int getDegreeMinPercent() {
		return degreeMinPercent;
	}

	public void setDegreeMinPercent(int degreeMinPercent) {
		this.degreeMinPercent = degreeMinPercent;
	}

	public int getDiplomaMinPercent() {
		return diplomaMinPercent;
	}

	public void setDiplomaMinPercent(int diplomaMinPercent) {
		this.diplomaMinPercent = diplomaMinPercent;
	}

	public int getUgMinPercent() {
		return ugMinPercent;
	}

	public void setUgMinPercent(int ugMinPercent) {
		this.ugMinPercent = ugMinPercent;
	}

	public int getPgMinPercent() {
		return pgMinPercent;
	}

	public void setPgMinPercent(int pgMinPercent) {
		this.pgMinPercent = pgMinPercent;
	}


	public boolean isAdditionalCertificateView() {
		return additionalCertificateView;
	}

	public void setAdditionalCertificateView(boolean additionalCertificateView) {
		this.additionalCertificateView = additionalCertificateView;
	}

	public int getInterMinPercent() {
		return interMinPercent;
	}

	public void setInterMinPercent(int interMinPercent) {
		this.interMinPercent = interMinPercent;
	}

	public void setPgUniversityView(boolean pgUniversityView) {
		this.pgUniversityView = pgUniversityView;
	}

	public boolean isPgCertificateView() {
		return pgCertificateView;
	}

	public void setPgCertificateView(boolean pgCertificateView) {
		this.pgCertificateView = pgCertificateView;
	}

	public boolean isLicenceNumberHgmvView() {
		return licenceNumberHgmvView;
	}

	public void setLicenceNumberHgmvView(boolean licenceNumberHgmvView) {
		this.licenceNumberHgmvView = licenceNumberHgmvView;
	}

	public boolean isIssueDateHgmvView() {
		return issueDateHgmvView;
	}

	public void setIssueDateHgmvView(boolean issueDateHgmvView) {
		this.issueDateHgmvView = issueDateHgmvView;
	}

	public boolean isValidUptoHgmvView() {
		return validUptoHgmvView;
	}

	public void setValidUptoHgmvView(boolean validUptoHgmvView) {
		this.validUptoHgmvView = validUptoHgmvView;
	}

	public boolean isIssuingAuthorityHgmvView() {
		return issuingAuthorityHgmvView;
	}

	public void setIssuingAuthorityHgmvView(boolean issuingAuthorityHgmvView) {
		this.issuingAuthorityHgmvView = issuingAuthorityHgmvView;
	}

	public boolean isDrivingExpUploadView() {
		return drivingExpUploadView;
	}

	public void setDrivingExpUploadView(boolean drivingExpUploadView) {
		this.drivingExpUploadView = drivingExpUploadView;
	}

	public boolean isIssueDateLmvView() {
		return issueDateLmvView;
	}

	public void setIssueDateLmvView(boolean issueDateLmvView) {
		this.issueDateLmvView = issueDateLmvView;
	}

	public boolean isValidUptoLmvView() {
		return validUptoLmvView;
	}

	public void setValidUptoLmvView(boolean validUptoLmvView) {
		this.validUptoLmvView = validUptoLmvView;
	}

	public boolean isIssuingAuthorityLmvView() {
		return issuingAuthorityLmvView;
	}

	public void setIssuingAuthorityLmvView(boolean issuingAuthorityLmvView) {
		this.issuingAuthorityLmvView = issuingAuthorityLmvView;
	}

	public boolean isLicenceNumberLmvView() {
		return licenceNumberLmvView;
	}

	public void setLicenceNumberLmvView(boolean licenceNumberLmvView) {
		this.licenceNumberLmvView = licenceNumberLmvView;
	}

	public boolean isLmvExperienceView() {
		return lmvExperienceView;
	}

	public void setLmvExperienceView(boolean lmvExperienceView) {
		this.lmvExperienceView = lmvExperienceView;
	}

	public boolean isInterYearView() {
		return interYearView;
	}

	public void setInterYearView(boolean interYearView) {
		this.interYearView = interYearView;
	}

	public boolean isInterInstitutionView() {
		return interInstitutionView;
	}

	public void setInterInstitutionView(boolean interInstitutionView) {
		this.interInstitutionView = interInstitutionView;
	}

	public boolean isInterSpecializationView() {
		return interSpecializationView;
	}

	public void setInterSpecializationView(boolean interSpecializationView) {
		this.interSpecializationView = interSpecializationView;
	}

	public boolean isInterCourseTypeView() {
		return interCourseTypeView;
	}

	public void setInterCourseTypeView(boolean interCourseTypeView) {
		this.interCourseTypeView = interCourseTypeView;
	}

	public boolean isInterTotalMarksView() {
		return interTotalMarksView;
	}

	public void setInterTotalMarksView(boolean interTotalMarksView) {
		this.interTotalMarksView = interTotalMarksView;
	}

	public boolean isInterObtainarksView() {
		return interObtainarksView;
	}

	public void setInterObtainarksView(boolean interObtainarksView) {
		this.interObtainarksView = interObtainarksView;
	}

	public boolean isInterPercentageView() {
		return interPercentageView;
	}

	public void setInterPercentageView(boolean interPercentageView) {
		this.interPercentageView = interPercentageView;
	}

	public boolean isInterBoardView() {
		return interBoardView;
	}

	public void setInterBoardView(boolean interBoardView) {
		this.interBoardView = interBoardView;
	}

	public boolean isInterCertificateView() {
		return interCertificateView;
	}

	public void setInterCertificateView(boolean interCertificateView) {
		this.interCertificateView = interCertificateView;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public String getPostProfile() {
		return postProfile;
	}

	public void setPostProfile(String postProfile) {
		this.postProfile = postProfile;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	// public Multipart getPostDetailsDocument() {
	// return postDetailsDocument;
	// }
	//
	// public void setPostDetailsDocument(Multipart postDetailsDocument) {
	// this.postDetailsDocument = postDetailsDocument;
	// }

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getFeeSubmitDate() {
		return feeSubmitDate;
	}

	public void setFeeSubmitDate(Date feeSubmitDate) {
		this.feeSubmitDate = feeSubmitDate;
	}

	public Date getFeeSubmitLastDate() {
		return feeSubmitLastDate;
	}

	public void setFeeSubmitLastDate(Date feeSubmitLastDate) {
		this.feeSubmitLastDate = feeSubmitLastDate;
	}

	public boolean isRegtinNumberForm() {
		return regtinNumberForm;
	}

	public void setRegtinNumberForm(boolean regtinNumberForm) {
		this.regtinNumberForm = regtinNumberForm;
	}

	public boolean isStenographyForm() {
		return stenographyForm;
	}

	public void setStenographyForm(boolean stenographyForm) {
		this.stenographyForm = stenographyForm;
	}

	public boolean isHindiStenographyForm() {
		return hindiStenographyForm;
	}

	public void setHindiStenographyForm(boolean hindiStenographyForm) {
		this.hindiStenographyForm = hindiStenographyForm;
	}

	public boolean isEnglishStenographyForm() {
		return englishStenographyForm;
	}

	public void setEnglishStenographyForm(boolean englishStenographyForm) {
		this.englishStenographyForm = englishStenographyForm;
	}

	public boolean isFirstaidCertificateView() {
		return firstaidCertificateView;
	}

	public void setFirstaidCertificateView(boolean firstaidCertificateView) {
		this.firstaidCertificateView = firstaidCertificateView;
	}

	public boolean isItiYearView() {
		return itiYearView;
	}

	public void setItiYearView(boolean itiYearView) {
		this.itiYearView = itiYearView;
	}

	public boolean isItiInstitutionView() {
		return itiInstitutionView;
	}

	public void setItiInstitutionView(boolean itiInstitutionView) {
		this.itiInstitutionView = itiInstitutionView;
	}

	public boolean isItiSpecializationView() {
		return itiSpecializationView;
	}

	public void setItiSpecializationView(boolean itiSpecializationView) {
		this.itiSpecializationView = itiSpecializationView;
	}

	public boolean isItiCourseTypeView() {
		return itiCourseTypeView;
	}

	public void setItiCourseTypeView(boolean itiCourseTypeView) {
		this.itiCourseTypeView = itiCourseTypeView;
	}

	public boolean isItiTotalMarksView() {
		return itiTotalMarksView;
	}

	public void setItiTotalMarksView(boolean itiTotalMarksView) {
		this.itiTotalMarksView = itiTotalMarksView;
	}

	public boolean isItiObtainarksView() {
		return itiObtainarksView;
	}

	public void setItiObtainarksView(boolean itiObtainarksView) {
		this.itiObtainarksView = itiObtainarksView;
	}

	public boolean isItiPercentageView() {
		return itiPercentageView;
	}

	public void setItiPercentageView(boolean itiPercentageView) {
		this.itiPercentageView = itiPercentageView;
	}

	public boolean isItiUniversityView() {
		return itiUniversityView;
	}

	public void setItiUniversityView(boolean itiUniversityView) {
		this.itiUniversityView = itiUniversityView;
	}

	public boolean isItiCertificateView() {
		return itiCertificateView;
	}

	public void setItiCertificateView(boolean itiCertificateView) {
		this.itiCertificateView = itiCertificateView;
	}

	public int getItiMinPercent() {
		return itiMinPercent;
	}

	public void setItiMinPercent(int itiMinPercent) {
		this.itiMinPercent = itiMinPercent;
	}

	public boolean isItiForm() {
		return itiForm;
	}

	public void setItiForm(boolean itiForm) {
		this.itiForm = itiForm;
	}

	public boolean isDegreeBranchView() {
		return degreeBranchView;
	}

	public void setDegreeBranchView(boolean degreeBranchView) {
		this.degreeBranchView = degreeBranchView;
	}

	public boolean isItiBranchView() {
		return itiBranchView;
	}

	public void setItiBranchView(boolean itiBranchView) {
		this.itiBranchView = itiBranchView;
	}

	public boolean isDiplomaBranchView() {
		return diplomaBranchView;
	}

	public void setDiplomaBranchView(boolean diplomaBranchView) {
		this.diplomaBranchView = diplomaBranchView;
	}

	public boolean isLicenceNumberRoadView() {
		return licenceNumberRoadView;
	}

	public void setLicenceNumberRoadView(boolean licenceNumberRoadView) {
		this.licenceNumberRoadView = licenceNumberRoadView;
	}

	public boolean isIssueDateRoadView() {
		return issueDateRoadView;
	}

	public void setIssueDateRoadView(boolean issueDateRoadView) {
		this.issueDateRoadView = issueDateRoadView;
	}

	public boolean isValidUptoRoadView() {
		return validUptoRoadView;
	}

	public void setValidUptoRoadView(boolean validUptoRoadView) {
		this.validUptoRoadView = validUptoRoadView;
	}

	public boolean isIssuingAuthorityRoadView() {
		return issuingAuthorityRoadView;
	}

	public void setIssuingAuthorityRoadView(boolean issuingAuthorityRoadView) {
		this.issuingAuthorityRoadView = issuingAuthorityRoadView;
	}

	public boolean isLicenceNumberOegView() {
		return licenceNumberOegView;
	}

	public void setLicenceNumberOegView(boolean licenceNumberOegView) {
		this.licenceNumberOegView = licenceNumberOegView;
	}

	public boolean isIssueDateOegView() {
		return issueDateOegView;
	}

	public void setIssueDateOegView(boolean issueDateOegView) {
		this.issueDateOegView = issueDateOegView;
	}

	public boolean isValidUptoOegView() {
		return validUptoOegView;
	}

	public void setValidUptoOegView(boolean validUptoOegView) {
		this.validUptoOegView = validUptoOegView;
	}

	public boolean isIssuingAuthorityOegView() {
		return issuingAuthorityOegView;
	}

	public void setIssuingAuthorityOegView(boolean issuingAuthorityOegView) {
		this.issuingAuthorityOegView = issuingAuthorityOegView;
	}

	public boolean isDrivingLicenceRoadForm() {
		return drivingLicenceRoadForm;
	}

	public void setDrivingLicenceRoadForm(boolean drivingLicenceRoadForm) {
		this.drivingLicenceRoadForm = drivingLicenceRoadForm;
	}

	public boolean isDrivingLicenceOegForm() {
		return drivingLicenceOegForm;
	}

	public void setDrivingLicenceOegForm(boolean drivingLicenceOegForm) {
		this.drivingLicenceOegForm = drivingLicenceOegForm;
	}

	public boolean isInterGradeView() {
		return interGradeView;
	}

	public void setInterGradeView(boolean interGradeView) {
		this.interGradeView = interGradeView;
	}

	public boolean isHighYearView() {
		return highYearView;
	}

	public void setHighYearView(boolean highYearView) {
		this.highYearView = highYearView;
	}

	public boolean isHighInstitutionView() {
		return highInstitutionView;
	}

	public void setHighInstitutionView(boolean highInstitutionView) {
		this.highInstitutionView = highInstitutionView;
	}

	public boolean isHighSpecializationView() {
		return highSpecializationView;
	}

	public void setHighSpecializationView(boolean highSpecializationView) {
		this.highSpecializationView = highSpecializationView;
	}

	public boolean isHighCourseTypeView() {
		return highCourseTypeView;
	}

	public void setHighCourseTypeView(boolean highCourseTypeView) {
		this.highCourseTypeView = highCourseTypeView;
	}

	public boolean isHighTotalMarksView() {
		return highTotalMarksView;
	}

	public void setHighTotalMarksView(boolean highTotalMarksView) {
		this.highTotalMarksView = highTotalMarksView;
	}

	public boolean isHighObtainarksView() {
		return highObtainarksView;
	}

	public void setHighObtainarksView(boolean highObtainarksView) {
		this.highObtainarksView = highObtainarksView;
	}

	public boolean isHighPercentageView() {
		return highPercentageView;
	}

	public void setHighPercentageView(boolean highPercentageView) {
		this.highPercentageView = highPercentageView;
	}

	public boolean isHighBoardView() {
		return highBoardView;
	}

	public void setHighBoardView(boolean highBoardView) {
		this.highBoardView = highBoardView;
	}

	public boolean isHighCertificateView() {
		return highCertificateView;
	}

	public void setHighCertificateView(boolean highCertificateView) {
		this.highCertificateView = highCertificateView;
	}

	public int getHighMinPercent() {
		return highMinPercent;
	}

	public void setHighMinPercent(int highMinPercent) {
		this.highMinPercent = highMinPercent;
	}

	public boolean isHighGradeView() {
		return highGradeView;
	}

	public void setHighGradeView(boolean highGradeView) {
		this.highGradeView = highGradeView;
	}

	public boolean isTypingview() {
		return typingview;
	}

	public void setTypingview(boolean typingview) {
		this.typingview = typingview;
	}

	public boolean isStenographyview() {
		return stenographyview;
	}

	public void setStenographyview(boolean stenographyview) {
		this.stenographyview = stenographyview;
	}
	
	
}
