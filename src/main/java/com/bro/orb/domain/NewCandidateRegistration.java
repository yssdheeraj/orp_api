package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_newCandidateRegistration")
public class NewCandidateRegistration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 388963271152787310L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "registration_id", unique = true, nullable = false)
	private String registrationId;

//	@Column(name = "first_name")
//	private String firstName;
//
//	@Column(name = "middle_name")
//	private String middleName;
//
//	@Column(name = "last_name")
//	private String lastName;
	
	@Column(name = "candidate_name")
	private String candidateName;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "age")
	private String age;

	@Column(name = "gender")
	private String gender;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "email_address")
	private String emailAddress;

	@Column(name = "nationality")
	private String nationality;

	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;


	public NewCandidateRegistration() {
		super();
		// TODO Auto-generated constructor stub
	}


	public NewCandidateRegistration(long id, String registrationId, String candidateName, String fatherName,
			Date dateOfBirth, String age, String gender, String mobileNumber, String emailAddress, String nationality,
			User user) {
		super();
		this.id = id;
		this.registrationId = registrationId;
		this.candidateName = candidateName;
		this.fatherName = fatherName;
		this.dateOfBirth = dateOfBirth;
		this.age = age;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailAddress = emailAddress;
		this.nationality = nationality;
		this.user = user;
	}


	@Override
	public String toString() {
		return "NewCandidateRegistration [id=" + id + ", registrationId=" + registrationId + ", candidateName="
				+ candidateName + ", fatherName=" + fatherName + ", dateOfBirth=" + dateOfBirth + ", age=" + age
				+ ", gender=" + gender + ", mobileNumber=" + mobileNumber + ", emailAddress=" + emailAddress
				+ ", nationality=" + nationality + ", user=" + user + "]";
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getRegistrationId() {
		return registrationId;
	}


	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}


	public String getCandidateName() {
		return candidateName;
	}


	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}


	public String getFatherName() {
		return fatherName;
	}


	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public Date getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	
}
