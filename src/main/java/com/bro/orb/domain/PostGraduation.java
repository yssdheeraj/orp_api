package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_post_graduate")
public class PostGraduation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "pg_year")
	private boolean pgYearView;

	@Column(name = "pg_institution")
	private boolean pgInstitutionView;

	@Column(name = "pg_specialization")
	private boolean pgSpecializationView;

	@Column(name = "pg_course_type")
	private boolean pgCourseTypeView;

	@Column(name = "pg_total_marks")
	private boolean pgTotalMarksView;

	@Column(name = "pg_obtain_marks")
	private boolean pgObtainarksView;

	@Column(name = "pg_percentage")
	private boolean pgPercentageView;

	@Column(name = "pg_course")
	private boolean pgCourseView;

	@Column(name = "pg_university")
	private boolean pgUniversityView;

	@Column(name = "pg_certificate")
	private boolean pgCertificateView;
	
	@Column(name = "pg_min_percent")
	private int pgMinPercent;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public PostGraduation() {
		super();
	}

	public PostGraduation(int id, boolean pgYearView, boolean pgInstitutionView, boolean pgSpecializationView,
			boolean pgCourseTypeView, boolean pgTotalMarksView, boolean pgObtainarksView, boolean pgPercentageView,
			boolean pgCourseView, boolean pgUniversityView, boolean pgCertificateView, int pgMinPercent,
			RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.pgYearView = pgYearView;
		this.pgInstitutionView = pgInstitutionView;
		this.pgSpecializationView = pgSpecializationView;
		this.pgCourseTypeView = pgCourseTypeView;
		this.pgTotalMarksView = pgTotalMarksView;
		this.pgObtainarksView = pgObtainarksView;
		this.pgPercentageView = pgPercentageView;
		this.pgCourseView = pgCourseView;
		this.pgUniversityView = pgUniversityView;
		this.pgCertificateView = pgCertificateView;
		this.pgMinPercent = pgMinPercent;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPgYearView() {
		return pgYearView;
	}

	public void setPgYearView(boolean pgYearView) {
		this.pgYearView = pgYearView;
	}

	public boolean isPgInstitutionView() {
		return pgInstitutionView;
	}

	public void setPgInstitutionView(boolean pgInstitutionView) {
		this.pgInstitutionView = pgInstitutionView;
	}

	public boolean isPgSpecializationView() {
		return pgSpecializationView;
	}

	public void setPgSpecializationView(boolean pgSpecializationView) {
		this.pgSpecializationView = pgSpecializationView;
	}

	public boolean isPgCourseTypeView() {
		return pgCourseTypeView;
	}

	public void setPgCourseTypeView(boolean pgCourseTypeView) {
		this.pgCourseTypeView = pgCourseTypeView;
	}

	public boolean isPgTotalMarksView() {
		return pgTotalMarksView;
	}

	public void setPgTotalMarksView(boolean pgTotalMarksView) {
		this.pgTotalMarksView = pgTotalMarksView;
	}

	public boolean isPgObtainarksView() {
		return pgObtainarksView;
	}

	public void setPgObtainarksView(boolean pgObtainarksView) {
		this.pgObtainarksView = pgObtainarksView;
	}

	public boolean isPgPercentageView() {
		return pgPercentageView;
	}

	public void setPgPercentageView(boolean pgPercentageView) {
		this.pgPercentageView = pgPercentageView;
	}

	public boolean isPgCourseView() {
		return pgCourseView;
	}

	public void setPgCourseView(boolean pgCourseView) {
		this.pgCourseView = pgCourseView;
	}

	public boolean isPgUniversityView() {
		return pgUniversityView;
	}

	public void setPgUniversityView(boolean pgUniversityView) {
		this.pgUniversityView = pgUniversityView;
	}

	public boolean isPgCertificateView() {
		return pgCertificateView;
	}

	public void setPgCertificateView(boolean pgCertificateView) {
		this.pgCertificateView = pgCertificateView;
	}

	public int getPgMinPercent() {
		return pgMinPercent;
	}

	public void setPgMinPercent(int pgMinPercent) {
		this.pgMinPercent = pgMinPercent;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "PostGraduation [id=" + id + ", pgYearView=" + pgYearView + ", pgInstitutionView=" + pgInstitutionView
				+ ", pgSpecializationView=" + pgSpecializationView + ", pgCourseTypeView=" + pgCourseTypeView
				+ ", pgTotalMarksView=" + pgTotalMarksView + ", pgObtainarksView=" + pgObtainarksView
				+ ", pgPercentageView=" + pgPercentageView + ", pgCourseView=" + pgCourseView + ", pgUniversityView="
				+ pgUniversityView + ", pgCertificateView=" + pgCertificateView + ", pgMinPercent=" + pgMinPercent
				+ ", registrationViewBean=" + registrationViewBean + "]";
	}
}
