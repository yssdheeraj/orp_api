package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jobs_desc")
public class JobsDescription extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "job_title")
	private String jobTitle;

	@Column(name = "job_desc")
	private String jobDesc;

	public JobsDescription() {
		super();
	}

	public JobsDescription(int id, String jobTitle, String jobDesc) {
		super();
		this.id = id;
		this.jobTitle = jobTitle;
		this.jobDesc = jobDesc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	@Override
	public String toString() {
		return "JobsDescription [id=" + id + ", jobTitle=" + jobTitle
				+ ", jobDesc=" + jobDesc + "]";
	}

}
