package com.bro.orb.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_shortlist1_personal_details")
public class ShortListAPersonalDetails extends AbstractAuditingEntity implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "registration_id", unique = true, nullable = false)
	private String registrationId;

	
	@Column(name="candidate_Marks")
	private String candidatemarks;
	
	@Column(name = "recruitment_zone")
	private String recruitmentZone;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "mother_name")
	private String motherName;

	@Column(name = "category")
	private String category;

	@Column(name = "category_certificate")
	private String categoryCertificate;

	@Column(name = "aadhar_certificate")
	private String aadharCertificate;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "age")
	private String age;

	@Column(name = "gender")
	private String gender;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "email_address")
	private String emailAddress;

	@Column(name = "nationality")
	private String nationality;

	@Column(name = "aadhaar_card")
	private boolean aadhaarCard;

	@Column(name = "aadhaar_number")
	private String aadhaarNumber;

	@Column(name = "marital_status")
	private String maritalStatus;

	@Column(name = "belong_community")
	private boolean belongCommunity;

	@Column(name = "community_name")
	private String communityName;

	@Column(name = "permanent_address")
	private String permanentAddress;

	@Column(name = "permanent_state")
	private String permanentState;

	@Column(name = "permanent_city")
	private String permanentCity;

	@Column(name = "permanent_pincode")
	private int permanentPincode;

	@Column(name = "postal_address")
	private String postalAddress;

	@Column(name = "postal_state")
	private String postalState;

	@Column(name = "postal_city")
	private String postalCity;

	@Column(name = "postal_pincode")
	private int postalPincode;

	@Column(name = "son_daughter_exservice")
	private boolean sonDaughterExservice;

	@Column(name = "army_number")
	private String armyNumber;

	@Column(name = "army_rank")
	private String armyRank;

	@Column(name = "son_daughter_gref")
	private boolean sonDaughterGref;

	@Column(name = "son_daughter_number")
	private String sonDaughterNumber;

	@Column(name = "disablity")
	private boolean disablity;

	@Column(name = "disability_specify")
	private String disabilitySpecify;

	@Column(name = "height")
	private int candidateHeight;

	@Column(name = "weight")
	private int candidateWeight;

	@Column(name = "blood_group")
	private String bloodGroup;

	@Column(name = "eye_sight")
	private String eyeSight;

	@Column(name = "armed_force")
	private boolean armedForce;

	@Column(name = "armed_from")
	private Date armedFrom;

	@Column(name = "armed_to")
	private Date armedTo;

	@Column(name = "armed_dis_certificate")
	private String armeddisCertificate;

	@Column(name = "armed_army_number")
	private String armedarmyNumber;

	@Column(name = "central_govt")
	private boolean centralGovt;

	@Column(name = "served_from")
	private Date servedFrom;

	@Column(name = "served_to")
	private Date servedTo;

	@Column(name = "central_govt_certificate")
	private String centralGovtCertificate;

	@Column(name = "son_daughter_rank")
	private String sonDaughterRank;

	@Column(name = "son_daughter_name")
	private String sonDaughterName;

	@Column(name = "brother_sister_gref")
	private boolean brotherSisterGref;

	@Column(name = "brother_sister_number")
	private String brotherSisterNumber;

	@Column(name = "brother_sister_rank")
	private String brotherSisterRank;

	@Column(name = "brother_sister_name")
	private String brotherSisterName;

	@Column(name = "ncc_b")
	private boolean nccB;

	@Column(name = "nccb_certificate")
	private String nccbCertificate;

	@Column(name = "ncc_c")
	private boolean nccC;

	@Column(name = "nccc_certificate")
	private String nccCertificate;

	@Column(name = "sportsman")
	private boolean sportsman;

	@Column(name = "payment")
	private boolean payment;

	@Column(name = "final_submit")
	private boolean finalSubmit;

	@Column(name = "sport_certificate")
	private String sportCertificate;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "essential_id", referencedColumnName = "id")
	private ShortListAEssentialQualification essential;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "graduation_id", referencedColumnName = "id")
	private ShortListAGraduationQualification graduation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "technical_id", referencedColumnName = "id")
	private ShortListATechnicalQualification technical;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "experience_id", referencedColumnName = "id")
	private ShortListAOrganisationExperience experience;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "driving_id", referencedColumnName = "id")
	private ShortListADrivingLicense driving;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "certificate_id", referencedColumnName = "id")
	private ShortListAUploadCertificate certificate;

	@OneToOne
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private ShortListAJobDetails jobDetails;

	public ShortListAPersonalDetails() {
		super();
	}


	
	
	
	public ShortListAPersonalDetails(long id, String registrationId, String candidatemarks, String recruitmentZone,
			String firstName, String middleName, String lastName, String fatherName, String motherName, String category,
			String categoryCertificate, String aadharCertificate, Date dateOfBirth, String age, String gender,
			String mobileNumber, String emailAddress, String nationality, boolean aadhaarCard, String aadhaarNumber,
			String maritalStatus, boolean belongCommunity, String communityName, String permanentAddress,
			String permanentState, String permanentCity, int permanentPincode, String postalAddress, String postalState,
			String postalCity, int postalPincode, boolean sonDaughterExservice, String armyNumber, String armyRank,
			boolean sonDaughterGref, String sonDaughterNumber, boolean disablity, String disabilitySpecify,
			int candidateHeight, int candidateWeight, String bloodGroup, String eyeSight, boolean armedForce,
			Date armedFrom, Date armedTo, String armeddisCertificate, String armedarmyNumber, boolean centralGovt,
			Date servedFrom, Date servedTo, String centralGovtCertificate, String sonDaughterRank,
			String sonDaughterName, boolean brotherSisterGref, String brotherSisterNumber, String brotherSisterRank,
			String brotherSisterName, boolean nccB, String nccbCertificate, boolean nccC, String nccCertificate,
			boolean sportsman, boolean payment, boolean finalSubmit, String sportCertificate, User user,
			ShortListAEssentialQualification essential, ShortListAGraduationQualification graduation,
			ShortListATechnicalQualification technical, ShortListAOrganisationExperience experience,
			ShortListADrivingLicense driving, ShortListAUploadCertificate certificate,
			ShortListAJobDetails jobDetails) {
		super();
		this.id = id;
		this.registrationId = registrationId;
		this.candidatemarks = candidatemarks;
		this.recruitmentZone = recruitmentZone;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.category = category;
		this.categoryCertificate = categoryCertificate;
		this.aadharCertificate = aadharCertificate;
		this.dateOfBirth = dateOfBirth;
		this.age = age;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailAddress = emailAddress;
		this.nationality = nationality;
		this.aadhaarCard = aadhaarCard;
		this.aadhaarNumber = aadhaarNumber;
		this.maritalStatus = maritalStatus;
		this.belongCommunity = belongCommunity;
		this.communityName = communityName;
		this.permanentAddress = permanentAddress;
		this.permanentState = permanentState;
		this.permanentCity = permanentCity;
		this.permanentPincode = permanentPincode;
		this.postalAddress = postalAddress;
		this.postalState = postalState;
		this.postalCity = postalCity;
		this.postalPincode = postalPincode;
		this.sonDaughterExservice = sonDaughterExservice;
		this.armyNumber = armyNumber;
		this.armyRank = armyRank;
		this.sonDaughterGref = sonDaughterGref;
		this.sonDaughterNumber = sonDaughterNumber;
		this.disablity = disablity;
		this.disabilitySpecify = disabilitySpecify;
		this.candidateHeight = candidateHeight;
		this.candidateWeight = candidateWeight;
		this.bloodGroup = bloodGroup;
		this.eyeSight = eyeSight;
		this.armedForce = armedForce;
		this.armedFrom = armedFrom;
		this.armedTo = armedTo;
		this.armeddisCertificate = armeddisCertificate;
		this.armedarmyNumber = armedarmyNumber;
		this.centralGovt = centralGovt;
		this.servedFrom = servedFrom;
		this.servedTo = servedTo;
		this.centralGovtCertificate = centralGovtCertificate;
		this.sonDaughterRank = sonDaughterRank;
		this.sonDaughterName = sonDaughterName;
		this.brotherSisterGref = brotherSisterGref;
		this.brotherSisterNumber = brotherSisterNumber;
		this.brotherSisterRank = brotherSisterRank;
		this.brotherSisterName = brotherSisterName;
		this.nccB = nccB;
		this.nccbCertificate = nccbCertificate;
		this.nccC = nccC;
		this.nccCertificate = nccCertificate;
		this.sportsman = sportsman;
		this.payment = payment;
		this.finalSubmit = finalSubmit;
		this.sportCertificate = sportCertificate;
		this.user = user;
		this.essential = essential;
		this.graduation = graduation;
		this.technical = technical;
		this.experience = experience;
		this.driving = driving;
		this.certificate = certificate;
		this.jobDetails = jobDetails;
	}

	
	
	public long getId() {
		return id;
	}





	public void setId(long id) {
		this.id = id;
	}





	public String getRegistrationId() {
		return registrationId;
	}





	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}





	public String getCandidatemarks() {
		return candidatemarks;
	}





	public void setCandidatemarks(String candidatemarks) {
		this.candidatemarks = candidatemarks;
	}





	public String getRecruitmentZone() {
		return recruitmentZone;
	}





	public void setRecruitmentZone(String recruitmentZone) {
		this.recruitmentZone = recruitmentZone;
	}





	public String getFirstName() {
		return firstName;
	}





	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}





	public String getMiddleName() {
		return middleName;
	}





	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}





	public String getLastName() {
		return lastName;
	}





	public void setLastName(String lastName) {
		this.lastName = lastName;
	}





	public String getFatherName() {
		return fatherName;
	}





	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}





	public String getMotherName() {
		return motherName;
	}





	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}





	public String getCategory() {
		return category;
	}





	public void setCategory(String category) {
		this.category = category;
	}





	public String getCategoryCertificate() {
		return categoryCertificate;
	}





	public void setCategoryCertificate(String categoryCertificate) {
		this.categoryCertificate = categoryCertificate;
	}





	public String getAadharCertificate() {
		return aadharCertificate;
	}





	public void setAadharCertificate(String aadharCertificate) {
		this.aadharCertificate = aadharCertificate;
	}





	public Date getDateOfBirth() {
		return dateOfBirth;
	}





	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}





	public String getAge() {
		return age;
	}





	public void setAge(String age) {
		this.age = age;
	}





	public String getGender() {
		return gender;
	}





	public void setGender(String gender) {
		this.gender = gender;
	}





	public String getMobileNumber() {
		return mobileNumber;
	}





	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}





	public String getEmailAddress() {
		return emailAddress;
	}





	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}





	public String getNationality() {
		return nationality;
	}





	public void setNationality(String nationality) {
		this.nationality = nationality;
	}





	public boolean isAadhaarCard() {
		return aadhaarCard;
	}





	public void setAadhaarCard(boolean aadhaarCard) {
		this.aadhaarCard = aadhaarCard;
	}





	public String getAadhaarNumber() {
		return aadhaarNumber;
	}





	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}





	public String getMaritalStatus() {
		return maritalStatus;
	}





	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}





	public boolean isBelongCommunity() {
		return belongCommunity;
	}





	public void setBelongCommunity(boolean belongCommunity) {
		this.belongCommunity = belongCommunity;
	}





	public String getCommunityName() {
		return communityName;
	}





	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}





	public String getPermanentAddress() {
		return permanentAddress;
	}





	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}





	public String getPermanentState() {
		return permanentState;
	}





	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}





	public String getPermanentCity() {
		return permanentCity;
	}





	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}





	public int getPermanentPincode() {
		return permanentPincode;
	}





	public void setPermanentPincode(int permanentPincode) {
		this.permanentPincode = permanentPincode;
	}





	public String getPostalAddress() {
		return postalAddress;
	}





	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}





	public String getPostalState() {
		return postalState;
	}





	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}





	public String getPostalCity() {
		return postalCity;
	}





	public void setPostalCity(String postalCity) {
		this.postalCity = postalCity;
	}





	public int getPostalPincode() {
		return postalPincode;
	}





	public void setPostalPincode(int postalPincode) {
		this.postalPincode = postalPincode;
	}





	public boolean isSonDaughterExservice() {
		return sonDaughterExservice;
	}





	public void setSonDaughterExservice(boolean sonDaughterExservice) {
		this.sonDaughterExservice = sonDaughterExservice;
	}





	public String getArmyNumber() {
		return armyNumber;
	}





	public void setArmyNumber(String armyNumber) {
		this.armyNumber = armyNumber;
	}





	public String getArmyRank() {
		return armyRank;
	}





	public void setArmyRank(String armyRank) {
		this.armyRank = armyRank;
	}





	public boolean isSonDaughterGref() {
		return sonDaughterGref;
	}





	public void setSonDaughterGref(boolean sonDaughterGref) {
		this.sonDaughterGref = sonDaughterGref;
	}





	public String getSonDaughterNumber() {
		return sonDaughterNumber;
	}





	public void setSonDaughterNumber(String sonDaughterNumber) {
		this.sonDaughterNumber = sonDaughterNumber;
	}





	public boolean isDisablity() {
		return disablity;
	}





	public void setDisablity(boolean disablity) {
		this.disablity = disablity;
	}





	public String getDisabilitySpecify() {
		return disabilitySpecify;
	}





	public void setDisabilitySpecify(String disabilitySpecify) {
		this.disabilitySpecify = disabilitySpecify;
	}





	public int getCandidateHeight() {
		return candidateHeight;
	}





	public void setCandidateHeight(int candidateHeight) {
		this.candidateHeight = candidateHeight;
	}





	public int getCandidateWeight() {
		return candidateWeight;
	}





	public void setCandidateWeight(int candidateWeight) {
		this.candidateWeight = candidateWeight;
	}





	public String getBloodGroup() {
		return bloodGroup;
	}





	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}





	public String getEyeSight() {
		return eyeSight;
	}





	public void setEyeSight(String eyeSight) {
		this.eyeSight = eyeSight;
	}





	public boolean isArmedForce() {
		return armedForce;
	}





	public void setArmedForce(boolean armedForce) {
		this.armedForce = armedForce;
	}





	public Date getArmedFrom() {
		return armedFrom;
	}





	public void setArmedFrom(Date armedFrom) {
		this.armedFrom = armedFrom;
	}





	public Date getArmedTo() {
		return armedTo;
	}





	public void setArmedTo(Date armedTo) {
		this.armedTo = armedTo;
	}





	public String getArmeddisCertificate() {
		return armeddisCertificate;
	}





	public void setArmeddisCertificate(String armeddisCertificate) {
		this.armeddisCertificate = armeddisCertificate;
	}





	public String getArmedarmyNumber() {
		return armedarmyNumber;
	}





	public void setArmedarmyNumber(String armedarmyNumber) {
		this.armedarmyNumber = armedarmyNumber;
	}





	public boolean isCentralGovt() {
		return centralGovt;
	}





	public void setCentralGovt(boolean centralGovt) {
		this.centralGovt = centralGovt;
	}





	public Date getServedFrom() {
		return servedFrom;
	}





	public void setServedFrom(Date servedFrom) {
		this.servedFrom = servedFrom;
	}





	public Date getServedTo() {
		return servedTo;
	}





	public void setServedTo(Date servedTo) {
		this.servedTo = servedTo;
	}





	public String getCentralGovtCertificate() {
		return centralGovtCertificate;
	}





	public void setCentralGovtCertificate(String centralGovtCertificate) {
		this.centralGovtCertificate = centralGovtCertificate;
	}





	public String getSonDaughterRank() {
		return sonDaughterRank;
	}





	public void setSonDaughterRank(String sonDaughterRank) {
		this.sonDaughterRank = sonDaughterRank;
	}





	public String getSonDaughterName() {
		return sonDaughterName;
	}





	public void setSonDaughterName(String sonDaughterName) {
		this.sonDaughterName = sonDaughterName;
	}





	public boolean isBrotherSisterGref() {
		return brotherSisterGref;
	}





	public void setBrotherSisterGref(boolean brotherSisterGref) {
		this.brotherSisterGref = brotherSisterGref;
	}





	public String getBrotherSisterNumber() {
		return brotherSisterNumber;
	}





	public void setBrotherSisterNumber(String brotherSisterNumber) {
		this.brotherSisterNumber = brotherSisterNumber;
	}





	public String getBrotherSisterRank() {
		return brotherSisterRank;
	}





	public void setBrotherSisterRank(String brotherSisterRank) {
		this.brotherSisterRank = brotherSisterRank;
	}





	public String getBrotherSisterName() {
		return brotherSisterName;
	}





	public void setBrotherSisterName(String brotherSisterName) {
		this.brotherSisterName = brotherSisterName;
	}





	public boolean isNccB() {
		return nccB;
	}





	public void setNccB(boolean nccB) {
		this.nccB = nccB;
	}





	public String getNccbCertificate() {
		return nccbCertificate;
	}





	public void setNccbCertificate(String nccbCertificate) {
		this.nccbCertificate = nccbCertificate;
	}





	public boolean isNccC() {
		return nccC;
	}





	public void setNccC(boolean nccC) {
		this.nccC = nccC;
	}





	public String getNccCertificate() {
		return nccCertificate;
	}





	public void setNccCertificate(String nccCertificate) {
		this.nccCertificate = nccCertificate;
	}





	public boolean isSportsman() {
		return sportsman;
	}





	public void setSportsman(boolean sportsman) {
		this.sportsman = sportsman;
	}





	public boolean isPayment() {
		return payment;
	}





	public void setPayment(boolean payment) {
		this.payment = payment;
	}





	public boolean isFinalSubmit() {
		return finalSubmit;
	}





	public void setFinalSubmit(boolean finalSubmit) {
		this.finalSubmit = finalSubmit;
	}





	public String getSportCertificate() {
		return sportCertificate;
	}





	public void setSportCertificate(String sportCertificate) {
		this.sportCertificate = sportCertificate;
	}





	public User getUser() {
		return user;
	}





	public void setUser(User user) {
		this.user = user;
	}





	public ShortListAEssentialQualification getEssential() {
		return essential;
	}





	public void setEssential(ShortListAEssentialQualification essential) {
		this.essential = essential;
	}





	public ShortListAGraduationQualification getGraduation() {
		return graduation;
	}





	public void setGraduation(ShortListAGraduationQualification graduation) {
		this.graduation = graduation;
	}





	public ShortListATechnicalQualification getTechnical() {
		return technical;
	}





	public void setTechnical(ShortListATechnicalQualification technical) {
		this.technical = technical;
	}





	public ShortListAOrganisationExperience getExperience() {
		return experience;
	}





	public void setExperience(ShortListAOrganisationExperience experience) {
		this.experience = experience;
	}





	public ShortListADrivingLicense getDriving() {
		return driving;
	}





	public void setDriving(ShortListADrivingLicense driving) {
		this.driving = driving;
	}





	public ShortListAUploadCertificate getCertificate() {
		return certificate;
	}





	public void setCertificate(ShortListAUploadCertificate certificate) {
		this.certificate = certificate;
	}





	public ShortListAJobDetails getJobDetails() {
		return jobDetails;
	}





	public void setJobDetails(ShortListAJobDetails jobDetails) {
		this.jobDetails = jobDetails;
	}





	@Override
	public String toString() {
		return "ShortListAPersonalDetails [id=" + id + ", registrationId=" + registrationId + ", candidatemarks="
				+ candidatemarks + ", recruitmentZone=" + recruitmentZone + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", fatherName=" + fatherName + ", motherName=" + motherName
				+ ", category=" + category + ", categoryCertificate=" + categoryCertificate + ", aadharCertificate="
				+ aadharCertificate + ", dateOfBirth=" + dateOfBirth + ", age=" + age + ", gender=" + gender
				+ ", mobileNumber=" + mobileNumber + ", emailAddress=" + emailAddress + ", nationality=" + nationality
				+ ", aadhaarCard=" + aadhaarCard + ", aadhaarNumber=" + aadhaarNumber + ", maritalStatus="
				+ maritalStatus + ", belongCommunity=" + belongCommunity + ", communityName=" + communityName
				+ ", permanentAddress=" + permanentAddress + ", permanentState=" + permanentState + ", permanentCity="
				+ permanentCity + ", permanentPincode=" + permanentPincode + ", postalAddress=" + postalAddress
				+ ", postalState=" + postalState + ", postalCity=" + postalCity + ", postalPincode=" + postalPincode
				+ ", sonDaughterExservice=" + sonDaughterExservice + ", armyNumber=" + armyNumber + ", armyRank="
				+ armyRank + ", sonDaughterGref=" + sonDaughterGref + ", sonDaughterNumber=" + sonDaughterNumber
				+ ", disablity=" + disablity + ", disabilitySpecify=" + disabilitySpecify + ", candidateHeight="
				+ candidateHeight + ", candidateWeight=" + candidateWeight + ", bloodGroup=" + bloodGroup
				+ ", eyeSight=" + eyeSight + ", armedForce=" + armedForce + ", armedFrom=" + armedFrom + ", armedTo="
				+ armedTo + ", armeddisCertificate=" + armeddisCertificate + ", armedarmyNumber=" + armedarmyNumber
				+ ", centralGovt=" + centralGovt + ", servedFrom=" + servedFrom + ", servedTo=" + servedTo
				+ ", centralGovtCertificate=" + centralGovtCertificate + ", sonDaughterRank=" + sonDaughterRank
				+ ", sonDaughterName=" + sonDaughterName + ", brotherSisterGref=" + brotherSisterGref
				+ ", brotherSisterNumber=" + brotherSisterNumber + ", brotherSisterRank=" + brotherSisterRank
				+ ", brotherSisterName=" + brotherSisterName + ", nccB=" + nccB + ", nccbCertificate=" + nccbCertificate
				+ ", nccC=" + nccC + ", nccCertificate=" + nccCertificate + ", sportsman=" + sportsman + ", payment="
				+ payment + ", finalSubmit=" + finalSubmit + ", sportCertificate=" + sportCertificate + ", user=" + user
				+ ", essential=" + essential + ", graduation=" + graduation + ", technical=" + technical
				+ ", experience=" + experience + ", driving=" + driving + ", certificate=" + certificate
				+ ", jobDetails=" + jobDetails + "]";
	}


	
	

}
