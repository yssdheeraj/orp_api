package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_driving_roadroller")
public class DrivingRoadRoller implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "licence_number_road")
	private boolean licenceNumberRoadView;

	@Column(name = "issue_date_road")
	private boolean issueDateRoadView;

	@Column(name = "valid_upto_road")
	private boolean validUptoRoadView;

	@Column(name = "issuing_authority_road")
	private boolean issuingAuthorityRoadView;

	@Column(name = "road_experience")
	private boolean roadExperienceView;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public DrivingRoadRoller() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DrivingRoadRoller(int id, boolean licenceNumberRoadView, boolean issueDateRoadView,
			boolean validUptoRoadView, boolean issuingAuthorityRoadView, boolean roadExperienceView,
			RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.licenceNumberRoadView = licenceNumberRoadView;
		this.issueDateRoadView = issueDateRoadView;
		this.validUptoRoadView = validUptoRoadView;
		this.issuingAuthorityRoadView = issuingAuthorityRoadView;
		this.roadExperienceView = roadExperienceView;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLicenceNumberRoadView() {
		return licenceNumberRoadView;
	}

	public void setLicenceNumberRoadView(boolean licenceNumberRoadView) {
		this.licenceNumberRoadView = licenceNumberRoadView;
	}

	public boolean isIssueDateRoadView() {
		return issueDateRoadView;
	}

	public void setIssueDateRoadView(boolean issueDateRoadView) {
		this.issueDateRoadView = issueDateRoadView;
	}

	public boolean isValidUptoRoadView() {
		return validUptoRoadView;
	}

	public void setValidUptoRoadView(boolean validUptoRoadView) {
		this.validUptoRoadView = validUptoRoadView;
	}

	public boolean isIssuingAuthorityRoadView() {
		return issuingAuthorityRoadView;
	}

	public void setIssuingAuthorityRoadView(boolean issuingAuthorityRoadView) {
		this.issuingAuthorityRoadView = issuingAuthorityRoadView;
	}

	public boolean isRoadExperienceView() {
		return roadExperienceView;
	}

	public void setRoadExperienceView(boolean roadExperienceView) {
		this.roadExperienceView = roadExperienceView;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "DrivingRoadRoller [id=" + id + ", licenceNumberRoadView=" + licenceNumberRoadView
				+ ", issueDateRoadView=" + issueDateRoadView + ", validUptoRoadView=" + validUptoRoadView
				+ ", issuingAuthorityRoadView=" + issuingAuthorityRoadView + ", roadExperienceView="
				+ roadExperienceView + ", registrationViewBean=" + registrationViewBean + "]";
	}


}
