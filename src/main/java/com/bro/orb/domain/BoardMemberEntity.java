package com.bro.orb.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="board_member")
public class BoardMemberEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@OneToOne
	private BoardMemberDetails boardName;

	@OneToOne
	private User userId;
	
	@Column(name="flag",columnDefinition = "boolean default true")
	private boolean flag;
	
	public BoardMemberEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BoardMemberEntity(long id, BoardMemberDetails boardName, User userId, boolean flag) {
		super();
		this.id = id;
		this.boardName = boardName;
		this.userId = userId;
		this.flag = flag;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BoardMemberDetails getBoardName() {
		return boardName;
	}

	public void setBoardName(BoardMemberDetails boardName) {
		this.boardName = boardName;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "BoardMemberEntity [id=" + id + ", boardName=" + boardName + ", userId=" + userId + ", flag=" + flag
				+ "]";
	}
	
	
	
	
	
	
	
	
	
	
	

}
