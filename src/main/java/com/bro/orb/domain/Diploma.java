package com.bro.orb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bro_diploma")
public class Diploma implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "diploma_year")
	private boolean diplomaYearView;
	
	@Column(name = "diploma_institution")
	private boolean diplomaInstitutionView;
	
	@Column(name = "diploma_specialization")
	private boolean diplomaSpecializationView;
	
	@Column(name = "diploma_course_type")
	private boolean diplomaCourseTypeView;
	
	@Column(name = "diploma_total_marks")
	private boolean diplomaTotalMarksView;
	
	@Column(name = "diploma_obtain_marks")
	private boolean diplomaObtainarksView;
	
	@Column(name = "diploma_percentage")
	private boolean diplomaPercentageView;
	
	@Column(name = "diploma_university")
	private boolean diplomaUniversityView;
	
	@Column(name = "diploma_certificate")
	private boolean diplomaCertificateView;
	
	@Column(name = "diploma_branch")
	private boolean diplomaBranchView;
	
	@Column(name = "diploma_min_percent")
	private int diplomaMinPercent;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "registration_bean_id", referencedColumnName = "id")
	private RegistrationViewBean registrationViewBean;

	public Diploma() {
		super();
	}

	public Diploma(int id, boolean diplomaYearView, boolean diplomaInstitutionView, boolean diplomaSpecializationView,
			boolean diplomaCourseTypeView, boolean diplomaTotalMarksView, boolean diplomaObtainarksView,
			boolean diplomaPercentageView, boolean diplomaUniversityView, boolean diplomaCertificateView,
			boolean diplomaBranchView, int diplomaMinPercent, RegistrationViewBean registrationViewBean) {
		super();
		this.id = id;
		this.diplomaYearView = diplomaYearView;
		this.diplomaInstitutionView = diplomaInstitutionView;
		this.diplomaSpecializationView = diplomaSpecializationView;
		this.diplomaCourseTypeView = diplomaCourseTypeView;
		this.diplomaTotalMarksView = diplomaTotalMarksView;
		this.diplomaObtainarksView = diplomaObtainarksView;
		this.diplomaPercentageView = diplomaPercentageView;
		this.diplomaUniversityView = diplomaUniversityView;
		this.diplomaCertificateView = diplomaCertificateView;
		this.diplomaBranchView = diplomaBranchView;
		this.diplomaMinPercent = diplomaMinPercent;
		this.registrationViewBean = registrationViewBean;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDiplomaYearView() {
		return diplomaYearView;
	}

	public void setDiplomaYearView(boolean diplomaYearView) {
		this.diplomaYearView = diplomaYearView;
	}

	public boolean isDiplomaInstitutionView() {
		return diplomaInstitutionView;
	}

	public void setDiplomaInstitutionView(boolean diplomaInstitutionView) {
		this.diplomaInstitutionView = diplomaInstitutionView;
	}

	public boolean isDiplomaSpecializationView() {
		return diplomaSpecializationView;
	}

	public void setDiplomaSpecializationView(boolean diplomaSpecializationView) {
		this.diplomaSpecializationView = diplomaSpecializationView;
	}

	public boolean isDiplomaCourseTypeView() {
		return diplomaCourseTypeView;
	}

	public void setDiplomaCourseTypeView(boolean diplomaCourseTypeView) {
		this.diplomaCourseTypeView = diplomaCourseTypeView;
	}

	public boolean isDiplomaTotalMarksView() {
		return diplomaTotalMarksView;
	}

	public void setDiplomaTotalMarksView(boolean diplomaTotalMarksView) {
		this.diplomaTotalMarksView = diplomaTotalMarksView;
	}

	public boolean isDiplomaObtainarksView() {
		return diplomaObtainarksView;
	}

	public void setDiplomaObtainarksView(boolean diplomaObtainarksView) {
		this.diplomaObtainarksView = diplomaObtainarksView;
	}

	public boolean isDiplomaPercentageView() {
		return diplomaPercentageView;
	}

	public void setDiplomaPercentageView(boolean diplomaPercentageView) {
		this.diplomaPercentageView = diplomaPercentageView;
	}

	public boolean isDiplomaUniversityView() {
		return diplomaUniversityView;
	}

	public void setDiplomaUniversityView(boolean diplomaUniversityView) {
		this.diplomaUniversityView = diplomaUniversityView;
	}

	public boolean isDiplomaCertificateView() {
		return diplomaCertificateView;
	}

	public void setDiplomaCertificateView(boolean diplomaCertificateView) {
		this.diplomaCertificateView = diplomaCertificateView;
	}

	public boolean isDiplomaBranchView() {
		return diplomaBranchView;
	}

	public void setDiplomaBranchView(boolean diplomaBranchView) {
		this.diplomaBranchView = diplomaBranchView;
	}

	public int getDiplomaMinPercent() {
		return diplomaMinPercent;
	}

	public void setDiplomaMinPercent(int diplomaMinPercent) {
		this.diplomaMinPercent = diplomaMinPercent;
	}

	public RegistrationViewBean getRegistrationViewBean() {
		return registrationViewBean;
	}

	public void setRegistrationViewBean(RegistrationViewBean registrationViewBean) {
		this.registrationViewBean = registrationViewBean;
	}

	@Override
	public String toString() {
		return "Diploma [id=" + id + ", diplomaYearView=" + diplomaYearView + ", diplomaInstitutionView="
				+ diplomaInstitutionView + ", diplomaSpecializationView=" + diplomaSpecializationView
				+ ", diplomaCourseTypeView=" + diplomaCourseTypeView + ", diplomaTotalMarksView="
				+ diplomaTotalMarksView + ", diplomaObtainarksView=" + diplomaObtainarksView
				+ ", diplomaPercentageView=" + diplomaPercentageView + ", diplomaUniversityView="
				+ diplomaUniversityView + ", diplomaCertificateView=" + diplomaCertificateView + ", diplomaBranchView="
				+ diplomaBranchView + ", diplomaMinPercent=" + diplomaMinPercent + ", registrationViewBean="
				+ registrationViewBean + "]";
	}

	
}
