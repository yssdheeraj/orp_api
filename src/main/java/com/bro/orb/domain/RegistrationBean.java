package com.bro.orb.domain;

import java.time.Instant;
import java.util.Date;

public class RegistrationBean {

	private String registrationId;
	private String recruitmentZone;
	private String firstName;
	private String middleName;
	private String lastName;
	private String fatherName;
    private String motherName;
	private String category;
	private String categoryCertificate;
	private Date dateOfBirth;
	private String age;
	private String gender;
	private String mobileNumber;
	private String emailAddress;
	private String nationality;
//	private boolean aadhaarCard;
	private String aadhaarNumber;
//	private String maritalStatus;
	private boolean beStringCommunity;
	private String communityName;
	private String permanentAddress;
	private String permanentState;
	private String permanentCity;
	private String permanentPincode;
	private String postalAddress;
	private String postalState;
	private String postalCity;
//	private double height;
//	private double weight;
//	private String bloodGroup;
//	private String eyeSight;
//	private boolean disablity;
//	private String disabilitySpecify;
	private String postalPincode;
	private boolean sonDaughterExservice;
//	private String armyNumber;
//	private String armyRank;
//	private boolean sonDaughterGref;
//	private String sonDaughterNumber;
//	private String sonDaughterRank;
//	private String sonDaughterName;
//	private boolean brotherSisterGref;
//	private String brotherSisterNumber;
//	private String brotherSisterRank;
//	private String brotherSisterName;
	private boolean nccB;
	private String nccbCertificate;
	private boolean nccC;
	private String nccCertificate;
	private boolean sportsman;
//	private boolean centralGovt;
//	private String centralGovtCertificate;
//	private Date servedFrom;
//	private Date servedTo;
//	private boolean armedForce;
//	private String armeddisCertificate;
//	private Date armedFrom;
//	private Date armedTo;
//	private String armedarmyNumber;
//	private boolean empExGref;
//	private Date servedBroFrom;
//	private Date servedBroTo;
//	private String gsNumber;
	
	private boolean sonExGps;
	private String sonGsNumber;
	private String sonGpsRank;
	private String sonGpsName;
	private String sonUnitName;
	private String unitCertificate;
	
	private boolean reapptCandidate;
	private String reapptCandidateGs;
	private String reaaptCandidateRank;
	private String reapptCandidateName;
	private String reapptCandidateUnit;
	
	private boolean sonExservice;
	private String armyNumber;
	private String armyRank;
	private String armyName;
	private String armyLastUnit;
	private String lastUnitCertificate;
	
	private boolean exService;
	private String dischargeCertificate;
	
	private String sportsLevel;
	private String sportCertificate;
	
	private String panNumber;
	private String pancertificate;
	
	private boolean casualplabour;
	private String cplCertificate;
	
	private String licenceNumberLmv;
	private Date issueDateLmv;
	private Date validUptoLmv;
	private String issuingAuthorityLmv;
	private String licenceNumberHgmv;
	private Date issueDateHgmv;
	private Date validUptoHgmv;
	private String issuingAuthorityHlmv;
	private String licenceNumberRoad;
	private Date issueDateRoad;
	private Date validUptoRoad;
	private String issuingAuthorityRoad;
	private String licenceNumberOeg;
	private Date issueDateOeg;
	private Date validUptoOeg;
	private String issuingAuthorityOeg;
	private String uploadLicence;
	private String highYear;
	private String highInstitution;
	private String highSpecialization;
	private String highCourseType;
	private String highBoard;
	private String highTotalMarks;
	private String highObtainMarks;
	private String highPercentage;
	private String highCertificate;
	private String highGrade;
	private String interGrade;
	private String interYear;
	private String interInstitution;
	private String interSpecialization;
	private String interCourseType;
	private String interBoard;
	private String interTotalMarks;
	private String interObtainMarks;
	private String interPercentage;
	private String interCertificate;
	private String englishTyping;
	private String hindiTyping;
	private String hindiStenography;
	private String englishStenography;
	private String additionalCertificate;
	private String firstaidCertificate;
	private String ugCourse;
	private String ugPassingYear;
	private String ugInstitution;
	private String ugUniversity;
	private String ugSpecialization;
	private String ugTotalMarks;
	private String ugObtainMarks;
	private String ugPercentage;
	private String ugCourseType;
	private String ugCertificate;
	private String pgCourse;
	private String pgPassingYear;
	private String pgInstitution;
	private String pgUniversity;
	private String pgSpecialization;
	private String pgTotalMarks;
	private String pgObtainMarks;
	private String pgPercentage;
	private String pgCourseType;
	private String pgCertificate;

	private String CompanyName;
	private Date employmentFrom;
	private Date employmentTo;
	private String cinNumber;
	private String regtinNumber;
	private String workNature;
	private String monthlySalary;
	private String temporaryPermanent;
	private String experienceCertificate;

	private String diplomaYear;
	private String diplomaInstitution;
	private String diplomaSpecialization;
	private String diplomaBranch;
	private String diplomaUniversity;
	private String diplomaTotalMarks;
	private String diplomaObtainMarks;
	private String diplomaPercentage;
	private String diplomaCourseType;
	private String diplomaCertificate;
	private String degreeYear;
	private String degreeInstitution;
	private String degreeSpecialization;
	private String degreeCourseType;
	private String degreeBranch;
	private String degreeUniversity;
	private String degreeTotalMarks;
	private String degreeMarksObtain;
	private String degreePercentage;
	private String degreeCertificate;
	private String itiYear;
	private String itiInstitution;
	private String itiSpecialization;
	private String itiCourseType;
	private String itiBranch;
	private String itiUniversity;
	private String itiTotalMarks;
	private String itiMarksObtain;
	private String itiPercentage;
	private String itiCertificate;
	private String payment;
	private String jobId;
	private String finalSubmit;
	private boolean belongCommunity;

//	private String browsePhoto;
//	private String browseSignature;
//	private MultipartFile nccBCertificateDoc;

	private String login;
	private String password;
	private String activated;
	private String langKey;
	private String activationKey;
	private String resetKey;
	private Instant resetDate;
	private String authorityName;

	/*private MultipartFile nccCCertificateDoc;
	private MultipartFile sportsManDoc;
	private MultipartFile highCertificateDoc;
	private MultipartFile StringerCertificateDoc;
	private MultipartFile additionalCertificateDoc;
	private MultipartFile ugCertificateDoc;
	private MultipartFile pgCertificateDoc;
	private MultipartFile diplomaCertificateDoc;
	private MultipartFile degreeCertificateDoc;
	private MultipartFile browsePhotoDoc;
	private MultipartFile browseSignatureDoc;
	private MultipartFile drivingExperienceDoc;
	private MultipartFile drivingLicenseDoc;
	private MultipartFile experienceCertificateDoc;
	private MultipartFile aadhaarCertificateDoc;*/
//	private MultipartFile categoryCertificateDoc;

	public String getRecruitmentZone() {
		return recruitmentZone;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the payment
	 */
	public String isPayment() {
		return payment;
	}

	/**
	 * @param payment the payment to set
	 */
	public void setPayment(String payment) {
		this.payment = payment;
	}

	public void setRecruitmentZone(String recruitmentZone) {
		this.recruitmentZone = recruitmentZone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

//	public boolean isAadhaarCard() {
//		return aadhaarCard;
//	}
//
//	public void setAadhaarCard(boolean aadhaarCard) {
//		this.aadhaarCard = aadhaarCard;
//	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

//	public String getMaritalStatus() {
//		return maritalStatus;
//	}
//
//	public void setMaritalStatus(String maritalStatus) {
//		this.maritalStatus = maritalStatus;
//	}

	public boolean isBeStringCommunity() {
		return beStringCommunity;
	}

	public void setBeStringCommunity(boolean beStringCommunity) {
		this.beStringCommunity = beStringCommunity;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentState() {
		return permanentState;
	}

	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}

	public String getPermanentCity() {
		return permanentCity;
	}

	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getPostalState() {
		return postalState;
	}

	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}

	public String getPostalCity() {
		return postalCity;
	}

	public void setPostalCity(String postalCity) {
		this.postalCity = postalCity;
	}

	

//	public double getHeight() {
//		return height;
//	}
//
//	public void setHeight(double height) {
//		this.height = height;
//	}
//
//	public double getWeight() {
//		return weight;
//	}
//
//	public void setWeight(double weight) {
//		this.weight = weight;
//	}
//
//	public String getBloodGroup() {
//		return bloodGroup;
//	}
//
//	public void setBloodGroup(String bloodGroup) {
//		this.bloodGroup = bloodGroup;
//	}
//
//	public String getEyeSight() {
//		return eyeSight;
//	}
//
//	public void setEyeSight(String eyeSight) {
//		this.eyeSight = eyeSight;
//	}
//
//	public boolean isDisablity() {
//		return disablity;
//	}
//
//	public void setDisablity(boolean disablity) {
//		this.disablity = disablity;
//	}
//
//	public String getDisabilitySpecify() {
//		return disabilitySpecify;
//	}
//
//	public void setDisabilitySpecify(String disabilitySpecify) {
//		this.disabilitySpecify = disabilitySpecify;
//	}

	public String getPostalPincode() {
		return postalPincode;
	}

	public void setPostalPincode(String postalPincode) {
		this.postalPincode = postalPincode;
	}

	public boolean isSonDaughterExservice() {
		return sonDaughterExservice;
	}

	public void setSonDaughterExservice(boolean sonDaughterExservice) {
		this.sonDaughterExservice = sonDaughterExservice;
	}

	public String getArmyNumber() {
		return armyNumber;
	}

	public void setArmyNumber(String armyNumber) {
		this.armyNumber = armyNumber;
	}

	public String getArmyRank() {
		return armyRank;
	}

	public void setArmyRank(String armyRank) {
		this.armyRank = armyRank;
	}

//	public boolean isSonDaughterGref() {
//		return sonDaughterGref;
//	}
//
//	public void setSonDaughterGref(boolean sonDaughterGref) {
//		this.sonDaughterGref = sonDaughterGref;
//	}
//
//	public String getSonDaughterNumber() {
//		return sonDaughterNumber;
//	}
//
//	public void setSonDaughterNumber(String sonDaughterNumber) {
//		this.sonDaughterNumber = sonDaughterNumber;
//	}
//
//	public String getSonDaughterRank() {
//		return sonDaughterRank;
//	}
//
//	public void setSonDaughterRank(String sonDaughterRank) {
//		this.sonDaughterRank = sonDaughterRank;
//	}
//
//	public String getSonDaughterName() {
//		return sonDaughterName;
//	}
//
//	public void setSonDaughterName(String sonDaughterName) {
//		this.sonDaughterName = sonDaughterName;
//	}
//
//	public boolean isBrotherSisterGref() {
//		return brotherSisterGref;
//	}
//
//	public void setBrotherSisterGref(boolean brotherSisterGref) {
//		this.brotherSisterGref = brotherSisterGref;
//	}
//
//	public String getBrotherSisterNumber() {
//		return brotherSisterNumber;
//	}
//
//	public void setBrotherSisterNumber(String brotherSisterNumber) {
//		this.brotherSisterNumber = brotherSisterNumber;
//	}
//
//	public String getBrotherSisterRank() {
//		return brotherSisterRank;
//	}
//
//	public void setBrotherSisterRank(String brotherSisterRank) {
//		this.brotherSisterRank = brotherSisterRank;
//	}
//
//	public String getBrotherSisterName() {
//		return brotherSisterName;
//	}
//
//	public void setBrotherSisterName(String brotherSisterName) {
//		this.brotherSisterName = brotherSisterName;
//	}

	public boolean isNccB() {
		return nccB;
	}

	public void setNccB(boolean nccB) {
		this.nccB = nccB;
	}

	public String getNccbCertificate() {
		return nccbCertificate;
	}

	public void setNccbCertificate(String nccbCertificate) {
		this.nccbCertificate = nccbCertificate;
	}

	public boolean isNccC() {
		return nccC;
	}

	public void setNccC(boolean nccC) {
		this.nccC = nccC;
	}

	public String getNccCertificate() {
		return nccCertificate;
	}

	public void setNccCertificate(String nccCertificate) {
		this.nccCertificate = nccCertificate;
	}

	public boolean isSportsman() {
		return sportsman;
	}

	public void setSportsman(boolean sportsman) {
		this.sportsman = sportsman;
	}

	public String getSportCertificate() {
		return sportCertificate;
	}

	public void setSportCertificate(String sportCertificate) {
		this.sportCertificate = sportCertificate;
	}

//	public boolean isCentralGovt() {
//		return centralGovt;
//	}
//
//	public void setCentralGovt(boolean centralGovt) {
//		this.centralGovt = centralGovt;
//	}
//
//	public String getCentralGovtCertificate() {
//		return centralGovtCertificate;
//	}
//
//	public void setCentralGovtCertificate(String centralGovtCertificate) {
//		this.centralGovtCertificate = centralGovtCertificate;
//	}
//
//	public Date getServedFrom() {
//		return servedFrom;
//	}
//
//	public void setServedFrom(Date servedFrom) {
//		this.servedFrom = servedFrom;
//	}
//
//	public Date getServedTo() {
//		return servedTo;
//	}
//
//	public void setServedTo(Date servedTo) {
//		this.servedTo = servedTo;
//	}
//
//	public boolean isArmedForce() {
//		return armedForce;
//	}
//
//	public void setArmedForce(boolean armedForce) {
//		this.armedForce = armedForce;
//	}
//
//	public String getArmeddisCertificate() {
//		return armeddisCertificate;
//	}
//
//	public void setArmeddisCertificate(String armeddisCertificate) {
//		this.armeddisCertificate = armeddisCertificate;
//	}
//
//	public Date getArmedFrom() {
//		return armedFrom;
//	}
//
//	public void setArmedFrom(Date armedFrom) {
//		this.armedFrom = armedFrom;
//	}
//
//	public Date getArmedTo() {
//		return armedTo;
//	}
//
//	public void setArmedTo(Date armedTo) {
//		this.armedTo = armedTo;
//	}
//
//	public String getArmedarmyNumber() {
//		return armedarmyNumber;
//	}
//
//	public void setArmedarmyNumber(String armedarmyNumber) {
//		this.armedarmyNumber = armedarmyNumber;
//	}

	public String getLicenceNumberLmv() {
		return licenceNumberLmv;
	}

	public void setLicenceNumberLmv(String licenceNumberLmv) {
		this.licenceNumberLmv = licenceNumberLmv;
	}

	public Date getIssueDateLmv() {
		return issueDateLmv;
	}

	public void setIssueDateLmv(Date issueDateLmv) {
		this.issueDateLmv = issueDateLmv;
	}

	public Date getValidUptoLmv() {
		return validUptoLmv;
	}

	public void setValidUptoLmv(Date validUptoLmv) {
		this.validUptoLmv = validUptoLmv;
	}

	public String getIssuingAuthorityLmv() {
		return issuingAuthorityLmv;
	}

	public void setIssuingAuthorityLmv(String issuingAuthorityLmv) {
		this.issuingAuthorityLmv = issuingAuthorityLmv;
	}

	public String getLicenceNumberHgmv() {
		return licenceNumberHgmv;
	}

	public void setLicenceNumberHgmv(String licenceNumberHgmv) {
		this.licenceNumberHgmv = licenceNumberHgmv;
	}

	public Date getIssueDateHgmv() {
		return issueDateHgmv;
	}

	public void setIssueDateHgmv(Date issueDateHgmv) {
		this.issueDateHgmv = issueDateHgmv;
	}

	public Date getValidUptoHgmv() {
		return validUptoHgmv;
	}

	public void setValidUptoHgmv(Date validUptoHgmv) {
		this.validUptoHgmv = validUptoHgmv;
	}

	public String getIssuingAuthorityHlmv() {
		return issuingAuthorityHlmv;
	}

	public void setIssuingAuthorityHlmv(String issuingAuthorityHlmv) {
		this.issuingAuthorityHlmv = issuingAuthorityHlmv;
	}

	public String getLicenceNumberRoad() {
		return licenceNumberRoad;
	}

	public void setLicenceNumberRoad(String licenceNumberRoad) {
		this.licenceNumberRoad = licenceNumberRoad;
	}

	public Date getIssueDateRoad() {
		return issueDateRoad;
	}

	public void setIssueDateRoad(Date issueDateRoad) {
		this.issueDateRoad = issueDateRoad;
	}

	public Date getValidUptoRoad() {
		return validUptoRoad;
	}

	public void setValidUptoRoad(Date validUptoRoad) {
		this.validUptoRoad = validUptoRoad;
	}

	public String getIssuingAuthorityRoad() {
		return issuingAuthorityRoad;
	}

	public void setIssuingAuthorityRoad(String issuingAuthorityRoad) {
		this.issuingAuthorityRoad = issuingAuthorityRoad;
	}

	public String getLicenceNumberOeg() {
		return licenceNumberOeg;
	}

	public void setLicenceNumberOeg(String licenceNumberOeg) {
		this.licenceNumberOeg = licenceNumberOeg;
	}

	public Date getIssueDateOeg() {
		return issueDateOeg;
	}

	public void setIssueDateOeg(Date issueDateOeg) {
		this.issueDateOeg = issueDateOeg;
	}

	public Date getValidUptoOeg() {
		return validUptoOeg;
	}

	public void setValidUptoOeg(Date validUptoOeg) {
		this.validUptoOeg = validUptoOeg;
	}

	public String getIssuingAuthorityOeg() {
		return issuingAuthorityOeg;
	}

	public void setIssuingAuthorityOeg(String issuingAuthorityOeg) {
		this.issuingAuthorityOeg = issuingAuthorityOeg;
	}

	public String getUploadLicence() {
		return uploadLicence;
	}

	public void setUploadLicence(String uploadLicence) {
		this.uploadLicence = uploadLicence;
	}

	public String getHighYear() {
		return highYear;
	}

	public void setHighYear(String highYear) {
		this.highYear = highYear;
	}

	public String getHighInstitution() {
		return highInstitution;
	}

	public void setHighInstitution(String highInstitution) {
		this.highInstitution = highInstitution;
	}

	public String getHighSpecialization() {
		return highSpecialization;
	}

	public void setHighSpecialization(String highSpecialization) {
		this.highSpecialization = highSpecialization;
	}

	public String getHighCourseType() {
		return highCourseType;
	}

	public void setHighCourseType(String highCourseType) {
		this.highCourseType = highCourseType;
	}

	public String getHighBoard() {
		return highBoard;
	}

	public void setHighBoard(String highBoard) {
		this.highBoard = highBoard;
	}

	public String getHighTotalMarks() {
		return highTotalMarks;
	}

	public void setHighTotalMarks(String highTotalMarks) {
		this.highTotalMarks = highTotalMarks;
	}

	public String getHighObtainMarks() {
		return highObtainMarks;
	}

	public void setHighObtainMarks(String highObtainMarks) {
		this.highObtainMarks = highObtainMarks;
	}

	public String getHighPercentage() {
		return highPercentage;
	}

	public void setHighPercentage(String highPercentage) {
		this.highPercentage = highPercentage;
	}

	public String getHighCertificate() {
		return highCertificate;
	}

	public void setHighCertificate(String highCertificate) {
		this.highCertificate = highCertificate;
	}

	public String getHighGrade() {
		return highGrade;
	}

	public void setHighGrade(String highGrade) {
		this.highGrade = highGrade;
	}

	
	public String getInterGrade() {
		return interGrade;
	}

	public void setInterGrade(String interGrade) {
		this.interGrade = interGrade;
	}

	public String getInterYear() {
		return interYear;
	}

	public void setInterYear(String interYear) {
		this.interYear = interYear;
	}

	public String getInterInstitution() {
		return interInstitution;
	}

	public void setInterInstitution(String interInstitution) {
		this.interInstitution = interInstitution;
	}

	public String getInterSpecialization() {
		return interSpecialization;
	}

	public void setInterSpecialization(String interSpecialization) {
		this.interSpecialization = interSpecialization;
	}

	public String getInterCourseType() {
		return interCourseType;
	}

	public void setInterCourseType(String interCourseType) {
		this.interCourseType = interCourseType;
	}

	public String getInterBoard() {
		return interBoard;
	}

	public void setInterBoard(String interBoard) {
		this.interBoard = interBoard;
	}

	public String getInterTotalMarks() {
		return interTotalMarks;
	}

	public void setInterTotalMarks(String interTotalMarks) {
		this.interTotalMarks = interTotalMarks;
	}

	public String getInterObtainMarks() {
		return interObtainMarks;
	}

	public void setInterObtainMarks(String interObtainMarks) {
		this.interObtainMarks = interObtainMarks;
	}

	public String getInterPercentage() {
		return interPercentage;
	}

	public void setInterPercentage(String interPercentage) {
		this.interPercentage = interPercentage;
	}

	public String getInterCertificate() {
		return interCertificate;
	}

	public void setInterCertificate(String interCertificate) {
		this.interCertificate = interCertificate;
	}

	public String getEnglishTyping() {
		return englishTyping;
	}

	public void setEnglishTyping(String englishTyping) {
		this.englishTyping = englishTyping;
	}

	public String getHindiTyping() {
		return hindiTyping;
	}

	public void setHindiTyping(String hindiTyping) {
		this.hindiTyping = hindiTyping;
	}

	public String getHindiStenography() {
		return hindiStenography;
	}

	public void setHindiStenography(String hindiStenography) {
		this.hindiStenography = hindiStenography;
	}

	public String getEnglishStenography() {
		return englishStenography;
	}

	public void setEnglishStenography(String englishStenography) {
		this.englishStenography = englishStenography;
	}

	public String getAdditionalCertificate() {
		return additionalCertificate;
	}

	public void setAdditionalCertificate(String additionalCertificate) {
		this.additionalCertificate = additionalCertificate;
	}

	public String getFirstaidCertificate() {
		return firstaidCertificate;
	}

	public void setFirstaidCertificate(String firstaidCertificate) {
		this.firstaidCertificate = firstaidCertificate;
	}

	public String getUgCourse() {
		return ugCourse;
	}

	public void setUgCourse(String ugCourse) {
		this.ugCourse = ugCourse;
	}

	public String getUgPassingYear() {
		return ugPassingYear;
	}

	public void setUgPassingYear(String ugPassingYear) {
		this.ugPassingYear = ugPassingYear;
	}

	public String getUgInstitution() {
		return ugInstitution;
	}

	public void setUgInstitution(String ugInstitution) {
		this.ugInstitution = ugInstitution;
	}

	public String getUgUniversity() {
		return ugUniversity;
	}

	public void setUgUniversity(String ugUniversity) {
		this.ugUniversity = ugUniversity;
	}

	public String getUgSpecialization() {
		return ugSpecialization;
	}

	public void setUgSpecialization(String ugSpecialization) {
		this.ugSpecialization = ugSpecialization;
	}

	public String getUgTotalMarks() {
		return ugTotalMarks;
	}

	public void setUgTotalMarks(String ugTotalMarks) {
		this.ugTotalMarks = ugTotalMarks;
	}

	public String getUgObtainMarks() {
		return ugObtainMarks;
	}

	public void setUgObtainMarks(String ugObtainMarks) {
		this.ugObtainMarks = ugObtainMarks;
	}

	public String getUgPercentage() {
		return ugPercentage;
	}

	public void setUgPercentage(String ugPercentage) {
		this.ugPercentage = ugPercentage;
	}

	public String getUgCourseType() {
		return ugCourseType;
	}

	public void setUgCourseType(String ugCourseType) {
		this.ugCourseType = ugCourseType;
	}

	public String getUgCertificate() {
		return ugCertificate;
	}

	public void setUgCertificate(String ugCertificate) {
		this.ugCertificate = ugCertificate;
	}

	public String getPgCourse() {
		return pgCourse;
	}

	public void setPgCourse(String pgCourse) {
		this.pgCourse = pgCourse;
	}

	public String getPgPassingYear() {
		return pgPassingYear;
	}

	public void setPgPassingYear(String pgPassingYear) {
		this.pgPassingYear = pgPassingYear;
	}

	public String getPgInstitution() {
		return pgInstitution;
	}

	public void setPgInstitution(String pgInstitution) {
		this.pgInstitution = pgInstitution;
	}

	public String getPgUniversity() {
		return pgUniversity;
	}

	public void setPgUniversity(String pgUniversity) {
		this.pgUniversity = pgUniversity;
	}

	public String getPgSpecialization() {
		return pgSpecialization;
	}

	public void setPgSpecialization(String pgSpecialization) {
		this.pgSpecialization = pgSpecialization;
	}

	public String getPgTotalMarks() {
		return pgTotalMarks;
	}

	public void setPgTotalMarks(String pgTotalMarks) {
		this.pgTotalMarks = pgTotalMarks;
	}

	public String getPgObtainMarks() {
		return pgObtainMarks;
	}

	public void setPgObtainMarks(String pgObtainMarks) {
		this.pgObtainMarks = pgObtainMarks;
	}

	public String getPgPercentage() {
		return pgPercentage;
	}

	public void setPgPercentage(String pgPercentage) {
		this.pgPercentage = pgPercentage;
	}

	public String getPgCourseType() {
		return pgCourseType;
	}

	public void setPgCourseType(String pgCourseType) {
		this.pgCourseType = pgCourseType;
	}

	public String getPgCertificate() {
		return pgCertificate;
	}

	public void setPgCertificate(String pgCertificate) {
		this.pgCertificate = pgCertificate;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public Date getEmploymentFrom() {
		return employmentFrom;
	}

	public void setEmploymentFrom(Date employmentFrom) {
		this.employmentFrom = employmentFrom;
	}

	public Date getEmploymentTo() {
		return employmentTo;
	}

	public void setEmploymentTo(Date employmentTo) {
		this.employmentTo = employmentTo;
	}

	public String getCinNumber() {
		return cinNumber;
	}

	public void setCinNumber(String cinNumber) {
		this.cinNumber = cinNumber;
	}

	public String getRegtinNumber() {
		return regtinNumber;
	}

	public void setRegtinNumber(String regtinNumber) {
		this.regtinNumber = regtinNumber;
	}

	public String getWorkNature() {
		return workNature;
	}

	public void setWorkNature(String workNature) {
		this.workNature = workNature;
	}

	public String getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(String monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public String getTemporaryPermanent() {
		return temporaryPermanent;
	}

	public void setTemporaryPermanent(String temporaryPermanent) {
		this.temporaryPermanent = temporaryPermanent;
	}

	public String getExperienceCertificate() {
		return experienceCertificate;
	}

	public void setExperienceCertificate(String experienceCertificate) {
		this.experienceCertificate = experienceCertificate;
	}

	public String getDiplomaYear() {
		return diplomaYear;
	}

	public void setDiplomaYear(String diplomaYear) {
		this.diplomaYear = diplomaYear;
	}

	public String getDiplomaInstitution() {
		return diplomaInstitution;
	}

	public void setDiplomaInstitution(String diplomaInstitution) {
		this.diplomaInstitution = diplomaInstitution;
	}

	public String getDiplomaSpecialization() {
		return diplomaSpecialization;
	}

	public void setDiplomaSpecialization(String diplomaSpecialization) {
		this.diplomaSpecialization = diplomaSpecialization;
	}

	public String getDiplomaBranch() {
		return diplomaBranch;
	}

	public void setDiplomaBranch(String diplomaBranch) {
		this.diplomaBranch = diplomaBranch;
	}

	public String getDiplomaUniversity() {
		return diplomaUniversity;
	}

	public void setDiplomaUniversity(String diplomaUniversity) {
		this.diplomaUniversity = diplomaUniversity;
	}

	public String getDiplomaTotalMarks() {
		return diplomaTotalMarks;
	}

	public void setDiplomaTotalMarks(String diplomaTotalMarks) {
		this.diplomaTotalMarks = diplomaTotalMarks;
	}

	public String getDiplomaObtainMarks() {
		return diplomaObtainMarks;
	}

	public void setDiplomaObtainMarks(String diplomaObtainMarks) {
		this.diplomaObtainMarks = diplomaObtainMarks;
	}

	public String getDiplomaPercentage() {
		return diplomaPercentage;
	}

	public void setDiplomaPercentage(String diplomaPercentage) {
		this.diplomaPercentage = diplomaPercentage;
	}

	public String getDiplomaCourseType() {
		return diplomaCourseType;
	}

	public void setDiplomaCourseType(String diplomaCourseType) {
		this.diplomaCourseType = diplomaCourseType;
	}

	public String getDiplomaCertificate() {
		return diplomaCertificate;
	}

	public void setDiplomaCertificate(String diplomaCertificate) {
		this.diplomaCertificate = diplomaCertificate;
	}

	public String getDegreeYear() {
		return degreeYear;
	}

	public void setDegreeYear(String degreeYear) {
		this.degreeYear = degreeYear;
	}

	public String getDegreeInstitution() {
		return degreeInstitution;
	}

	public void setDegreeInstitution(String degreeInstitution) {
		this.degreeInstitution = degreeInstitution;
	}

	public String getDegreeSpecialization() {
		return degreeSpecialization;
	}

	public void setDegreeSpecialization(String degreeSpecialization) {
		this.degreeSpecialization = degreeSpecialization;
	}

	public String getDegreeCourseType() {
		return degreeCourseType;
	}

	public void setDegreeCourseType(String degreeCourseType) {
		this.degreeCourseType = degreeCourseType;
	}

	public String getDegreeBranch() {
		return degreeBranch;
	}

	public void setDegreeBranch(String degreeBranch) {
		this.degreeBranch = degreeBranch;
	}

	public String getDegreeUniversity() {
		return degreeUniversity;
	}

	public void setDegreeUniversity(String degreeUniversity) {
		this.degreeUniversity = degreeUniversity;
	}

	public String getDegreeTotalMarks() {
		return degreeTotalMarks;
	}

	public void setDegreeTotalMarks(String degreeTotalMarks) {
		this.degreeTotalMarks = degreeTotalMarks;
	}

	public String getDegreeMarksObtain() {
		return degreeMarksObtain;
	}

	public void setDegreeMarksObtain(String degreeMarksObtain) {
		this.degreeMarksObtain = degreeMarksObtain;
	}

	public String getDegreePercentage() {
		return degreePercentage;
	}

	public void setDegreePercentage(String degreePercentage) {
		this.degreePercentage = degreePercentage;
	}

	public String getDegreeCertificate() {
		return degreeCertificate;
	}

	public void setDegreeCertificate(String degreeCertificate) {
		this.degreeCertificate = degreeCertificate;
	}

	public String getItiYear() {
		return itiYear;
	}

	public void setItiYear(String itiYear) {
		this.itiYear = itiYear;
	}

	public String getItiInstitution() {
		return itiInstitution;
	}

	public void setItiInstitution(String itiInstitution) {
		this.itiInstitution = itiInstitution;
	}

	public String getItiSpecialization() {
		return itiSpecialization;
	}

	public void setItiSpecialization(String itiSpecialization) {
		this.itiSpecialization = itiSpecialization;
	}

	public String getItiCourseType() {
		return itiCourseType;
	}

	public void setItiCourseType(String itiCourseType) {
		this.itiCourseType = itiCourseType;
	}

	public String getItiBranch() {
		return itiBranch;
	}

	public void setItiBranch(String itiBranch) {
		this.itiBranch = itiBranch;
	}

	public String getItiUniversity() {
		return itiUniversity;
	}

	public void setItiUniversity(String itiUniversity) {
		this.itiUniversity = itiUniversity;
	}

	public String getItiTotalMarks() {
		return itiTotalMarks;
	}

	public void setItiTotalMarks(String itiTotalMarks) {
		this.itiTotalMarks = itiTotalMarks;
	}

	public String getItiMarksObtain() {
		return itiMarksObtain;
	}

	public void setItiMarksObtain(String itiMarksObtain) {
		this.itiMarksObtain = itiMarksObtain;
	}

	public String getItiPercentage() {
		return itiPercentage;
	}

	public void setItiPercentage(String itiPercentage) {
		this.itiPercentage = itiPercentage;
	}

	public String getItiCertificate() {
		return itiCertificate;
	}

	public void setItiCertificate(String itiCertificate) {
		this.itiCertificate = itiCertificate;
	}

	public String isFinalSubmit() {
		return finalSubmit;
	}

	public void setFinalSubmit(String finalSubmit) {
		this.finalSubmit = finalSubmit;
	}

	public boolean isBelongCommunity() {
		return belongCommunity;
	}

	public void setBelongCommunity(boolean belongCommunity) {
		this.belongCommunity = belongCommunity;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getActivated() {
		return activated;
	}

	public void setActivated(String activated) {
		this.activated = activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public String getActivationKey() {
		return activationKey;
	}

	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public Instant getResetDate() {
		return resetDate;
	}

	public void setResetDate(Instant resetDate) {
		this.resetDate = resetDate;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public String getPayment() {
		return payment;
	}

//	public boolean isEmpExGref() {
//		return empExGref;
//	}
//
//	public void setEmpExGref(boolean empExGref) {
//		this.empExGref = empExGref;
//	}
//
//	public Date getServedBroFrom() {
//		return servedBroFrom;
//	}
//
//	public void setServedBroFrom(Date servedBroFrom) {
//		this.servedBroFrom = servedBroFrom;
//	}
//
//	public Date getServedBroTo() {
//		return servedBroTo;
//	}
//
//	public void setServedBroTo(Date servedBroTo) {
//		this.servedBroTo = servedBroTo;
//	}
//
//	public String getGsNumber() {
//		return gsNumber;
//	}
//
//	public void setGsNumber(String gsNumber) {
//		this.gsNumber = gsNumber;
//	}
//
	public String getCategoryCertificate() {
		return categoryCertificate;
	}

	public void setCategoryCertificate(String categoryCertificate) {
		this.categoryCertificate = categoryCertificate;
	}

	public String getFinalSubmit() {
		return finalSubmit;
	}

	public boolean isSonExGps() {
		return sonExGps;
	}

	public void setSonExGps(boolean sonExGps) {
		this.sonExGps = sonExGps;
	}

	public String getSonGsNumber() {
		return sonGsNumber;
	}

	public void setSonGsNumber(String sonGsNumber) {
		this.sonGsNumber = sonGsNumber;
	}

	public String getSonGpsRank() {
		return sonGpsRank;
	}

	public void setSonGpsRank(String sonGpsRank) {
		this.sonGpsRank = sonGpsRank;
	}

	public String getSonGpsName() {
		return sonGpsName;
	}

	public void setSonGpsName(String sonGpsName) {
		this.sonGpsName = sonGpsName;
	}

	public String getSonUnitName() {
		return sonUnitName;
	}

	public void setSonUnitName(String sonUnitName) {
		this.sonUnitName = sonUnitName;
	}

	public String getUnitCertificate() {
		return unitCertificate;
	}

	public void setUnitCertificate(String unitCertificate) {
		this.unitCertificate = unitCertificate;
	}

	public boolean isReapptCandidate() {
		return reapptCandidate;
	}

	public void setReapptCandidate(boolean reapptCandidate) {
		this.reapptCandidate = reapptCandidate;
	}

	public String getReapptCandidateGs() {
		return reapptCandidateGs;
	}

	public void setReapptCandidateGs(String reapptCandidateGs) {
		this.reapptCandidateGs = reapptCandidateGs;
	}

	public String getReaaptCandidateRank() {
		return reaaptCandidateRank;
	}

	public void setReaaptCandidateRank(String reaaptCandidateRank) {
		this.reaaptCandidateRank = reaaptCandidateRank;
	}

	public String getReapptCandidateName() {
		return reapptCandidateName;
	}

	public void setReapptCandidateName(String reapptCandidateName) {
		this.reapptCandidateName = reapptCandidateName;
	}

	public String getReapptCandidateUnit() {
		return reapptCandidateUnit;
	}

	public void setReapptCandidateUnit(String reapptCandidateUnit) {
		this.reapptCandidateUnit = reapptCandidateUnit;
	}

	public boolean isSonExservice() {
		return sonExservice;
	}

	public void setSonExservice(boolean sonExservice) {
		this.sonExservice = sonExservice;
	}

	public String getArmyName() {
		return armyName;
	}

	public void setArmyName(String armyName) {
		this.armyName = armyName;
	}

	public String getLastUnitCertificate() {
		return lastUnitCertificate;
	}

	public void setLastUnitCertificate(String lastUnitCertificate) {
		this.lastUnitCertificate = lastUnitCertificate;
	}

	public boolean isExService() {
		return exService;
	}

	public void setExService(boolean exService) {
		this.exService = exService;
	}

	public String getDischargeCertificate() {
		return dischargeCertificate;
	}

	public void setDischargeCertificate(String dischargeCertificate) {
		this.dischargeCertificate = dischargeCertificate;
	}

	public String getSportsLevel() {
		return sportsLevel;
	}

	public void setSportsLevel(String sportsLevel) {
		this.sportsLevel = sportsLevel;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPancertificate() {
		return pancertificate;
	}

	public void setPancertificate(String pancertificate) {
		this.pancertificate = pancertificate;
	}

	public boolean isCasualplabour() {
		return casualplabour;
	}

	public void setCasualplabour(boolean casualplabour) {
		this.casualplabour = casualplabour;
	}

	public String getCplCertificate() {
		return cplCertificate;
	}

	public void setCplCertificate(String cplCertificate) {
		this.cplCertificate = cplCertificate;
	}

	public String getArmyLastUnit() {
		return armyLastUnit;
	}

	public void setArmyLastUnit(String armyLastUnit) {
		this.armyLastUnit = armyLastUnit;
	}

		

}
