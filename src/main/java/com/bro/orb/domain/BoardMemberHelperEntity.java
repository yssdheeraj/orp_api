package com.bro.orb.domain;

import java.util.List;

public class BoardMemberHelperEntity {

	private long id;
	private String groupName;
	private long postId;
	private List<String> userslist;
	
	public BoardMemberHelperEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public BoardMemberHelperEntity(long id, String groupName, long postId, List<String> userslist) {
		super();
		this.id = id;
		this.groupName = groupName;
		this.postId = postId;
		this.userslist = userslist;
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public List<String> getUserslist() {
		return userslist;
	}
	public void setUserslist(List<String> userslist) {
		this.userslist = userslist;
	}
	
	
	@Override
	public String toString() {
		return "BoardMemberHelperEntity [id=" + id + ", groupName=" + groupName + ", postId=" + postId + ", userslist="
				+ userslist + "]";
	}
	
	
}
