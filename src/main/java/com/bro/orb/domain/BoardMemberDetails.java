package com.bro.orb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="admin_board_member")
public class BoardMemberDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	
	@OneToOne
	private JobDetails jobId;
	
	private String data_time;
	
	public BoardMemberDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BoardMemberDetails(long id, String name, JobDetails jobId, String data_time) {
		super();
		this.id = id;
		this.name = name;
		this.jobId = jobId;
		this.data_time = data_time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JobDetails getJobId() {
		return jobId;
	}

	public void setJobId(JobDetails jobId) {
		this.jobId = jobId;
	}

	public String getData_time() {
		return data_time;
	}

	public void setData_time(String data_time) {
		this.data_time = data_time;
	}

	@Override
	public String toString() {
		return "BoardMemberDetails [id=" + id + ", name=" + name + ", jobId=" + jobId + ", data_time=" + data_time
				+ "]";
	}

	
	
	

}
