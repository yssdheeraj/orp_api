package com.bro.orb.domain;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class FingerPrintInformetions {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String registrationId;
	private byte[] ansiTemplate;
	private byte[] isoTemplate;
	private String fingerImage;
	private String ansiImage;
	private String isoImage;
	private String nfiq;
	private String rawData;
	private String wsqImage;
	private String status;
	private String urlThumb;
	
	@OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "personal_detailsID",  referencedColumnName="id")
    private PersonalDetails personalDetails;
	
/*	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user; */
	
	public FingerPrintInformetions() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public FingerPrintInformetions(Long id, String registrationId, byte[] ansiTemplate, byte[] isoTemplate,
			String fingerImage, String ansiImage, String isoImage, String nfiq, String rawData, String wsqImage,
			String status, String urlThumb, PersonalDetails personalDetails) {
		super();
		this.id = id;
		this.registrationId = registrationId;
		this.ansiTemplate = ansiTemplate;
		this.isoTemplate = isoTemplate;
		this.fingerImage = fingerImage;
		this.ansiImage = ansiImage;
		this.isoImage = isoImage;
		this.nfiq = nfiq;
		this.rawData = rawData;
		this.wsqImage = wsqImage;
		this.status = status;
		this.urlThumb = urlThumb;
		this.personalDetails = personalDetails;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public byte[] getAnsiTemplate() {
		return ansiTemplate;
	}

	public void setAnsiTemplate(byte[] ansiTemplate) {
		this.ansiTemplate = ansiTemplate;
	}

	public byte[] getIsoTemplate() {
		return isoTemplate;
	}

	public void setIsoTemplate(byte[] isoTemplate) {
		this.isoTemplate = isoTemplate;
	}

	public String getFingerImage() {
		return fingerImage;
	}

	public void setFingerImage(String fingerImage) {
		this.fingerImage = fingerImage;
	}

	public String getAnsiImage() {
		return ansiImage;
	}

	public void setAnsiImage(String ansiImage) {
		this.ansiImage = ansiImage;
	}

	public String getIsoImage() {
		return isoImage;
	}

	public void setIsoImage(String isoImage) {
		this.isoImage = isoImage;
	}

	public String getNfiq() {
		return nfiq;
	}

	public void setNfiq(String nfiq) {
		nfiq = nfiq;
	}

	public String getRawData() {
		return rawData;
	}

	public void setRawData(String rawData) {
		rawData = rawData;
	}

	public String getWsqImage() {
		return wsqImage;
	}

	public void setWsqImage(String wsqImage) {
		this.wsqImage = wsqImage;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}
	

	public String getUrlThumb() {
		return urlThumb;
	}


	public void setUrlThumb(String urlThumb) {
		this.urlThumb = urlThumb;
	}


	@Override
	public String toString() {
		return "FingerPrintInformetions [id=" + id + ", registrationId=" + registrationId + ", ansiTemplate="
				+ Arrays.toString(ansiTemplate) + ", isoTemplate=" + Arrays.toString(isoTemplate) + ", fingerImage="
				+ fingerImage + ", ansiImage=" + ansiImage + ", isoImage=" + isoImage + ", nfiq=" + nfiq + ", rawData="
				+ rawData + ", wsqImage=" + wsqImage + ", status=" + status + ", urlThumb=" + urlThumb
				+ ", personalDetails=" + personalDetails + "]";
	}


	


	
}
