package com.bro.orb.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bro.orb.domain.Authority;
import com.bro.orb.domain.CandidateListFilter;
import com.bro.orb.domain.DrivingLicense;
import com.bro.orb.domain.EssentialQualification;
import com.bro.orb.domain.GraduationQualification;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.NewCandidateRegistration;
import com.bro.orb.domain.OrganisationExperience;
import com.bro.orb.domain.PersonalDetails;
import com.bro.orb.domain.RegistrationBean;
import com.bro.orb.domain.TechnicalQualification;
import com.bro.orb.domain.UploadCertificate;
import com.bro.orb.domain.User;
import com.bro.orb.repository.AuthorityRepository;
import com.bro.orb.repository.JobAmountRepository;
import com.bro.orb.repository.PersonalRepository;
import com.bro.orb.repository.RegistrationRepository;
import com.bro.orb.security.AuthoritiesConstants;
import com.bro.orb.service.util.RandomUtil;

import liquibase.util.StringUtils;

@Service
@Transactional
public class RegisterService {

	private final Logger log = LoggerFactory.getLogger(RegisterService.class);

	@Value("${filepath.uploadpath}")
	//private String UPLOADED_FOLDER;
	private static String UPLOADED_FOLDER ="E:/1_new_orp_gref/new_orp_ui/ORP/src/assets/images/";
	@Autowired
	private PersonalRepository personalRepository;

	@Autowired
	private AuthorityRepository authorityRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JobAmountRepository jobAmountRepository;

	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private MailService mailService;

	/*
	 * public void registerCandidate(RegistrationBean personal, MultipartFile
	 * categoryDoc, MultipartFile nccBCertificateDoc, MultipartFile
	 * nccCCertificateDoc, MultipartFile sportsManDoc, MultipartFile
	 * aadhaarCertificateDoc, MultipartFile highCertificateDoc, MultipartFile
	 * interCertificateDoc, MultipartFile additionalCertificateDoc, MultipartFile
	 * ugCertificateDoc, MultipartFile pgCertificateDoc, MultipartFile
	 * diplomaCertificateDoc, MultipartFile degreeCertificateDoc, MultipartFile
	 * browsePhotoDoc, MultipartFile browseSignatureDoc, MultipartFile
	 * drivingExperienceDoc, MultipartFile drivingLicenseDoc, MultipartFile
	 * experienceCertificateDoc) { // TODO Auto-generated method stub
	 * 
	 * }
	 */
	
	
	public void newcandidateRegister(RegistrationBean registrationBean) {

		NewCandidateRegistration candidateRegistration = new NewCandidateRegistration();
		candidateRegistration.setRegistrationId(registrationBean.getRegistrationId());
		candidateRegistration.setAge(registrationBean.getAge());
		candidateRegistration.setDateOfBirth(registrationBean.getDateOfBirth());
		candidateRegistration.setEmailAddress(registrationBean.getEmailAddress());
		candidateRegistration.setFatherName(registrationBean.getFatherName());
//		candidateRegistration.setFirstName(registrationBean.getFirstName());
//		candidateRegistration.setMiddleName(registrationBean.getMiddleName());
//		candidateRegistration.setLastName(registrationBean.getLastName());
		candidateRegistration.setCandidateName(registrationBean.getFirstName()+registrationBean.getMiddleName()+registrationBean.getLastName());
		candidateRegistration.setGender(registrationBean.getGender());
		candidateRegistration.setNationality(registrationBean.getNationality());
		candidateRegistration.setMobileNumber(registrationBean.getMobileNumber());
		
		User user = new User();
		user.setLogin(registrationBean.getRegistrationId());
		user.setFirstName(registrationBean.getFirstName());
		user.setLastName(registrationBean.getLastName());
		user.setEmail(registrationBean.getEmailAddress());
		user.setActivated(true);
		user.setActivationKey(registrationBean.getActivationKey());
		if (registrationBean.getActivationKey() == null) {
			user.setLangKey("en"); // default laguage
		}
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.CANDIDATE);
		Set<Authority> authorities = new HashSet<>();
		authorities.add(authority);
		user.setAuthorities(authorities);
		
		/*
		 * String encryptedPassword =
		 * passwordEncoder.encode(generatePassword(registrationBean));
		 */
/*		String encryptedPassword = passwordEncoder.encode(generatePassword());*/
		log.info("\n\n----------------------------------------\n\t"+"Passwrod:\t" +registrationBean.getPassword()+"\n----------------------------------------\n");
		String encryptedPassword = passwordEncoder.encode(registrationBean.getPassword());
		user.setPassword(encryptedPassword);
		candidateRegistration.setUser(user);

		registrationRepository.save(candidateRegistration);
	}
	
	public void registerCandidate(RegistrationBean register, MultipartFile categoryDoc,
			MultipartFile nccBCertificateDoc, MultipartFile nccCCertificateDoc, MultipartFile sportsManDoc,
			MultipartFile aadhaarCertificateDoc, MultipartFile highCertificateDoc, MultipartFile interCertificateDoc,
			MultipartFile additionalCertificateDoc, MultipartFile ugCertificateDoc, MultipartFile pgCertificateDoc,
			MultipartFile diplomaCertificateDoc, MultipartFile degreeCertificateDoc,MultipartFile itiCertificateDoc, MultipartFile browsePhotoDoc,
			MultipartFile browseSignatureDoc, MultipartFile drivingExperienceDoc, MultipartFile drivingLicenseDoc,
			MultipartFile experienceCertificateDoc, MultipartFile centralCertificateDoc, MultipartFile armedCertificateDoc,MultipartFile firstaidCertificateDoc,
			MultipartFile lastUnitCertificateDoc,MultipartFile panCertificateDoc,MultipartFile cplCertificateDoc,MultipartFile dischargeCertificateDoc,MultipartFile unitCertificateDoc) {

		
		PersonalDetails personalDetails = new PersonalDetails();
		personalDetails.setRegistrationId(register.getRegistrationId());
		personalDetails.setPayment(setBooleanNull(register.isPayment()));
		personalDetails.setFirstName(register.getFirstName());
		personalDetails.setMiddleName(register.getMiddleName());
		personalDetails.setLastName(register.getLastName());
		personalDetails.setFatherName(register.getFatherName());
		personalDetails.setMotherName(register.getMotherName());
		personalDetails.setDateOfBirth(register.getDateOfBirth());
		personalDetails.setMobileNumber(register.getMobileNumber());
		personalDetails.setEmailAddress(register.getEmailAddress());
		personalDetails.setNationality(register.getNationality());
//		personalDetails.setAadhaarCard(register.isAadhaarCard());
		personalDetails.setAadhaarNumber(register.getAadhaarNumber());
//		personalDetails.setMaritalStatus(register.getMaritalStatus());
		personalDetails.setCategory(register.getCategory());
		personalDetails.setBelongCommunity(register.isBelongCommunity());
		personalDetails.setCommunityName(register.getCommunityName());
		personalDetails.setPermanentAddress(register.getPermanentAddress());
		personalDetails.setPermanentCity(register.getPermanentCity());
		personalDetails.setPermanentState(register.getPermanentState());
		personalDetails.setPermanentPincode(setIntegerNull(register.getPermanentPincode()));
		personalDetails.setPostalAddress(register.getPostalAddress());
		personalDetails.setPostalCity(register.getPostalCity());
		personalDetails.setPostalState(register.getPostalState());
		personalDetails.setPostalPincode(setIntegerNull(register.getPostalPincode()));
//		personalDetails.setSonDaughterExservice(register.isSonDaughterGref());
		personalDetails.setArmyRank(register.getArmyRank());
		personalDetails.setArmyNumber(register.getArmyNumber());
//		personalDetails.setSonDaughterGref(register.isSonDaughterGref());
//		personalDetails.setSonDaughterNumber(register.getSonDaughterNumber());
//		personalDetails.setSonDaughterRank(register.getSonDaughterRank());
//		personalDetails.setSonDaughterName(register.getSonDaughterName());
//		personalDetails.setBrotherSisterGref(register.isBrotherSisterGref());
//		personalDetails.setBrotherSisterNumber(register.getBrotherSisterNumber());
//		personalDetails.setBrotherSisterRank(register.getBrotherSisterRank());
//		personalDetails.setBrotherSisterName(register.getBrotherSisterName());
		personalDetails.setNccB(register.isNccB());
//		personalDetails.setCandidateHeight((register.getHeight()));
//		personalDetails.setCandidateWeight(register.getWeight());
//		personalDetails.setBloodGroup(register.getBloodGroup());
//		personalDetails.setEyeSight(register.getEyeSight());
		personalDetails.setAge(register.getAge());
//		personalDetails.setDisablity(register.isDisablity());
//		personalDetails.setDisabilitySpecify((register.getDisabilitySpecify()));
//		personalDetails.setCentralGovt(register.isCentralGovt());
//		
//		personalDetails.setArmedForce(register.isArmedForce());
//		
//		personalDetails.setServedFrom(register.getServedFrom());
//		personalDetails.setServedTo(register.getServedTo());
//		personalDetails.setArmedarmyNumber(register.getArmedarmyNumber());
//		
//		personalDetails.setArmedFrom(register.getArmedFrom());
//		personalDetails.setArmedTo(register.getArmedTo());
//		personalDetails.setEmpExGref(register.isEmpExGref());
//		personalDetails.setServedBroFrom(register.getServedBroFrom());
//		personalDetails.setServedBroTo(register.getServedBroTo());
//		personalDetails.setGsNumber(register.getGsNumber());
//		personalDetails.setBrotherSisterGref(register.isBrotherSisterGref());
		
		personalDetails.setCasualplabour(register.isCasualplabour());
		personalDetails.setSonExGps(register.isSonExGps());
		personalDetails.setSonGsNumber(register.getSonGsNumber());
		personalDetails.setSonGpsRank(register.getSonGpsRank());
		personalDetails.setSonGpsName(register.getSonGpsName());
		personalDetails.setSonUnitName(register.getSonUnitName());
	    personalDetails.setReapptCandidate(register.isReapptCandidate());
		personalDetails.setReapptCandidateGs(register.getReapptCandidateGs());
        personalDetails.setReapptCandidateName(register.getReapptCandidateName());
        personalDetails.setReapptCandidateUnit(register.getReapptCandidateUnit());
        personalDetails.setReaaptCandidateRank(register.getReaaptCandidateRank());
        personalDetails.setSonExservice(register.isExService());
        personalDetails.setArmyNumber(register.getArmyNumber());
        personalDetails.setArmyRank(register.getArmyRank());
        personalDetails.setArmyName(register.getArmyName());
        personalDetails.setArmyLastUnit(register.getArmyLastUnit());
        personalDetails.setExService(register.isExService());
        personalDetails.setPanNumber(register.getPanNumber());
        personalDetails.setSportsman(register.isSportsman());
        personalDetails.setSportsLevel(register.getSportsLevel());
		
		

//		try {
//			String CENTRAL = uploadDocuments(register.getRegistrationId(), centralCertificateDoc, "CENTRAL");
//
//			personalDetails.setCentralGovtCertificate(CENTRAL != null ? CENTRAL : null);
//		} catch (Exception e) {
//			e.getMessage();
//		}
//		
//		try {
//			String ARMEDFORCE = uploadDocuments(register.getRegistrationId(), armedCertificateDoc, "ARMEDFORCE");
//
//			personalDetails.setArmeddisCertificate(ARMEDFORCE != null ? ARMEDFORCE : null);
//		} catch (Exception e) {
//			e.getMessage();
//		}
		
		try {
			String CATEGORY = uploadDocuments(register.getRegistrationId(), categoryDoc, "CATEGORY");

			personalDetails.setCategoryCertificate(CATEGORY != null ? CATEGORY : null);
		} catch (Exception e) {
			e.getMessage();
		}

//		try {
//			String AADHAR = uploadDocuments(register.getRegistrationId(), aadhaarCertificateDoc, "AADHAR");
//			personalDetails.setAadharCertificate(AADHAR != null ? AADHAR : null);
//		} catch (Exception e) {
//			e.getMessage();
//		}

		
		try {
			String NCCB = uploadDocuments(register.getRegistrationId(), nccBCertificateDoc, "NCCB");
			personalDetails.setNccbCertificate(NCCB != null ? NCCB : null);
		} catch (Exception e) {
			e.getMessage();
		}
		personalDetails.setNccC(register.isNccC());
		try {
			String NCCC = uploadDocuments(register.getRegistrationId(), nccCCertificateDoc, "NCCC");
			personalDetails.setNccCertificate(NCCC != null ? NCCC : null);
		} catch (Exception e) {
			e.getMessage();
		}
		personalDetails.setSportsman(register.isSportsman());
		try {
			String SPORTS = uploadDocuments(register.getRegistrationId(), sportsManDoc, "SPORTS");
			personalDetails.setSportCertificate(SPORTS != null ? SPORTS : null);
		} catch (Exception e) {
			e.getMessage();
		}
		personalDetails.setSonExGps(register.isSonExGps());
		try {
			String SONEXGPS = uploadDocuments(register.getRegistrationId(), unitCertificateDoc, "SONEXGPS");
			personalDetails.setUnitCertificate(SONEXGPS !=null ? SONEXGPS : null);
		}catch(Exception e)
		{
			e.getMessage();
		}
		personalDetails.setSonExservice(register.isSonExservice());
		try {
			String SONEXSERVICE = uploadDocuments(register.getRegistrationId(), lastUnitCertificateDoc, "SONEXSERVICE");
			personalDetails.setLastUnitCertificate(SONEXSERVICE !=null ? SONEXSERVICE : null);
		}catch(Exception e)
		{
			e.getMessage();
		}
		personalDetails.setExService(register.isExService());
		try {
			String EXSERVICE = uploadDocuments(register.getRegistrationId(), dischargeCertificateDoc, "EXSERVICE");
			personalDetails.setDischargeCertificate(EXSERVICE != null ? EXSERVICE : null);
		}catch(Exception e)
		{
			e.getMessage();
		}
		personalDetails.setPanNumber(register.getPanNumber());
		try {
			String PANNUMBER = uploadDocuments(register.getRegistrationId(), panCertificateDoc, "PANNUMBER");
			personalDetails.setPancertificate(PANNUMBER != null ? PANNUMBER : null);
		}catch(Exception e)
		{
			e.getMessage();
		}
		personalDetails.setCasualplabour(register.isCasualplabour());
		try
		{
			String CASUALPLABOUR = uploadDocuments(register.getRegistrationId(), cplCertificateDoc, "CASUALPLABOUR");
			personalDetails.setCplCertificate(CASUALPLABOUR !=null ? CASUALPLABOUR : null);
		}catch(Exception e)
		{
			e.getMessage();
		}
		personalDetails.setRecruitmentZone(register.getRecruitmentZone());
		personalDetails.setGender(register.getGender());
		personalDetails.setFinalSubmit(setBooleanNull(register.isFinalSubmit()));
		personalDetails.setPayment(setBooleanNull(register.isPayment()));

		JobDetails jobDetails = new JobDetails();
		jobDetails.setId(setIntegerNull(register.getJobId()));
		personalDetails.setJobDetails(jobDetails);
		// personalRepository.save(personalDetails);
		EssentialQualification essentials = new EssentialQualification();
		essentials.setHighBoard(setStringNull(register.getHighBoard()));
		essentials.setHighCourseType(setStringNull(register.getHighCourseType()));
		essentials.setHighInstitution(register.getHighInstitution());
		essentials.setHighObtainMarks(setIntegerNull(register.getHighObtainMarks()));
		essentials.setHighTotalMarks(setIntegerNull(register.getHighTotalMarks()));
		essentials.setHighPercentage(setFloatNull(register.getHighPercentage()));
		essentials.setHighGrade(register.getHighGrade());
		try {
			String HIGH = uploadDocuments(register.getRegistrationId(), highCertificateDoc, "HIGH");
			essentials.setHighCertificate(HIGH != null ? HIGH : null);
		} catch (Exception e) {
			e.getMessage();
		}
		essentials.setHighSpecialization(register.getHighSpecialization());
		essentials.setHighYear(setIntegerNull(register.getHighYear()));
		essentials.setInterBoard(setStringNull(register.getInterBoard()));
		essentials.setInterCourseType(setStringNull(register.getInterCourseType()));
		essentials.setInterInstitution(register.getInterInstitution());
		essentials.setInterObtainMarks(setIntegerNull(register.getInterObtainMarks()));
					
		essentials.setInterTotalMarks(setIntegerNull(register.getInterTotalMarks()));
		essentials.setInterPercentage(setFloatNull(register.getInterPercentage()));
		System.out.println(register.getInterGrade());
		essentials.setInterGrade(register.getInterGrade());
		try {
			String INTER = uploadDocuments(register.getRegistrationId(), interCertificateDoc, "INTER");
			essentials.setInterCertificate(INTER != null ? INTER : null);
		} catch (Exception e) {
			e.getMessage();
		}
		essentials.setInterSpecialization(register.getInterSpecialization());
		essentials.setInterYear(setIntegerNull(register.getInterYear()));
		essentials.setHindiTyping(setIntegerNull(register.getHindiTyping()));
		essentials.setEnglishTyping(setIntegerNull(register.getEnglishTyping()));
		essentials.setHindiStenography(setIntegerNull(register.getHindiStenography()));
		essentials.setEnglishStenography(setIntegerNull(register.getEnglishStenography()));
		try {
			String ADDITIONAL = uploadDocuments(register.getRegistrationId(), additionalCertificateDoc, "ADDITIONAL");
			essentials.setAdditionalCertificate(ADDITIONAL != null ? ADDITIONAL : null);
		} catch (Exception e) {
			e.getMessage();
		}
		
		try {
			String FIRSTAID = uploadDocuments(register.getRegistrationId(), firstaidCertificateDoc, "FIRSTAID");
			essentials.setFirstaidCertificate(FIRSTAID != null ? FIRSTAID : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// essentialRepository.save(essentials);

		personalDetails.setEssential(essentials);
		// register.setEssential(essentials);

		DrivingLicense driving = new DrivingLicense();
		driving.setIssueDateLmv(register.getIssueDateLmv());
		driving.setIssuingAuthorityLmv(register.getIssuingAuthorityLmv());
		driving.setLicenceNumberLmv(register.getLicenceNumberLmv());
		driving.setValidUptoLmv(register.getValidUptoLmv());
		driving.setIssueDateHgmv(register.getIssueDateHgmv());
		driving.setIssuingAuthorityHlmv(register.getIssuingAuthorityHlmv());
		driving.setLicenceNumberHgmv(register.getLicenceNumberHgmv());
		driving.setValidUptoHgmv(register.getValidUptoHgmv());
		driving.setIssueDateRoad(register.getIssueDateRoad());
		driving.setIssuingAuthorityRoad(register.getIssuingAuthorityRoad());
		driving.setLicenceNumberRoad(register.getLicenceNumberRoad());
		driving.setValidUptoRoad(register.getValidUptoRoad());
		driving.setIssueDateOeg(register.getIssueDateOeg());
		driving.setIssuingAuthorityOeg(register.getIssuingAuthorityOeg());
		driving.setLicenceNumberOeg(register.getLicenceNumberOeg());
		driving.setValidUptoOeg(register.getValidUptoOeg());
		
		try {
			String LICENSE = uploadDocuments(register.getRegistrationId(), drivingLicenseDoc, "LICENSE");
			driving.setUploadLicence(LICENSE != null ? LICENSE : null);

			String DRIVING = uploadDocuments(register.getRegistrationId(), drivingExperienceDoc, "DRIVING");
			driving.setDrivingExperience(DRIVING != null ? DRIVING : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// register.setLicence(driving);
		personalDetails.setDriving(driving);
		System.out.println(driving.toString());
		OrganisationExperience experience = new OrganisationExperience();
		experience.setCompanyName(register.getCompanyName());
		experience.setEmploymentFrom(register.getEmploymentFrom());
		experience.setEmploymentTo(register.getEmploymentTo());
		experience.setCinNumber(register.getCinNumber());
		experience.setRegtinNumber(register.getRegtinNumber());
		experience.setTemporaryPermanent(register.getTemporaryPermanent());
		experience.setWorkNature(register.getWorkNature());
		experience.setMonthlySalary(setLongNull(register.getMonthlySalary()));
		try {
			String EXPERIENCE = uploadDocuments(register.getRegistrationId(), experienceCertificateDoc, "EXPERIENCE");
			experience.setExperienceCertificate(EXPERIENCE != null ? EXPERIENCE : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// register.setExperience(experience);

		personalDetails.setExperience(experience);

		TechnicalQualification technicals = new TechnicalQualification();
		technicals.setDegreeYear(setIntegerNull(register.getDegreeYear()));
		technicals.setDegreeBranch(register.getDegreeBranch());
		technicals.setDegreeCourseType(register.getDegreeCourseType());
		technicals.setDegreeInstitution(register.getDegreeInstitution());
		technicals.setDegreeMarksObtain(setIntegerNull(register.getDegreeMarksObtain()));
		technicals.setDegreeTotalMarks(setIntegerNull(register.getDiplomaTotalMarks()));
		technicals.setDegreePercentage(setFloatNull(register.getDegreePercentage()));
		technicals.setDegreeSpecialization(register.getDegreeSpecialization());
		technicals.setDegreeUniversity(register.getDegreeUniversity());
		try {
			String DEGREE = uploadDocuments(register.getRegistrationId(), degreeCertificateDoc, "DEGREE");
			technicals.setDegreeCertificate(DEGREE != null ? DEGREE : null);
		} catch (Exception e) {
			e.getMessage();
		}
		technicals.setDiplomaBranch(register.getDiplomaBranch());
		technicals.setDiplomaYear(setIntegerNull(register.getDiplomaYear()));
		technicals.setDiplomaCourseType(register.getDiplomaCourseType());
		technicals.setDiplomaInstitution(register.getDiplomaInstitution());
		technicals.setDiplomaObtainMarks(setIntegerNull(register.getDiplomaObtainMarks()));
		technicals.setDiplomaTotalMarks(setIntegerNull(register.getDiplomaTotalMarks()));
		technicals.setDiplomaPercentage(setFloatNull(register.getDiplomaPercentage()));
		technicals.setDiplomaUniversity(register.getDiplomaUniversity());
		technicals.setDiplomaSpecialization(register.getDiplomaSpecialization());
		try {
			String DIPLOMA = uploadDocuments(register.getRegistrationId(), diplomaCertificateDoc, "DIPLOMA");
			technicals.setDiplomaCertificate(DIPLOMA != null ? DIPLOMA : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// register.setTechnical(technicals);
         
		
		technicals.setItiYear(setIntegerNull(register.getItiYear()));
		technicals.setItiBranch(register.getItiBranch());
		technicals.setItiCourseType(register.getItiCourseType());
		technicals.setItiInstitution(register.getItiInstitution());
		technicals.setItiMarksObtain(setIntegerNull(register.getItiMarksObtain()));
		technicals.setItiTotalMarks(setIntegerNull(register.getDiplomaTotalMarks()));
		technicals.setItiPercentage(setFloatNull(register.getItiPercentage()));
		technicals.setItiSpecialization(register.getItiSpecialization());
		technicals.setItiUniversity(register.getItiUniversity());
		try {
			String Iti = uploadDocuments(register.getRegistrationId(), itiCertificateDoc, "Iti");
			technicals.setItiCertificate(Iti != null ? Iti : null);
		} catch (Exception e) {
			e.getMessage();
		}
		
		personalDetails.setTechnical(technicals);

		GraduationQualification qualify = new GraduationQualification();
		qualify.setPgInstitution(register.getPgInstitution());
		qualify.setPgCourse(register.getPgCourse());
		qualify.setPgCourseType(register.getPgCourseType());
		qualify.setPgObtainMarks(setIntegerNull(register.getPgObtainMarks()));
		qualify.setPgTotalMarks(setIntegerNull(register.getPgTotalMarks()));
		qualify.setPgPassingYear(setIntegerNull(register.getPgPassingYear()));
		qualify.setPgPercentage(setFloatNull(register.getPgPercentage()));
		qualify.setPgSpecialization(register.getPgSpecialization());
		qualify.setPgUniversity(register.getPgUniversity());
		try {
			String PG = uploadDocuments(register.getRegistrationId(), pgCertificateDoc, "PG");
			qualify.setPgCertificate(PG != null ? PG : null);
		} catch (Exception e) {
			e.getMessage();
		}
		qualify.setUgInstitution(register.getUgInstitution());
		qualify.setUgCourse(register.getUgCourse());
		qualify.setUgCourseType(register.getUgCourseType());
		qualify.setUgObtainMarks(setIntegerNull(register.getUgObtainMarks()));
		qualify.setUgTotalMarks(setIntegerNull(register.getUgTotalMarks()));
		qualify.setUgPassingYear(setIntegerNull(register.getUgPassingYear()));
		qualify.setUgPercentage(setFloatNull(register.getUgPercentage()));
		qualify.setUgSpecialization(register.getUgSpecialization());
		qualify.setUgUniversity(register.getUgUniversity());
		try {
			String UG = uploadDocuments(register.getRegistrationId(), ugCertificateDoc, "UG");
			qualify.setUgCertificate(UG != null ? UG : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// register.setGraduation(qualify);

		personalDetails.setGraduation(qualify);

		User user = new User();
		user.setLogin(register.getRegistrationId());
		user.setFirstName(register.getFirstName());
		user.setLastName(register.getLastName());
		user.setEmail(register.getEmailAddress());
		user.setActivated(true);
		user.setActivationKey(register.getActivationKey());
		if (register.getLangKey() == null) {
			user.setLangKey("en"); // default language
		}
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
		Set<Authority> authorities = new HashSet<>();
		authorities.add(authority);
		user.setAuthorities(authorities);

		// String password = register.getFirstName().toUpperCase();
		String encryptedPassword = passwordEncoder.encode(generatePassword(register));
		user.setPassword(encryptedPassword);

		personalDetails.setUser(user);
		UploadCertificate certificate = new UploadCertificate();
		try {
			String PHOTO = uploadDocuments(register.getRegistrationId(), browsePhotoDoc, "PHOTO");
			certificate.setBrowsePhoto(PHOTO != null ? PHOTO : null);
		} catch (Exception e) {
		}
		try {
			String SIGN = uploadDocuments(register.getRegistrationId(), browseSignatureDoc, "SIGN");
			certificate.setBrowseSignature(SIGN != null ? SIGN : null);
		} catch (Exception e) {
			e.getMessage();
		}
		// register.setUpload(certificate);
		personalDetails.setCertificate(certificate);

		personalRepository.save(personalDetails);

	}

	private boolean setBooleanNull(boolean belongCommunity) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean setBooleanNull(String payment) {
		// TODO Auto-generated method stub
		return false;
	}

	private String uploadDocuments(String registrationId, MultipartFile multipartFile, String fileNamePrefix) {

		String newDocName = "";
		try {
			if (registrationId != null || !multipartFile.isEmpty()) {
				File doc = new File(UPLOADED_FOLDER + registrationId + "/");
				if (!doc.exists()) {
					log.info(" Dir " + registrationId + " NOT Exists");
					doc.mkdirs();
				}
				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("trim file name" + fileName);
				}
				String folder_name = UPLOADED_FOLDER + registrationId + "/" + fileName;
				log.info("orignal folder_name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + fileName;
					System.out.println("New Document nmae " + newDocName);
				} else {
					newDocName = fileName;
					System.out.println("New Document nmae " + newDocName);

				}
				String newPath = UPLOADED_FOLDER + registrationId + "/" + newDocName;
				log.info("rename file : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				System.out.println("path" + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			log.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	private String generatePassword(RegistrationBean bean) {

		String pass = "";
		Date dob = bean.getDateOfBirth();
		pass = bean.getFirstName().substring(0, 2).toUpperCase().trim();
		pass = pass + bean.getMotherName().substring(0, 2).toUpperCase().trim();
		System.out.println("Enter DOB : " + dob);
		int i = dob.getDate();
		if (i < 10)
			pass = pass + "0" + i;
		else
			pass = pass + i;

		i = dob.getMonth();
		if (i < 10)
			pass = pass + "0" + i;
		else
			pass = pass + i;
		;
		System.out.println("---------------------------------------------------------------------");
		System.out.println("Generated Password : " + pass);
		System.out.println("---------------------------------------------------------------------");
		return pass;

	}
	
	
	/****************Auto Genrated paasword******************/
	
	/*	private String generatePassword() {
			String pass = "";
			// Length of your password as I have choose
			// here to be 8
			int length = 10;
			log.info("\n--------------Password in String\n\t-----------------" + geek_Password(length).toString()
					+ "\n----------------------\t-----------------");
			return pass;

		}

		private char[] geek_Password(int length) {
			// A strong password has Cap_chars, Lower_chars,
			// numeric value and symbols. So we are using all of
			// them to generate our password
			String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String Small_chars = "abcdefghijklmnopqrstuvwxyz";
			String numbers = "0123456789";
			String symbols = "!@#$%^&*_=+-/.?<>)";

			String values = Capital_chars + Small_chars + numbers + symbols;

			// Using random method
			Random rndm_method = new Random();

			int len = 0;
			char[] password = new char[len];

			for (int i = 0; i < len; i++) {
				// Use of charAt() method : to get character value
				// Use of nextInt() as it is scanning the value as int
				password[i] = values.charAt(rndm_method.nextInt(values.length()));

			}
			return password;
		}
	*/

	public List<PersonalDetails> filterList(CandidateListFilter filter) {
		if (StringUtils.isNotEmpty(filter.getRegistrationId())) {
			return personalRepository.findForRegistrationId(filter.getRegistrationId());
		}
		if (StringUtils.isNotEmpty(filter.getCategory())) {
			return personalRepository.findForCategory(filter.getCategory());
		}
		return personalRepository.findAll();

	}

	private String setStringNull(Object object) {
		if (object.equals("undefined")) {
			return null;
		}
		return String.valueOf(object);
	}

	private int setIntegerNull(Object object) {
		if (object.equals("undefined")) {
			return 0;
		}
		return Integer.parseInt(String.valueOf(object));
	}

	private long setLongNull(Object object) {
		if (object.equals("undefined")) {
			return 0;
		}
		return Long.parseLong(String.valueOf(object));
	}

	private boolean setBooleanNull(Object object) {
		if (object.equals("undefined") || object.equals("false")) {
			return false;
		}
		return Boolean.parseBoolean(String.valueOf(object));
	}

	private Float setFloatNull(Object object) {
		if (object.equals("undefined")) {
			return Float.parseFloat("0.0");
		}
		return Float.parseFloat(String.valueOf(object));
	}

	private Date setDateNull(Object object) {
		Date startDate = null;
		if (object.equals("undefined")) {
			return startDate;
		}
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		try {
			startDate = df.parse(String.valueOf(object));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}

		return startDate;
	}

	public String getMaxDate(int maxAge, Date date) {
		Calendar now1 = Calendar.getInstance();
		now1.setTime(date);
		now1 = Calendar.getInstance();
		now1.add(Calendar.YEAR, -maxAge);
		return "" + ((now1.get(Calendar.YEAR) + 1) + ", " + now1.get(Calendar.MONTH) + ", " + now1.get(Calendar.DATE));
	}

}
