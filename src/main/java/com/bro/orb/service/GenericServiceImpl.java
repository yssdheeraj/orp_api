package com.bro.orb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bro.orb.dao.IGenericDao;

@Service
public class GenericServiceImpl<T> implements IGenericService<T> {

	@Autowired
	IGenericDao<T> igenericDao;

	@Override
	public T findOne(T entity, long id) {
		return igenericDao.findOne(entity, id);
	}

	public T findOneByCondition(T entity, String condition) {
		return igenericDao.findOneByCondition(entity, condition);
	}

	@Override
	public List<T> findAll(T entity) {
		
		return igenericDao.findAll(entity);
	}

	@Override
	public List<T> findAllByCondition(T entity, String condition) {
		return igenericDao.findAllByCondition(entity, condition);
	}

	@Override
	public void save(T entity) {
		igenericDao.save(entity);
	}

	@Override
	public T update(T entity) {
		return igenericDao.update(entity);
	}

	@Override
	public void delete(T entity) {
		igenericDao.delete(entity);
	}

	@Override
	public boolean exists(T entity, long entityId) {
		return igenericDao.exists(entity, entityId);
	}

	@Override
	public T findOneByCondition(T entity, String condition, int index) { 
		return igenericDao.findOneByCondition(entity, condition, index);
	}

	@Override
	public boolean exists(T entity, String condition) { 
		return igenericDao.exists(entity, condition);
	}


 
}