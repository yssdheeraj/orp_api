package com.bro.orb.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bro.orb.domain.FingerPrintInformetions;
import com.bro.orb.domain.PersonalDetails;
import com.bro.orb.fingerprint.service.ScannerService;
import com.bro.orb.repository.FingerPrintInformetionsRepository;
import com.bro.orb.repository.PersonalRepository;

@RestController
@RequestMapping("/api/admin")
public class FingerPrintController {
	
	@Autowired
	FingerPrintInformetionsRepository fpiRepository;
	
	@Autowired
	private PersonalRepository personalRepository;
	
	private List<String> registrationID=new ArrayList<String>();
	private String temp;
	

	
	@RequestMapping("/deviceConnectin/varification")
	public List deviceConnectionVarification() {
		List<String> deviceInfo=new ArrayList();
				
		ScannerService ss=new ScannerService();
		deviceInfo.add(ss.getVersion());
		deviceInfo.add(ss.getInit()+"");
		deviceInfo.add(ss.checkDevice()+"");
		
		
		return deviceInfo;
	}
	
	
	@RequestMapping("/fingerprint/storetion/{registrationId}")
	public ResponseEntity<FingerPrintInformetions> fingerPrintDataStore(@PathVariable("registrationId") String registrationId) {
		registrationID.add(0, "123456");
		temp="1234";
		String result="";
		
		FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
		ScannerService ss=new ScannerService();
		try {
			
			PersonalDetails pd=personalRepository.findForRegistrationId(registrationId).get(0);
					fingerPrintInformetions=fpiRepository.findByRegistrationId(registrationId);
			 		 	System.out.println(registrationId+"       "+fingerPrintInformetions);
			 		 	if(fingerPrintInformetions == null) {	
			 		 			fingerPrintInformetions=ss.getAutoCapture(registrationId);
			 		 				if(fingerPrintInformetions.getAnsiTemplate() !=null && fingerPrintInformetions.getAnsiTemplate() !=null)
			 		 					{
			 		 						System.out.println("personal data "+pd.getId());
			 		 						fingerPrintInformetions.setPersonalDetails(pd);
			 		 						fpiRepository.save(fingerPrintInformetions);
			 		 						fingerPrintInformetions.setStatus("NEW");	
			 		 						
			 		 					}else
			 		 						{
			 		 						//please try again thumb not verify
			 		 						fingerPrintInformetions.setStatus("TRYAGAIN");
			 		 						}
			 		 		}else
			 		 		{
			 		 			//user all ready exist
			 		 		fingerPrintInformetions.setStatus("ALREADY_REPORTED");
			 		 			
			 		 		}
			 		temp=fingerPrintInformetions.getRegistrationId();
			 		 	
			 		 	System.out.println("reg array list "+registrationID);
			 	 return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
			 	}catch(Exception e) {
			System.out.println(e.getMessage());
				fingerPrintInformetions=new FingerPrintInformetions();
			fingerPrintInformetions.setStatus("NOTEXIST");
			return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
		}
		// return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
	} 
	
	@RequestMapping("/fingerprint/details")
	public ResponseEntity<FingerPrintInformetions> fingerPrintDetails() {
		//String reg=registrationID.get(0);
		FingerPrintInformetions fingerPrintInformetionsList=fpiRepository.findByRegistrationId(temp);
		temp="";
		return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetionsList,HttpStatus.OK);
	}
	
	@RequestMapping("/fingerprint/varification1")
	public ResponseEntity<FingerPrintInformetions> fingerPrintVarification1() {
	List<FingerPrintInformetions> fingerPrintInformetionsList=fpiRepository.findAll();
	ScannerService scan=new ScannerService();
	FingerPrintInformetions fingerPrintInformetions=scan.matchANSIActionPerformed(fingerPrintInformetionsList);
	return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
	}
	
	@RequestMapping("fingerprint/varification2")
	public ResponseEntity<FingerPrintInformetions> fingerPrintVarification2() {
	List<FingerPrintInformetions> fingerPrintInformetionsList=fpiRepository.findAll();
	ScannerService scan=new ScannerService();
	FingerPrintInformetions fingerPrintInformetions=scan.matchISOActionPerformed(fingerPrintInformetionsList);
	return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
	}
	
	@RequestMapping("/fingerprint/stop")
	public ResponseEntity<?> fingerPrintStop() {
		ScannerService scan=new ScannerService();
		System.out.println("stop..........");
		scan.stopCapture();
		scan.unInitdevice();
		FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
		fingerPrintInformetions.setStatus("STOP");
		return new ResponseEntity<FingerPrintInformetions>(fingerPrintInformetions,HttpStatus.OK);
	}

	
	
	

}
