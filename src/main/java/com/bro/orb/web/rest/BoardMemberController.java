package com.bro.orb.web.rest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bro.orb.domain.BoardMemberDetails;
import com.bro.orb.domain.BoardMemberEntity;
import com.bro.orb.domain.BoardMemberHelperEntity;
import com.bro.orb.domain.JobDetails;
import com.bro.orb.domain.User;
import com.bro.orb.service.IGenericService;
import com.codahale.metrics.annotation.Timed;

@RestController
@CrossOrigin
@RequestMapping("/board")
public class BoardMemberController {
	
	private final Logger log = LoggerFactory.getLogger(AccountResource.class);
	
	@Autowired
	IGenericService<BoardMemberEntity> BMService;
	@Autowired
	IGenericService<JobDetails> jobService;
	
	@Autowired
    private IGenericService<User> userServices;
	
	@Autowired
	private IGenericService<BoardMemberDetails> bmdService;
	
	@GetMapping(value="/member/list")
	public ResponseEntity<List<BoardMemberEntity>> getBoardMember(){
		
		List<BoardMemberEntity> boardmemberList=BMService.findAll(new BoardMemberEntity());
		return new ResponseEntity<List<BoardMemberEntity>>(boardmemberList,HttpStatus.OK);
	}
	
	@GetMapping(value="/member/list/{group}")
	public ResponseEntity<List<BoardMemberEntity>> getBoardMemberByGroup(@PathVariable String group){
		
		System.out.println("----------------------------- \n  "+group);
		List<BoardMemberEntity> boardmemberList=BMService.findAllByCondition(new BoardMemberEntity(),"where boardName='"+group+"' and flag = true ");
		return new ResponseEntity<List<BoardMemberEntity>>(boardmemberList,HttpStatus.OK);
	}
	
	@GetMapping(value="/member/addnew/{groupId}")
	public ResponseEntity<List<User>> getAddNewBoardMember(@PathVariable String groupId){
		
		System.out.println("----------------------------- \n  "+groupId);
		List<BoardMemberEntity> boardmemberList=BMService.findAllByCondition(new BoardMemberEntity(),"where boardName='"+groupId+"'  and flag =true  ");
	//	List<User> userList1=boardmemberList.
		System.out.println(" list of user id \n "+boardmemberList);
		List<User> userList=userServices.findAll(new User());
		for(BoardMemberEntity BME:boardmemberList){
			System.out.println(BME.getUserId());
			User u1=BME.getUserId();
			for(User u:userList){
				if(u.equals(u1)){
					System.out.println(u.getId()+"   "+u1.getId());
					userList.remove(u);
					break;
				}else{
					System.out.println("  no match ");
				}
			}
		}
		
		
		return new ResponseEntity<List<User>>(userList,HttpStatus.OK);
	}
	
	@GetMapping(value="/member/group")
	public ResponseEntity<List<BoardMemberDetails>> getAllGroup(){
		
		List<BoardMemberEntity> newGroupList=new ArrayList<BoardMemberEntity>();
		//Set<String> groupList=new HashSet<String>();
		
		List<BoardMemberDetails> boardmemberList=bmdService.findAll(new BoardMemberDetails());
		for(BoardMemberDetails boardMemberEntity:boardmemberList){
		/*if(groupList.add(boardMemberEntity.getBoardName())){
			newGroupList.add(boardMemberEntity);
		}
		*/
	}
		return new ResponseEntity<List<BoardMemberDetails>>(boardmemberList,HttpStatus.OK);
	}
	
	/*@PostMapping(value="/save")
	public ResponseEntity<?> saveBoardMember1(@RequestBody BoardMemberEntity boardMemberEntity){
		System.out.println("---------"+boardMemberEntity);
		if(boardMemberEntity!=null){
			BMService.save(boardMemberEntity);
			return new ResponseEntity<>("data saved",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("data not saved",HttpStatus.BAD_REQUEST);
		}
			
		
	}*/
	
	@PostMapping(value="/member/save")
	public ResponseEntity<?> saveBoardMember(@RequestBody BoardMemberHelperEntity boardMemberHelperEntity){
		System.out.println("---------"+boardMemberHelperEntity);
		if(boardMemberHelperEntity!=null){
			
			String groupName=boardMemberHelperEntity.getGroupName();
			long postId=boardMemberHelperEntity.getPostId();
			BoardMemberDetails bmd=new BoardMemberDetails();
			bmd.setName(groupName);
			JobDetails job=jobService.findOne(new JobDetails(), postId);
			System.out.println("job "+job);
			bmd.setJobId(job);
			BoardMemberDetails bmd1=bmdService.update(bmd);
			
			
			
			List<String> usersId=boardMemberHelperEntity.getUserslist();
		System.out.println(groupName+"  "+postId+"  "+usersId);
		
		for(String id:usersId){
			BoardMemberEntity boardMemberEntity=new BoardMemberEntity();
			boardMemberEntity.setBoardName(bmd1);
			long userId=Long.parseLong(id);
			User user=userServices.findOne(new User(), userId);
			boardMemberEntity.setUserId(user);
			boardMemberEntity.setFlag(true);
			
			
			BMService.save(boardMemberEntity);
			System.out.println("data saved  \n ----------------------"+boardMemberEntity);
			
		}
			return new ResponseEntity<>("data saved",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("data not saved",HttpStatus.BAD_REQUEST);
		}
			
		
	}
	
	
	@PostMapping(value="/member/update")
	public ResponseEntity<?> updateBoardMember(@RequestBody BoardMemberHelperEntity boardMemberHelperEntity){
		System.out.println("---------"+boardMemberHelperEntity);
		if(boardMemberHelperEntity!=null){
			
			String groupName=boardMemberHelperEntity.getGroupName();
			long postId=boardMemberHelperEntity.getPostId();
			BoardMemberDetails bmd=new BoardMemberDetails();
			long bmdid=0;
			try{
			 bmdid=Long.parseLong(groupName);
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			
			/*bmd.setName(groupName);
			JobDetails job=jobService.findOne(new JobDetails(), postId);
			System.out.println("job "+job);
			bmd.setJobId(job);*/
			bmd=bmdService.findOne(new BoardMemberDetails(), bmdid);
			//BoardMemberDetails bmd1=bmdService.update(bmd);
			
			
			
			List<String> usersId=boardMemberHelperEntity.getUserslist();
		System.out.println(groupName+"  "+postId+"  "+usersId);
		
		for(String id:usersId){
			BoardMemberEntity boardMemberEntity=new BoardMemberEntity();
			boardMemberEntity.setBoardName(bmd);
			long userId=Long.parseLong(id);
			User user=userServices.findOne(new User(), userId);
			boardMemberEntity.setUserId(user);
			boardMemberEntity.setFlag(true);
			BMService.update(boardMemberEntity);
			System.out.println("data updated  \n ----------------------"+boardMemberEntity);
			
		}
			return new ResponseEntity<>("data updated",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("data not updated",HttpStatus.BAD_REQUEST);
		}
			
		
	}
	
	   @GetMapping("/users/list")
	    public ResponseEntity<List<User>> getAllUser(){
	    	List<User> userList=userServices.findAll(new User());
	    	return new ResponseEntity<List<User>>(userList,HttpStatus.OK);
	    }

	   @GetMapping("/member/delete/{id}")
	   public ResponseEntity<?> deleteUserFromGroup(@PathVariable long id){
		   BoardMemberEntity boardMemberEntity=BMService.findOne(new BoardMemberEntity(), id);
		   boardMemberEntity.setFlag(false);
		   BMService.delete(boardMemberEntity);
		   System.out.println("data delete");
		   return new ResponseEntity<>(HttpStatus.OK);
	   }
}
