package com.bro.orb.fingerprint.MFS100;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ThumbImageEntity {
	
	private int _width=0;
	private int _height=0;
	private String thumbImage;
	private String filePath;
	public ThumbImageEntity() {
		
	}
	
	
	
public ThumbImageEntity(int _width,int _heigth) {
		this._height=_heigth;
		this._width=_width;
	}



public String getFilePath() {
	return filePath;
}



public void setFilePath(String filePath) {
	this.filePath = filePath;
}



public boolean loadImage(String path) {
	boolean result=false;
	BufferedImage newImage;
	try {
		File f=new File(path);
		newImage=ImageIO.read(f);
		result=true;
		setImage(newImage);
		
	}catch(IOException e) {
		System.out.println(e.getMessage());
	}
	
	return result;
}

public void setImage(BufferedImage image) {
	if(image !=null) {
		thumbImage=image.toString();
		thumbImage=image.getScaledInstance(200, 200, 1).toString();
	}
	else
	{
		thumbImage=null;
	}
	//return thumbImage;
}

public String getImage() {
	
	return thumbImage;
}


public void writeBytesToFile(String FileName, byte[] Bytes) {
       try {
          // String FilePath = System.getProperty("user.dir");
           String FilePath =ImagePath.SOURCE_PATH; 
           File file = new File(FilePath);
           if (!file.exists()) {
               file.mkdirs();
           }
           FilePath += "\\" + FileName;
           System.out.println("file Path ==>"+FilePath);
         
           
           FileOutputStream fos = new FileOutputStream(FilePath);
           
           fos.write(Bytes);
           fos.close();
       } catch (Exception ex) {
       }
   }
}
