package com.bro.orb.fingerprint.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.bro.orb.domain.FingerPrintInformetions;
import com.bro.orb.fingerprint.MFS100.ThumbImageEntity;
import com.bro.orb.repository.FingerPrintInformetionsRepository;

import MFS100.FingerData;
import MFS100.MFS100;
import MFS100.MFS100Event;

public class ScannerService {
	
	@Autowired
	FingerPrintInformetionsRepository fpiRepository;
	
	MFS100Event event = null;
	MFS100 mfs100=null;
	String result="";
	boolean status=false;
	FingerData fingerData=null;
	byte[] ISOTemplate = null;
	byte[] ANSITemplate = null;
	ThumbImageEntity thumbImageEntity=null;

	/*
	MFS100Event event = null;
	MFS100 mfs100=null;
	String result="";
	boolean status=false;
	FingerData fingerData=null;
	byte[] ISOTemplate = null;
    byte[] ANSITemplate = null;
    ThumbImageEntity thumbImageEntity=null;
    FingerPrintInformetions thumb=null;*/
	public ScannerService() {
		
		thumbImageEntity=new ThumbImageEntity();
		mfs100=new MFS100(event);
		mfs100.GetSDKVersion();
		mfs100.Init();
		mfs100.GetDeviceInfo();
		mfs100.IsConnected();
	}
	
	public String getVersion() {
		result=mfs100.GetSDKVersion();
		
		return result;
	}
	
public int getInit() {
	int result=mfs100.Init();
		return result;
	}

public boolean checkDevice() {
	status=mfs100.IsConnected();
	return status;
}

public int getcapture() {
	int result=mfs100.StartCapture(100, 10000, true);
	return result;
}

public int stopCapture(){
	int result=mfs100.StopCapture();
	return  result;
}
public int unInitdevice() {
	int result=mfs100.Uninit();
	return result;
}

public String caputure() {
	
	return "";
	
}
public String matching() {
	return "";
}

public FingerPrintInformetions getAutoCapture(String registration) {
	fingerData = new FingerData();
	FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
	
	int result=mfs100.AutoCapture(fingerData, 1000, true, true);
	if(result==0) {
	thumbImageEntity.setImage(mfs100.BytesToBitmap(fingerData.FingerImage()));
	 ISOTemplate = new byte[fingerData.ISOTemplate().length];
	 System.arraycopy(fingerData.ISOTemplate(), 0, ISOTemplate, 0, fingerData.ISOTemplate().length);
     ANSITemplate = new byte[fingerData.ANSITemplate().length];
     System.arraycopy(fingerData.ANSITemplate(), 0, ANSITemplate, 0, fingerData.ANSITemplate().length);
    
          thumbImageEntity.writeBytesToFile("FingerImage.bmp", fingerData.FingerImage());
          thumbImageEntity.writeBytesToFile("ISOTemplate.iso", fingerData.ISOTemplate());
          thumbImageEntity.writeBytesToFile("ISOImage.iso", fingerData.ISOImage());
          thumbImageEntity.writeBytesToFile("AnsiTemplate.ansi", fingerData.ANSITemplate());
          thumbImageEntity.writeBytesToFile("RawData.raw", fingerData.RawData());
          thumbImageEntity.writeBytesToFile("WSQImage.wsq", fingerData.WSQImage());
          System.out.println("Capture Success.\nFinger data is saved at application path");
          System.out.println("data saving...in object...");
       
        fingerPrintInformetions=new FingerPrintInformetions();
  	   	fingerPrintInformetions.setRegistrationId(registration);
  	   	fingerPrintInformetions.setAnsiTemplate(ANSITemplate);
  	   	fingerPrintInformetions.setIsoTemplate(ISOTemplate);
  	   	fingerPrintInformetions.setFingerImage("FingerImage.bmp");
  	   	fingerPrintInformetions.setAnsiImage("AnsiTemplate.ansi");
  	   	fingerPrintInformetions.setIsoImage("ISOTemplate.iso");
  	   	fingerPrintInformetions.setNfiq("nfiq");
  	   	fingerPrintInformetions.setRawData("RawData.raw");
  	   	fingerPrintInformetions.setWsqImage("WSQImage.wsq");
  	   	
          System.out.println(getSave(registration));
          
     }
//	}
	

		
   
	return fingerPrintInformetions;
}


 public FingerPrintInformetions matchISOActionPerformed(List<FingerPrintInformetions> thumblist) {//GEN-FIRST:event_btnMatchISOActionPerformed
	 
	 FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
	System.out.println(" scnnanre "+thumblist);
	System.out.println("verification.....................");
	  int score = 0;
        FingerData fingerData = new FingerData();
        int ret = mfs100.AutoCapture(fingerData, 10000,  true,true);
        if (ret == 0) {

            for(int i=0;i<thumblist.size();i++)
            {
            	
            	System.out.println();
            	ISOTemplate=thumblist.get(i).getIsoTemplate();
            	System.out.println(ISOTemplate);
            	if(ISOTemplate !=null) {
            			score = mfs100.MatchISO(fingerData.ISOTemplate(), ISOTemplate);
            			System.out.println(score);
            				if (score >= 14000) {
            					System.out.println("condition true "+score);
            					fingerPrintInformetions=thumblist.get(i);
            						break;
            				} 
            	}
            }
        }
         /*
            if (score >= 14000) {
       //         JOptionPane.showMessageDialog(rootPane, "Finger Matched With Score " + String.valueOf(score),appTitle,JOptionPane.INFORMATION_MESSAGE);
            } else if (score >= 0 && score < 14000) {
           //     JOptionPane.showMessageDialog(rootPane, "Finger Not Matched",appTitle,JOptionPane.INFORMATION_MESSAGE);
            } else {
         //       JOptionPane.showMessageDialog(rootPane, "Error: " + mfs100.GetLastError() + " (" + String.valueOf(ret) + ")",appTitle,JOptionPane.ERROR_MESSAGE);
            }
        } else {
      //     JOptionPane.showMessageDialog(rootPane, "Error: " + mfs100.GetLastError() + " (" + String.valueOf(ret) + ")",appTitle,JOptionPane.ERROR_MESSAGE);
        }*/
        return fingerPrintInformetions;
 }



public String getSave(String id) {
	 new FingerImageStore().newImageStore(id);
	 result="data saved";
	 System.out.println(result);
	
	return result;
}



public FingerPrintInformetions matchANSIActionPerformed(List<FingerPrintInformetions> thumblist) {//GEN-FIRST:event_btnMatchISOActionPerformed
	 
	FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
	System.out.println(" scnnanre "+thumblist);
	
        FingerData fingerData = new FingerData();
        int ret = mfs100.AutoCapture(fingerData, 10000,  true,true);
        int score = 0;
        if (ret == 0) {
           
            for(int i=0;i<thumblist.size();i++)
            {
            	ANSITemplate=thumblist.get(i).getAnsiTemplate();
            	System.out.println(ANSITemplate);
            	System.out.println(thumblist.get(i));
            	if(ANSITemplate !=null) {
            			score = mfs100.MatchANSI(fingerData.ANSITemplate(), ANSITemplate);
            		
            			System.out.println(score);
            				if (score >= 14000) {
            					System.out.println("condition true "+score);
            				fingerPrintInformetions=thumblist.get(i);
            					
            						break;
            				} 
            	}
            }
        }
       /*     if (score >= 14000) {
       //         JOptionPane.showMessageDialog(rootPane, "Finger Matched With Score " + String.valueOf(score),appTitle,JOptionPane.INFORMATION_MESSAGE);
            } else if (score >= 0 && score < 14000) {
           //     JOptionPane.showMessageDialog(rootPane, "Finger Not Matched",appTitle,JOptionPane.INFORMATION_MESSAGE);
            } else {
         //       JOptionPane.showMessageDialog(rootPane, "Error: " + mfs100.GetLastError() + " (" + String.valueOf(ret) + ")",appTitle,JOptionPane.ERROR_MESSAGE);
            }
        } else {
      //     JOptionPane.showMessageDialog(rootPane, "Error: " + mfs100.GetLastError() + " (" + String.valueOf(ret) + ")",appTitle,JOptionPane.ERROR_MESSAGE);
        }
      //  System.out.println("before return "+fingerPrintInformetions);
*/  
      return  fingerPrintInformetions;
 }


//verifications...
    public FingerPrintInformetions getVarify(String name,byte[] file,List<FingerPrintInformetions> thumblist) {
	int score=0;
	FingerPrintInformetions fingerPrintInformetions=new FingerPrintInformetions();
	boolean result=false;
	System.out.println("varification "+thumblist);
	int i=0;
	System.out.println("new iso--------- "+file);
	if(name=="ANSI")
	{ 
		 for( i=0;i<thumblist.size();i++)
         {
         	ANSITemplate=thumblist.get(i).getAnsiTemplate();
         	System.out.println(ANSITemplate);
         	if(ANSITemplate !=null) {
         			score = mfs100.MatchISO(file, ANSITemplate);
         			System.out.println(score);
         				if (score >= 14000) {
         					System.out.println("condition true "+score);
         					fingerPrintInformetions=thumblist.get(i);
         						break;
         				} 
         	}
         }
	}
	
	
	return fingerPrintInformetions;
}


}
