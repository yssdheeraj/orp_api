package com.bro.orb.fingerprint.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.bro.orb.fingerprint.MFS100.ImagePath;

public class FingerImageStore {




	public void newImageStore(String id) {
	
		 /*String FilePath = System.getProperty("user.dir");
		 String sourceFolder =FilePath+"\\src\\main\\webapp\\FingerData\\";
		 String targetFolder="E:\\1_new_orp_gref\\new_orp_ui\\ORP\\src\\assets\\images\\"+id+"\\";
		  */
		String sourceFolder=ImagePath.SOURCE_PATH;
		String targetFolder=ImagePath.DESTINETION_PATH+"\\"+id+"\\";
		
		 File sFile = new File(sourceFolder);

		 File[] sourceFiles = sFile.listFiles(new FilenameFilter() {
			
			           @Override
			          public boolean accept(File dir, String name) {
			
			                 if(name.endsWith(".bmp")||name.endsWith(".iso")||name.endsWith(".ansi")||name.endsWith(".raw")||name.endsWith(".wsq")) {// change this to your extension
			                	 System.out.println("reading......"+name);
			          return true;
			
			                 }else {
			
			                	 System.out.println(" no   reading......"+name);
			                     return false;
			
			                 }
			 
			             }
			 
			         });
		 
		 for(File fSource:sourceFiles) {
			 System.out.println("source file "+fSource);
		
			 if(fSource!=null) {
			 File file=new File(targetFolder);
			 if(!file.exists()) {
				 file.mkdirs();
				 System.out.println("new Folder created");
			 }
			             File fTarget = new File(new File(targetFolder),fSource.getName());
			             copyFileOneFolderToOtherFolder(fSource,fTarget);
			
			             System.out.println("DATAT DELETE");
			            fSource.delete(); // Uncomment this line if you want source file deleted
			
			         }
			 
		 }


	}
	public void copyFileOneFolderToOtherFolder(File source, File dest) {
		InputStream inStream=null;
		OutputStream outStream=null;
		
		
		try {
			inStream=new FileInputStream(source);
			outStream=new FileOutputStream(dest);
		byte[] buffer=new byte[1024];
		int length;
		while((length=inStream.read(buffer))>0) {
			outStream.write(buffer, 0, length);
		//	System.out.println("file copying.................");
		}
		inStream.close();
		outStream.close();
		
		System.out.println("file in copied successful !");
			
		
	}catch(IOException e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	}
	
	
	
	
}
