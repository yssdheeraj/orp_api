package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bro.orb.domain.NewCandidateRegistration;

public interface RegistrationRepository extends JpaRepository<NewCandidateRegistration,Long> {

}
