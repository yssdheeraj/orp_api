package com.bro.orb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.CenterAddress;
import com.bro.orb.domain.City;

@Repository
public interface CenterAddressRepository extends JpaRepository<CenterAddress, Long> {
	

}
