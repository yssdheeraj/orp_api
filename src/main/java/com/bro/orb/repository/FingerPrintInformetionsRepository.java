package com.bro.orb.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bro.orb.domain.FingerPrintInformetions;



public interface FingerPrintInformetionsRepository extends JpaRepository<FingerPrintInformetions, Long>  {
	
	@Query("Select f from FingerPrintInformetions f where f.registrationId = ?1")
	FingerPrintInformetions findByRegistrationId(String registrationId);

	
}
