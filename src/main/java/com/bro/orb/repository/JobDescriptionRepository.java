package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bro.orb.domain.JobsDescription;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface JobDescriptionRepository extends JpaRepository<JobsDescription, Long> {
	
	
}
