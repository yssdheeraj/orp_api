package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.JobAmountDetails;

@Repository
public interface JobAmountRepository extends JpaRepository<JobAmountDetails, Long> {

	

}
