package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.UploadCertificate;

@Repository
public interface UploadCertificateRepository extends JpaRepository<UploadCertificate, Long>{

}
