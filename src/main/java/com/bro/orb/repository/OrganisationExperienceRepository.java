package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bro.orb.domain.OrganisationExperience;

@Repository
public interface OrganisationExperienceRepository extends JpaRepository<OrganisationExperience, Long> {

}
