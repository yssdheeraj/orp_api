package com.bro.orb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bro.orb.domain.ShortListAPersonalDetails;

public interface CandidateUpdateMarksRespository extends JpaRepository<ShortListAPersonalDetails ,Long> {
		
	/*@Query("UPDATE ShortListAPersonalDetails s SET s.candidatemarks = ?1  WHERE s.id= ?2")
	
    int updateCandidateMarks(@Param("candidatemarks") String candidatemarks, @Param("id") long id);*/
		     
	
}
